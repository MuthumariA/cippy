//
//  WebViewController.swift
//  Cippy
//
//  Created by apple on 19/12/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController,UIWebViewDelegate ,WKNavigationDelegate {

    
    @IBOutlet weak var webView: UIWebView!
    var webView1 : WKWebView!
    var alertmsg = ""

    var back = UIBarButtonItem()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        back = UIBarButtonItem(image: UIImage(named: "left-blue.png"), style: .Plain, target: self, action: Selector("backaction"))
        navigationItem.leftBarButtonItem = back
        webView.delegate = self
        print(Appconstant.Url)
        let urlstrs: NSString = Appconstant.Url
        let urlStr : NSString = urlstrs.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let url = NSURL(string: urlStr as String)!
        let requestObj = NSURLRequest(URL: url);
        webView.loadRequest(requestObj)
    
//        let url = NSURL (string: urlStr as String)
//        let requestObj = NSURLRequest(URL: url!);
//             webView.loadRequest(requestObj)
        
//        webView1 = WKWebView(frame: self.view.bounds)
//        webView1.navigationDelegate = self
//        self.view.addSubview(webView1)
//        webView1.loadRequest(requestObj)
    }
    
    func backaction(){
        self.performSegueWithIdentifier("back_money", sender: self)
    }
    

    func webViewDidStartLoad(webView: UIWebView)
    {
    
    }
    func webViewDidFinishLoad(webView: UIWebView) {
    
            let doc = webView.stringByEvaluatingJavaScriptFromString("document.documentElement.outerHTML")

        let result:Bool = !(doc!.rangeOfString("\"result\"")==nil);
        if result{
            let str = doc?.componentsSeparatedByString("<pre style=\"word-wrap: break-word; white-space: pre-wrap;\">")
            let str1 = str![1]
            let finalstr = str1.componentsSeparatedByString("</pre>")
            let encodedString : NSData = (finalstr[0] as NSString).dataUsingEncoding(NSUTF8StringEncoding)!
            
            
            
            let finalJSON = JSON(data: encodedString)
            print(finalJSON)
            let item = finalJSON["result"]
            if item["authStatus"].stringValue == "0300"{
                alertmsg = "Yeh! Money loaded successfully"
                self.performSegueWithIdentifier("web_home", sender: self)
            }
            else if item["authStatus"].stringValue == "0399"{
                alertmsg = "Sorry! This transaction failed."
                self.performSegueWithIdentifier("web_home", sender: self)
            }
            else if item["authStatus"].stringValue == "NA"{
                alertmsg = "This happens sometimes, please try again"
                self.performSegueWithIdentifier("web_home", sender: self)
            }
            else if item["authStatus"].stringValue == "0002"{
                alertmsg = "BillDesk is waiting for Response from Bank"
                self.performSegueWithIdentifier("web_home", sender: self)
            }
            else if item["authStatus"].stringValue == "0001"{
                alertmsg = "Error at BillDesk"
                self.performSegueWithIdentifier("web_home", sender: self)
            }
            else if item["authStatus"].stringValue == "0000"{
                alertmsg = "Invalid payment request"
                self.performSegueWithIdentifier("web_home", sender: self)
            }
            else{
                let mtdpresent:Bool = !(doc!.rangeOfString("methodName")==nil);
                if mtdpresent{
                    alertmsg = "Huh! Something went wrong.. Please try again later"
                    self.performSegueWithIdentifier("web_home", sender: self)
                }
                else{
                    let exception:Bool = !(doc!.rangeOfString("\"exception\"")==nil);
                    if(exception)
                    {
                        let str = doc?.componentsSeparatedByString("<pre style=\"word-wrap: break-word; white-space: pre-wrap;\">")
                        let str1 = str![1]
                        let finalstr = str1.componentsSeparatedByString("</pre>")
                        let encodedString : NSData = (finalstr[0] as NSString).dataUsingEncoding(NSUTF8StringEncoding)!
                        
                        let finalJSON = JSON(data: encodedString)
                        print(finalJSON)
                        let item = finalJSON["exception"]
                        alertmsg = "Sorry! This transaction failed. This happens sometimes,please try again"
                        
                         self.performSegueWithIdentifier("web_home", sender: self)
                    }
                }
            }
            
            
            
        }
        else{
        let exception:Bool = !(doc!.rangeOfString("\"exception\"")==nil);
            if(exception)            
            {
            let str = doc?.componentsSeparatedByString("<pre style=\"word-wrap: break-word; white-space: pre-wrap;\">")
            let str1 = str![1]
            let finalstr = str1.componentsSeparatedByString("</pre>")
            let encodedString : NSData = (finalstr[0] as NSString).dataUsingEncoding(NSUTF8StringEncoding)!
            
            let finalJSON = JSON(data: encodedString)
            print(finalJSON)
            let item = finalJSON["exception"]
       
                alertmsg =  item["detailMessage"].stringValue
            
            }
            
        }
        
    }
//    func webViewDidStartLoad(webView: UIWebView, decidePolicyForNavigationResponse navigationResponse: WKNavigationResponse, decisionHandler: (WKNavigationResponsePolicy) -> Void) {
//        print("start")
//    }
//
    func webView(webView: WKWebView, decidePolicyForNavigationResponse navigationResponse: WKNavigationResponse, decisionHandler: (WKNavigationResponsePolicy) -> Void) {
        // make sure the response is a NSHTTPURLResponse
        guard let response = navigationResponse.response as? NSHTTPURLResponse else { return decisionHandler(.Allow) }
       
    
        print(response.URL)
        
        let url = (response.URL?.absoluteString)! as String
            let gotCode:Bool = !(url.rangeOfString("/processor/payment")==nil);
        if(gotCode)
        {
//            let requestObj = NSURLRequest(URL: response.URL!);
//            webView.loadRequest(requestObj)
            print("payment");
          
                
                //put your code which should be executed with a delay here
                
         
            //                let startIndexJSON  = doc.rangeOfString("{")?.startIndex
//                
//                let endIndexJSON  = doc.rangeOfString("}}")?.startIndex
//                
//                var finalJson =   doc.substringWithRange( Range<String.Index>(start:startIndexJSON!, end: endIndexJSON!.advancedBy(2)))
//                print("\n\n finalJson JSON  = \(finalJson)")
          //  sleep(15)
//print(response)
//         let page = try! String(contentsOfURL: response.URL!, encoding: NSASCIIStringEncoding)
//            print(page)
        }
        
        // allow the request to continue
        decisionHandler(.Allow);
    }


    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "web_home") {
            let nextview = segue.destinationViewController as! HomeViewController
            nextview.fromweb = true
            nextview.alertmsg = alertmsg
        }
    }
}
