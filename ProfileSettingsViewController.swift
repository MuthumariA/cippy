//
//  ProfileSettingsViewController.swift
//  Cippy
//
//  Created by apple on 26/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit
import AZSClient

class ProfileSettingsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var badgebtn: UIButton!
    @IBOutlet weak var profilebtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var savebtn: UIButton!
    @IBOutlet weak var forgotpwdbtn: UIButton!
    @IBOutlet weak var addr1txtField: UITextField!
    @IBOutlet weak var addr2txtField: UITextField!
    @IBOutlet weak var citytxtField: UITextField!
    @IBOutlet weak var statetxtField: UITextField!
    @IBOutlet weak var pintxtField: UITextField!
    @IBOutlet weak var securequestiontxtField: UITextField!
    @IBOutlet weak var secureanstxtField: UITextField!
    @IBOutlet weak var dobtxtField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var profileimg: UIImageView!
    @IBOutlet weak var genderbtn: UIButton!
    
    
    
    
    
    let imagePicker = UIImagePickerController()
    var strBase64 = ""
    var profileimgstr = ""
    var datePickerView:UIDatePicker!
    var characterCountLimit = 6
    var date = ""
    var calendar : NSCalendar = NSCalendar.currentCalendar()
    var toolbarView = UIView()
    var gender = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setvaluefortextfield()
        setvaluestoall()
        badgebtn.userInteractionEnabled = false
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 650)
        setimagefromserver()
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    func setvaluefortextfield(){
        namelbl.text = Appconstant.customername
        badgebtn.layer.cornerRadius = self.badgebtn.frame.size.height/2
        profilebtn.layer.cornerRadius = self.profilebtn.frame.size.height/2
        profileimg.layer.cornerRadius = self.profileimg.frame.size.height/2
        profileimg.clipsToBounds = true
        profileimg.layer.borderWidth = 3
        profileimg.layer.borderColor = UIColor(red: 41.0/255.0, green: 192.0/255.0, blue: 222.0/255.0, alpha: 1).CGColor
        genderbtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        savebtn.layer.cornerRadius = 10
        forgotpwdbtn.layer.cornerRadius = 10
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        scrollView.addGestureRecognizer(tap)
        imagePicker.delegate = self
        if(Appconstant.notificationcount > 0){
            badgebtn.hidden = false
            badgebtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
        }
        else{
            badgebtn.hidden = true
        }
        
        
    }
    func setimagefromserver(){
        if Appconstant.profileimg.count <= 0{
            let profileimgPath = Appconstant.GET_PROFILE_IMAGE+"customer_profile_images/"+Appconstant.mobileno+".png"
            
            if let data = NSData(contentsOfURL: NSURL(string:profileimgPath)!){
                let profile_img =  UIImage(data: data)
                self.profileimg.image = profile_img
                self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
                if Appconstant.profileimg.count <= 0{
                    Appconstant.profileimg.append(profile_img!)
                }
                else{
                    Appconstant.profileimg[0] = profile_img!
                }
            }
            else{
                self.activityIndicator.stopAnimating()
                
            }
            
        }
        else{
            self.profileimg.image = Appconstant.profileimg[0]
            self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
        }
    }
    func setvaluestoall(){
        DBHelper().cippyDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
        let databasePath = databaseURL.absoluteString
        let cippyDB = FMDatabase(path: databasePath as String)
        if cippyDB.open() {
            
            let selectSQL = "SELECT * FROM CUSTOMERDETAIL WHERE CUSTOMER_ID=" + Appconstant.customerid
            
            let result:FMResultSet! = cippyDB.executeQuery(selectSQL,
                                                           withArgumentsInArray: nil)
            if (result.next()){
                Appconstant.signuplat = result.stringForColumn("SIGNUPLAT")
                Appconstant.signuplong = result.stringForColumn("SIGNUPLONG")
                profileimgstr = result.stringForColumn("IMAGE_PATH")
                date = result.stringForColumn("DATE_OF_BIRTH")
                let defaults = NSUserDefaults.standardUserDefaults()
                addr1txtField.text = defaults.stringForKey("addr1")
                addr2txtField.text = defaults.stringForKey("addr2")
                citytxtField.text = result.stringForColumn("CITY")
                statetxtField.text = result.stringForColumn("STATE")
                pintxtField.text = result.stringForColumn("PIN")
                if result.stringForColumn("GENDER") == "" || result.stringForColumn("GENDER").isEmpty{
                    genderbtn.setTitle("Gender", forState: .Normal)
                }
                if result.stringForColumn("GENDER") == "M" && !result.stringForColumn("GENDER").isEmpty{
                    genderbtn.setTitle("Male", forState: .Normal)
                    gender = "M"
                }
                else if result.stringForColumn("GENDER") == "F" && !result.stringForColumn("GENDER").isEmpty{
                    genderbtn.setTitle("Female", forState: .Normal)
                    gender = "F"
                }
                else{
                    genderbtn.setTitle("Gender", forState: .Normal)
                }
                
                //                print(result.stringForColumn("SECURITY_QUESTION"))
                //                print(result.stringForColumn("SECURITY_ANSWER"))
                if result.stringForColumn("SECURITY_QUESTION") == nil {
                    
                }
                else{
                    let defaults = NSUserDefaults.standardUserDefaults()
                    let secure_q = defaults.stringForKey("security_q")
                    if secure_q != nil {
                        securequestiontxtField.text = secure_q
                    }
                    if(result.stringForColumn("SECURITY_QUESTION").isEmpty || result.stringForColumn("SECURITY_QUESTION") == "null" || result.stringForColumn("SECURITY_QUESTION") == "nul"){
                    }
                    else{
                        //                        securequestiontxtField.text = result.stringForColumn("SECURITY_QUESTION")
                    }
                    if(result.stringForColumn("SECURITY_ANSWER").isEmpty || result.stringForColumn("SECURITY_ANSWER") == "null"){
                    }
                    else{
                        secureanstxtField.text = result.stringForColumn("SECURITY_ANSWER")
                    }
                }
                
                if date.isEmpty || date == "empty"{
                    dobtxtField.text = "DOB    " + "dd/mm/yy"
                }
                else{
                    let deFormatter = NSDateFormatter()
                    deFormatter.dateFormat = "yyyy-MM-dd"
                    print(date)
                    let startTime = deFormatter.dateFromString(date)!
                    
                    let formatter = NSDateFormatter()
                    formatter.dateFormat = "dd/MM/yy"
                    let date1 = formatter.stringFromDate(startTime)
                    dobtxtField.text = "DOB    " + date1
                }
                if(profileimgstr == "empty" || profileimgstr.isEmpty){
                    
                }
                else{
                    
                    let dataDecoded:NSData = NSData(base64EncodedString: profileimgstr, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
                    let img = UIImage(data: dataDecoded)!
                    //                    profilebtn.setImage(img, forState: UIControlState.Normal)
                    profileimg.image = img
                    self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
                    profileimg.clipsToBounds = true
                    profileimg.layer.borderWidth = 3
                    profileimg.layer.borderColor = UIColor(red: 41.0/255.0, green: 192.0/255.0, blue: 222.0/255.0, alpha: 1).CGColor
                }
            }
            else{
                //   status.text = "Failed to add contact"
                print("Error: \(cippyDB.lastErrorMessage())")
            }
            
        }
        cippyDB.close()
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == addr1txtField{
            addr2txtField.becomeFirstResponder()
        }
        if textField == addr2txtField{
            citytxtField.becomeFirstResponder()
        }
        if textField == citytxtField{
            statetxtField.becomeFirstResponder()
        }
        if textField == statetxtField{
            pintxtField.becomeFirstResponder()
        }
        if textField == pintxtField{
            securequestiontxtField.becomeFirstResponder()
        }
        if textField == securequestiontxtField{
            secureanstxtField.becomeFirstResponder()
        }
        if textField == secureanstxtField{
            saveaction()
        }
        
        return true // We do not want UITextField to insert line-breaks.
    }
    func textFieldShouldBeginEditing(txtfield: UITextField) -> Bool {
        if txtfield == dobtxtField{
            self.toolbarView.hidden = false
        }
        else{
            self.toolbarView.hidden = true
            self.toolbarView.hidden = true
            self.toolbarView.hidden = true
        }
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 880)
        return true
    }
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 880)
        if textFieldToChange == dobtxtField{
            
        }
        else{
            self.toolbarView.hidden = true
        }
        if(textFieldToChange == pintxtField){
            characterCountLimit = 6
        }
        else{
            
            characterCountLimit = 50
            
        }
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
        
    }
    
    func dismissKeyboard(){
            self.toolbarView.hidden = true
            self.view.endEditing(true)
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 700)
    }
    @IBAction func profileimgBtnAction(sender: AnyObject) {
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "Add Photo!",
                                            message: "",
                                            preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            self!.takePhotoFromCamera()
            
            })
        let action1 = UIAlertAction(title: "Choose from Gallery", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            self!.takePhotofromGallery()
            
            })
        
        
        
        
        
        let action4 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
            
        }
        
        alertController?.addAction(action)
        alertController?.addAction(action1)
        alertController?.addAction(action4)
        self.presentViewController(alertController!, animated: true, completion: nil)
        
        
    }
    func takePhotoFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
            imagePicker.allowsEditing = true
            self.presentViewController(imagePicker, animated: true, completion: nil)
            
        }
    }
    func takePhotofromGallery(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .PhotoLibrary
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //            profilebtn.setImage(pickedImage, forState: .Normal)
            profileimg.image = pickedImage
            self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
            profilebtn.layer.cornerRadius = self.profilebtn.frame.size.height - 20 / 2
            profileimg.layer.cornerRadius = self.profileimg.frame.size.height / 2
            profileimg.clipsToBounds = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.FormSheet
            if Appconstant.profileimg.count <= 0{
                Appconstant.profileimg.append(pickedImage)
            }
            else{
                Appconstant.profileimg[0] = pickedImage
            }
            
            let image : UIImage = pickedImage
            let imageData:NSData = UIImageJPEGRepresentation(image,1)!
            strBase64 = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
            
            profilebtn.layer.cornerRadius = profilebtn.frame.height/2
            profilebtn.clipsToBounds = true
            uploadImageToAzure(pickedImage)
            DBHelper().cippyDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
            let databasePath = databaseURL.absoluteString
            let cippyDB = FMDatabase(path: databasePath as String)
            if cippyDB.open() {
                let selectSQL = "SELECT * FROM CUSTOMERDETAIL"
                
                let results:FMResultSet! = cippyDB.executeQuery(selectSQL,
                                                                withArgumentsInArray: nil)
                if (results.next()){
                    
                    let insertSQL = "UPDATE CUSTOMERDETAIL SET IMAGE_PATH = '"+self.strBase64+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                    
                    let result = cippyDB.executeUpdate(insertSQL,
                                                       withArgumentsInArray: nil)
                    if !result {
                        //   status.text = "Failed to add contact"
                        print("Error: \(cippyDB.lastErrorMessage())")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                }
                else{
                    let insertsql = "INSERT INTO CUSTOMERDETAIL (CUSTOMER_ID,DATE_OF_BIRTH,CUSTOMER_BANK,IMAGE_PATH,SIGNUPLAT,SIGNUPLONG) VALUES ('\(Appconstant.customerid)','\("empty")','\("DCB")','"+self.strBase64+"','\(Appconstant.signuplat)','\(Appconstant.signuplong)')"
                    let result = cippyDB.executeUpdate(insertsql,
                                                       withArgumentsInArray: nil)
                    if !result {
                        //   status.text = "Failed to add contact"
                        print("Error: \(cippyDB.lastErrorMessage())")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                    
                }
            }
            cippyDB.close()
            
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func uploadImageToAzure(img: UIImage){
        self.activityIndicator.startAnimating()
        if self.profileimg.image?.imageOrientation == UIImageOrientation.Up {
            
        }
        else{
            UIGraphicsBeginImageContextWithOptions((self.profileimg.image?.size)!, false, (self.profileimg.image?.scale)!)
            self.profileimg.image?.drawInRect(CGRectMake(0, 0, (self.profileimg.image?.size.width)!, (self.profileimg.image?.size.height)!))
            self.profileimg.image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
        }
        
        do{
            let url = Appconstant.GET_PROFILE_IMAGE+"customer_profile_images/"+Appconstant.mobileno+".png";
            print(url)
            let account = try AZSCloudStorageAccount(fromConnectionString: "DefaultEndpointsProtocol=https;AccountName=cippy;AccountKey=gRCxYEHsMUIGEMiHk30ANvAd0kgjS+cAI/G78QKKC+aiPvpoRgRzX8ztiVTaOFpgJRDIGqNBS46Z0v5Z+CFkzA==") //I stored the property in my header file
            let blobClient: AZSCloudBlobClient = account.getBlobClient()
            let blobContainer: AZSCloudBlobContainer = blobClient.containerReferenceFromName("cdn/customer_profile_images")
            blobContainer.createContainerIfNotExistsWithAccessType(AZSContainerPublicAccessType.Container, requestOptions: nil, operationContext: nil) { (NSError, Bool) -> Void in
                let blob: AZSCloudBlockBlob = blobContainer.blockBlobReferenceFromName(Appconstant.mobileno+".png") //If you want a random name, I used let imageName = CFUUIDCreateString(nil, CFUUIDCreate(nil))
                print(Appconstant.mobileno)
                let imageData = UIImageJPEGRepresentation(self.profileimg.image!,1)
                
                blob.uploadFromData(imageData!, completionHandler: {(NSError) -> Void in
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    print("Ok, uploaded !")
                    
                })
                
            }
        }
        catch {
            dispatch_async(dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
            }
            print("very bad")
        }
        
    }
    @IBAction func datepickerBtnAction(sender: AnyObject) {
        
        createDatePickerViewWithAlertController()
    }
    
    @IBAction func dobtxtFieldAction(sender: AnyObject) {
        createDatePickerViewWithAlertController()
    }
    
    
    func createDatePickerViewWithAlertController()
    {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.Date
        var dateString = ""
        if self.date != ""{
            dateString = self.date
        }
        else{
            dateString = "1990-01-01"
        }
        let df = NSDateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let date = df.dateFromString(dateString)
        datePickerView.setDate(date!, animated: false)
        print(datePickerView.frame.size.height)
        dobtxtField.inputView = datePickerView
        toolbarView = UIView(frame: CGRectMake(0,self.view.frame.size.height-datePickerView.frame.size.height-44,self.view.frame.size.width,44))
        toolbarView.backgroundColor = UIColor.lightGrayColor()
        //      let toolBar = UIToolbar(frame: CGRectMake(0,0,self.view.frame.size.width,44))
        let doneButton = UIButton.init(frame: CGRectMake(self.view.frame.size.width - 60,5,50,34))
        doneButton.addTarget(self, action: "Clicked", forControlEvents: UIControlEvents.TouchUpInside)
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        
        //        let cancelButton = UIButton.init(frame: CGRectMake(10,5,70,34))
        //        cancelButton.addTarget(self, action: "Clicked", forControlEvents: UIControlEvents.TouchUpInside)
        //        cancelButton.setTitle("Cancel", forState: UIControlState.Normal)
        
        NSTimer.scheduledTimerWithTimeInterval(0.2, target:self, selector: Selector("timerfunc"), userInfo: nil, repeats: false)
        toolbarView.addSubview(doneButton)
        //        toolbarView.addSubview(cancelButton)
        
        datePickerView.addTarget(self, action: "datePickerValueChanged:", forControlEvents: UIControlEvents.ValueChanged)
        //        let viewDatePicker: UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, 200))
        //        viewDatePicker.backgroundColor = UIColor.clearColor()
        //
        //
        //        self.datePickerView = UIDatePicker(frame: CGRectMake(0, 0, self.view.frame.size.width, 200))
        //        self.datePickerView.datePickerMode = UIDatePickerMode.Date
        //        self.datePickerView.addTarget(self, action: "datePickerValueChanged:", forControlEvents: UIControlEvents.ValueChanged)
        //
        //        viewDatePicker.addSubview(self.datePickerView)
        //        if(UIDevice.currentDevice().systemVersion >= "8.0")
        //        {
        //
        //            let alertController = UIAlertController(title: nil, message: "\n\n\n\n\n\n\n\n", preferredStyle: UIAlertControllerStyle.ActionSheet)
        //
        //            alertController.view.addSubview(viewDatePicker)
        //
        //            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel)
        //                { (action) in
        //                    self.datelbl.text = "          " + "dd/mm/yy"
        //            }
        //
        //            alertController.addAction(cancelAction)
        //
        //            let OKAction = UIAlertAction(title: "Done", style: .Default)
        //                { (action) in
        //
        //
        //            }
        //
        //            alertController.addAction(OKAction)
        //            self.presentViewController(alertController, animated: true){
        //
        //            }
        //        }
        //        else {
        //            let actionSheet = UIActionSheet(title: "\n\n\n\n\n\n\n\n\n\n", delegate:self, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Done")
        //
        //            actionSheet.addSubview(viewDatePicker)
        //            actionSheet.showInView(self.view)
        //        }
        //
    }
    
    func timerfunc(){
        toolbarView.hidden = false
        self.view.addSubview(toolbarView)
        
    }
    func Clicked(){
        
        self.toolbarView.hidden = true
        self.view.endEditing(true)
    }
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let currentdate = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: currentdate)
        
        let year =  components.year
        
        
        print(sender.date)
        let dateFormatter = NSDateFormatter()
        let dateFormatter1 = NSDateFormatter()
        let dateFormatter2 = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter1.dateFormat = "dd/MM/yy"
        dateFormatter2.dateFormat = "yyyy"
        
        print(dateFormatter2.stringFromDate(sender.date))
        let selectdate = Int(dateFormatter2.stringFromDate(sender.date))!
        if(selectdate+18 > Int(year)){
            if !toolbarView.hidden{
                toolbarView.hidden = true
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.presentViewController(Alert().alert("Sorry! The minimum age for using Cippy is 18 years", message: ""),animated: true,completion: nil)
            }
        }
            
            //        let ageComponents = calendar.components(.CalendarUnitYear,
            //            fromDate: dateFormatter1.stringFromDate(sender.date),
            //            toDate: NSDate(),
            //            options: nil)
            //        let age = ageComponents.year
        else{
            dobtxtField.text = "DOB    " + dateFormatter1.stringFromDate(sender.date)
            date = dateFormatter.stringFromDate(sender.date)
        }
        
    }
    
    @IBAction func genderBtnAction(sender: AnyObject) {
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "Gender",
                                            message: "",
                                            preferredStyle: .Alert)
        
        
        let action1 = UIAlertAction(title: "Male", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            self!.genderbtn.setTitle("Male", forState: .Normal)
            self!.gender = "M"
            })
        let action2 = UIAlertAction(title: "Female", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            self!.genderbtn.setTitle("Female", forState: .Normal)
            self!.gender = "F"
            })
        let action3 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            })
        
        alertController?.addAction(action1)
        alertController?.addAction(action2)
        alertController?.addAction(action3)
        self.presentViewController(alertController!, animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func saveBtnAction(sender: AnyObject) {
        saveaction()
    }
    
    func saveaction(){
        if(date.isEmpty){
            self.presentViewController(Alert().alert("Sorry! Seems you forgot to fill DOB..We need you to fill it and try again!", message: ""),animated: true,completion: nil)
        }
        else if(addr1txtField.text!.isEmpty || addr2txtField.text!.isEmpty || citytxtField.text!.isEmpty || statetxtField.text!.isEmpty || pintxtField.text!.isEmpty || securequestiontxtField.text!.isEmpty || secureanstxtField.text!.isEmpty || self.gender == ""){
            self.presentViewController(Alert().alert("Sorry! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else if(pintxtField.text!.characters.count != 6){
            self.presentViewController(Alert().alert("Please Enter a valid Pincode", message: ""),animated: true,completion: nil)
        }
        else{
            //            let description = ";SignUp:"+Appconstant.latitude+","+Appconstant.longitude+";SignIn:"+Appconstant.latitude+","+Appconstant.longitude
            let description = self.securequestiontxtField.text! + ":" + self.secureanstxtField.text! + ";SignUp:" + Appconstant.signuplat + "," + Appconstant.signuplong + ";SignIn:" + Appconstant.latitude + "," + Appconstant.longitude
            
            let profileviewmodel = ProfileViewModel.init(specialDate: date, entityId: Appconstant.customerid, address: self.addr1txtField.text!, address2: self.addr2txtField.text!, city: self.citytxtField.text!, state: self.statetxtField.text!, pincode: self.pintxtField.text!, description: description, gender: self.gender)!
            let serializedjson  = JSONSerializer.toJson(profileviewmodel)
            print(serializedjson)
            activityIndicator.startAnimating()
            sendrequesttoserverForUpdateEntity(Appconstant.BASE_URL+Appconstant.URL_UPDATE_ENTITY, values: serializedjson)
        }
        
    }
    
    
    func sendrequesttoserverForUpdateEntity(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print(error)
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")  // check for http errors
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                if(json["result"].stringValue == "true"){
                    self.saveintoDB()
                    
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        
                    }
                }
            }
        }
        
        task.resume()
        
    }
    
    func saveintoDB(){
        DBHelper().cippyDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
        let databasePath = databaseURL.absoluteString
        let cippyDB = FMDatabase(path: databasePath as String)
        if cippyDB.open() {
            
            let questiontxt = ""
            //            let questiontxt: String = self.securequestiontxtField.text!.stringByReplacingOccurrencesOfString("'", withString: "\'", options: NSStringCompareOptions.LiteralSearch, range: nil)
            
            let selectSQL = "SELECT * FROM CUSTOMERDETAIL"
            
            let results:FMResultSet! = cippyDB.executeQuery(selectSQL,
                                                            withArgumentsInArray: nil)
            if (results.next()){
                let update1 = "UPDATE CUSTOMERDETAIL SET DATE_OF_BIRTH='"+self.date+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update4 = "UPDATE CUSTOMERDETAIL SET CITY='"+self.citytxtField.text!+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update5 = "UPDATE CUSTOMERDETAIL SET STATE='"+self.statetxtField.text!+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update6 = "UPDATE CUSTOMERDETAIL SET PIN='"+self.pintxtField.text!+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update8 = "UPDATE CUSTOMERDETAIL SET SECURITY_ANSWER='"+self.secureanstxtField.text!+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                let update9 = "UPDATE CUSTOMERDETAIL SET GENDER='"+self.gender+"' WHERE CUSTOMER_ID=" + Appconstant.customerid
                
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(self.securequestiontxtField.text!, forKey: "security_q")
                defaults.setObject(self.addr1txtField.text!, forKey: "addr1")
                defaults.setObject(self.addr2txtField.text!, forKey: "addr2")
                let result1 = cippyDB.executeUpdate(update1,
                                                    withArgumentsInArray: nil)
                let result4 = cippyDB.executeUpdate(update4,
                                                    withArgumentsInArray: nil)
                let result5 = cippyDB.executeUpdate(update5,
                                                    withArgumentsInArray: nil)
                let result6 = cippyDB.executeUpdate(update6,
                                                    withArgumentsInArray: nil)
                let result8 = cippyDB.executeUpdate(update8,
                                                    withArgumentsInArray: nil)
                let result9 = cippyDB.executeUpdate(update9,
                                                    withArgumentsInArray: nil)
                
                if (!result1 || !result4 || !result5 || !result6 || !result8 || !result9){
                    
                    print("Error: \(cippyDB.lastErrorMessage())")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        var alertController:UIAlertController?
                        alertController?.view.tintColor = UIColor.blackColor()
                        alertController = UIAlertController(title: "Yey! Your details saved successfully",
                                                            message: "",
                                                            preferredStyle: .Alert)
                        
                        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                            
                            self?.performSegueWithIdentifier("back_to_home", sender: self)
                            
                            
                            })
                        alertController?.addAction(action)
                        self.presentViewController(alertController!, animated: true, completion: nil)
                        
                        
                    }
                }
            }
            else{
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(self.securequestiontxtField.text!, forKey: "security_q")
                defaults.setObject(self.addr1txtField.text!, forKey: "addr1")
                defaults.setObject(self.addr2txtField.text!, forKey: "addr2")
                let insertsql = "INSERT INTO CUSTOMERDETAIL (CUSTOMER_ID,DATE_OF_BIRTH,CUSTOMER_BANK,ADDRESS_LINE_1,ADDRESS_LINE_2,CITY,STATE,PIN,SECURITY_QUESTION,SECURITY_ANSWER,IMAGE_PATH,SIGNUPLAT,SIGNUPLONG) VALUES ('\(Appconstant.customerid)','\(self.date)','\("DCB")','\("")','\("")','\(self.citytxtField.text!)','\(self.statetxtField.text!)','\(self.pintxtField.text!)','\("")','\(self.secureanstxtField.text!)','\(self.strBase64)','\(Appconstant.signuplat)','\(Appconstant.signuplong)')"
                
                let result = cippyDB.executeUpdate(insertsql,
                                                   withArgumentsInArray: nil)
                
                if !result {
                    //   status.text = "Failed to add contact"
                    print("Error: \(cippyDB.lastErrorMessage())")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        var alertController:UIAlertController?
                        alertController?.view.tintColor = UIColor.blackColor()
                        alertController = UIAlertController(title: "Yey! Your details saved successfully",
                                                            message: "",
                                                            preferredStyle: .Alert)
                        
                        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                            
                            self?.performSegueWithIdentifier("back_to_home", sender: self)
                            
                            
                            })
                        alertController?.addAction(action)
                        self.presentViewController(alertController!, animated: true, completion: nil)
                        
                        
                    }
                }
                
            }
        }
    }
    
    @IBAction func forgotpwdBtnAction(sender: AnyObject) {
        self.sendrequesttoserverForForgotPassword(Appconstant.BASE_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
    }
    
    func sendrequesttoserverForForgotPassword(url : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                self.activityIndicator.stopAnimating()
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                self.activityIndicator.stopAnimating()
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            self.activityIndicator.stopAnimating()
            let item = json["result"]
            if(item["success"].stringValue == "true"){
                dispatch_async(dispatch_get_main_queue()) {
                    Appconstant.otp = item["otp"].stringValue
                    self.performSegueWithIdentifier("profile_otp", sender: self)
                }
            }
                
            else{
                dispatch_async(dispatch_get_main_queue()) {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
            }
        }
        
        task.resume()
        
    }
    
    @IBAction func backbtnaction(sender: AnyObject) {
        self.toolbarView.hidden = true
       
    }
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "profile_otp") {
            let nextview = segue.destinationViewController as! OTPViewController
            nextview.fromprofile = true
        }
    }
    
}
