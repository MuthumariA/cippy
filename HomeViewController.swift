//
//  HomeViewController.swift
//  Cippy
//
//  Created by apple on 16/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit
import AZSClient

class HomeViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var sendoraskMoneyBtn: UIButton!
    @IBOutlet weak var transactionview: UIView!
    @IBOutlet weak var addbtn: UIButton!
    @IBOutlet weak var showallbtn: UIButton!
    @IBOutlet weak var transactionbtn1: UIButton!
    @IBOutlet weak var transactionbtn2: UIButton!
    @IBOutlet weak var transactionbtn3: UIButton!
    @IBOutlet weak var transactionamount1lbl: UILabel!
    @IBOutlet weak var transactionamount2lbl: UILabel!
    @IBOutlet weak var transactionamount3lbl: UILabel!
    @IBOutlet weak var transactionname1lbl: UILabel!
    @IBOutlet weak var transactionname2lbl: UILabel!
    @IBOutlet weak var transactionname3lbl: UILabel!
    @IBOutlet weak var profileimgBtn: UIButton!
    @IBOutlet weak var rupeesymbollbl: UILabel!
    @IBOutlet weak var transactionlbl: UILabel!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var balancelbl: UILabel!
    @IBOutlet weak var RechargeorPayBtn: UIButton!
    @IBOutlet weak var splitBtn: UIButton!
    @IBOutlet weak var payatshopBtn: UIButton!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var badgebtn: UIButton!
    @IBOutlet weak var profileimg: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    
    
    
    
    
    let imagePicker = UIImagePickerController()
    var strBase64: String!
    var profileimgstr = ""
    var trans_date = [String]()
    var transactionamt = [String]()
    var beneficiaryname = [String]()
    var balance = [String]()
    var refresh_balnc = 0
    
    
    var fromweb = false
    var alertmsg = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBarHidden = true
    
        initialfunc()
        print(Appconstant.customerid)
        getbalancefromServer(Appconstant.BASE_URL+Appconstant.URL_FETCH_MULTI_BALANCE_INFO+Appconstant.customerid)
        
        getrecenttransaction(Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=3")
        if (Operators.post_operatorName.count == 0){
            getplanlist(Appconstant.BASE_URL+Appconstant.URL_GETOPEARTORS)
        }
        activityIndicator.startAnimating()
        setimagefromserver()
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    func initialfunc(){
        if fromweb{
            dispatch_async(dispatch_get_main_queue()) {
                self.presentViewController(Alert().alert(self.alertmsg, message: ""),animated: true,completion: nil)
            }
        }
        sendoraskMoneyBtn.layer.cornerRadius = 10
        transactionview.layer.cornerRadius = 10
        addbtn.layer.cornerRadius = 10
        showallbtn.layer.cornerRadius = 10
        transactionbtn1.layer.cornerRadius = 10
        transactionbtn2.layer.cornerRadius = 10
        transactionbtn3.layer.cornerRadius = 10
        profileimgBtn.layer.cornerRadius = profileimgBtn.frame.height/2
        profileimg.layer.cornerRadius = profileimg.frame.height/2
        profileimg.clipsToBounds = true
        self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
        rupeesymbollbl.text = "\u{20B9}"
        transactionbtn1.hidden = true
        transactionbtn2.hidden = true
        transactionbtn3.hidden = true
        transactionamount1lbl.hidden = true
        transactionamount2lbl.hidden = true
        transactionamount3lbl.hidden = true
        transactionname1lbl.hidden = true
        transactionname2lbl.hidden = true
        transactionname3lbl.hidden = true
        transactionlbl.hidden = true
        imagePicker.delegate = self
        print(Appconstant.customername)
        dispatch_async(dispatch_get_main_queue()) {
            self.namelbl.text = Appconstant.customername
        }
        transactionbtn1.userInteractionEnabled = false
        transactionbtn2.userInteractionEnabled = false
        transactionbtn3.userInteractionEnabled = false
        let frame = 45 - self.view.frame.size.width/2
        let frame1 = 75 - self.view.frame.size.width/2
        RechargeorPayBtn.imageEdgeInsets = UIEdgeInsetsMake(0,frame, 0, 0)
        RechargeorPayBtn.titleEdgeInsets = UIEdgeInsetsMake(0, frame1, 0, 0)
        badgebtn.userInteractionEnabled = false
        splitBtn.imageEdgeInsets = UIEdgeInsetsMake(0,frame-90, 0, 0)
        splitBtn.titleEdgeInsets = UIEdgeInsetsMake(0, frame1-80, 0, 0)
        payatshopBtn.imageEdgeInsets = UIEdgeInsetsMake(0,frame-48, 0, 0)
        payatshopBtn.titleEdgeInsets = UIEdgeInsetsMake(0, frame1-48, 0, 0)
        profileBtn.imageEdgeInsets = UIEdgeInsetsMake(0,frame-23, 0, 0)
        profileBtn.titleEdgeInsets = UIEdgeInsetsMake(0, frame1-23, 0, 0)
        badgebtn.layer.cornerRadius = self.badgebtn.frame.size.height/2
        let defaults = NSUserDefaults.standardUserDefaults()
//        defaults.setObject(Appconstant.notificationcount, forKey: "badgecount")
        
        profileimg.layer.borderWidth = 3
        profileimg.layer.borderColor = UIColor(red: 41.0/255.0, green: 192.0/255.0, blue: 222.0/255.0, alpha: 1).CGColor
        self.navigationController?.navigationBarHidden = true
        let cippybalance = defaults.stringForKey("cippybalance")
        let badgevalue = defaults.stringForKey("badgecount")
        if(cippybalance != nil){
            balancelbl.text = cippybalance
        }
        print(badgevalue)
        if(badgevalue == nil){
            
        }
        else{
            Appconstant.notificationcount = Int(badgevalue!)!
        }
        if(Appconstant.notificationcount > 0){
            badgebtn.hidden = false
            badgebtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
        }
        else{
            badgebtn.hidden = true
        }
      
    
    }
    
    func setimagefromserver(){
        if Appconstant.profileimg.count <= 0{
            dispatch_async(dispatch_get_main_queue()) {
        let profileimgPath = Appconstant.GET_PROFILE_IMAGE+"customer_profile_images/"+Appconstant.mobileno+".png"
        
        if let data = NSData(contentsOfURL: NSURL(string:profileimgPath)!){
            
            
            
            let profile_img =  UIImage(data: data)
            self.profileimg.image = profile_img
            
            let imageData = UIImageJPEGRepresentation(profile_img!,1)
            self.profileimg.image = UIImage(data: imageData!)
            
            self.activityIndicator.stopAnimating()
            self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
            if Appconstant.profileimg.count <= 0{
                Appconstant.profileimg.append(profile_img!)
            }
            else{
                Appconstant.profileimg[0] = profile_img!
            }
            
        }
        else{
            self.activityIndicator.stopAnimating()
            
                }
            }
//        else{
//            setvaluestoall()
//        }
        }
        else{
            dispatch_async(dispatch_get_main_queue()) {
            self.activityIndicator.stopAnimating()
             self.profileimg.image = Appconstant.profileimg[0]
            self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
            }
        }
    }
    
    func transactiontable(){
        DBHelper().cippyDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
        let databasePath = databaseURL.absoluteString
        let cippyDB = FMDatabase(path: databasePath as String)
        if cippyDB.open() {
        let select = "SELECT * FROM TRANSACTIONS"
        let result4:FMResultSet = cippyDB.executeQuery(select,
            withArgumentsInArray: nil)
        if(result4.next()){
            
        }
        else{
            dispatch_async(dispatch_get_main_queue()) {
                self.gettentransaction(Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=10")
            }
        }
        }
        cippyDB.close()

    }
    
    @IBAction func balanceRefereshBtnAction(sender: AnyObject) {
        refresh_balnc = 1
        getrecenttransaction(Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=3")
    }
    
    
    
    func setvaluestoall(){
        DBHelper().cippyDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
        let databasePath = databaseURL.absoluteString
        let cippyDB = FMDatabase(path: databasePath as String)
        if cippyDB.open() {
            
            let selectSQL = "SELECT * FROM CUSTOMERDETAIL WHERE CUSTOMER_ID=" + Appconstant.customerid
            
            let result:FMResultSet! = cippyDB.executeQuery(selectSQL,
                withArgumentsInArray: nil)
            if (result.next()){
                profileimgstr = result.stringForColumn("IMAGE_PATH")
                if(profileimgstr == "empty" || profileimgstr.isEmpty){
                    
                }
                else{
                    
                let dataDecoded:NSData = NSData(base64EncodedString: profileimgstr, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
                let img = UIImage(data: dataDecoded)!
//                profileimgBtn.setImage(img, forState: UIControlState.Normal)
                    self.profileimg.image = img
                    profileimg.layer.cornerRadius = self.profileimgBtn.frame.size.height / 2
                    self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
                    profileimg.clipsToBounds = true
                    profileimg.layer.borderWidth = 3
                    profileimg.layer.borderColor = UIColor(red: 41.0/255.0, green: 192.0/255.0, blue: 222.0/255.0, alpha: 1).CGColor
                }
            }
            else{
                //   status.text = "Failed to add contact"
                print("Error: \(cippyDB.lastErrorMessage())")
            }
            
        }
        cippyDB.close()
    }
    
    
    @IBAction func profileimgBtnAction(sender: AnyObject) {
        
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "Add Photo!",
            message: "",
            preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            self!.takePhotoFromCamera()
            
            })
        let action1 = UIAlertAction(title: "Choose from Gallery", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            self!.takePhotofromGallery()
            
            })
        
        
        
        
        
        let action4 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
            
        }
        
        alertController?.addAction(action)
        alertController?.addAction(action1)
        alertController?.addAction(action4)
        self.presentViewController(alertController!, animated: true, completion: nil)
        
        
    }
    func takePhotoFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
            imagePicker.allowsEditing = true
            self.presentViewController(imagePicker, animated: true, completion: nil)
            
        }
    }
    func takePhotofromGallery(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .PhotoLibrary
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
            
            
            
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
//            profileimgBtn.setImage(pickedImage, forState: .Normal)
            print(pickedImage)
            self.profileimg.image = pickedImage
            profileimg.layer.cornerRadius = self.profileimgBtn.frame.size.height / 2
            self.profileimg.contentMode = UIViewContentMode.ScaleAspectFill
            print(pickedImage)
            if Appconstant.profileimg.count <= 0{
                Appconstant.profileimg.append(pickedImage)
            }
            else{
                Appconstant.profileimg[0] = pickedImage
            }
            
            profileimgBtn.layer.cornerRadius = profileimgBtn.frame.height/2
            profileimgBtn.clipsToBounds = true
            DBHelper().cippyDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
            let databasePath = databaseURL.absoluteString
            let cippyDB = FMDatabase(path: databasePath as String)
            uploadImageToAzure(pickedImage)
            cippyDB.close()
            
            }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
        
    
    func uploadImageToAzure(img: UIImage){
        self.activityIndicator.startAnimating()
        if self.profileimg.image?.imageOrientation == UIImageOrientation.Up {
            
        }
        else{
            UIGraphicsBeginImageContextWithOptions((self.profileimg.image?.size)!, false, (self.profileimg.image?.scale)!)
            self.profileimg.image?.drawInRect(CGRectMake(0, 0, (self.profileimg.image?.size.width)!, (self.profileimg.image?.size.height)!))
            self.profileimg.image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
        }
        do{
            let url = Appconstant.GET_PROFILE_IMAGE+"customer_profile_images/"+Appconstant.mobileno+".png";
            print(url)
            let account = try AZSCloudStorageAccount(fromConnectionString: "DefaultEndpointsProtocol=https;AccountName=cippy;AccountKey=gRCxYEHsMUIGEMiHk30ANvAd0kgjS+cAI/G78QKKC+aiPvpoRgRzX8ztiVTaOFpgJRDIGqNBS46Z0v5Z+CFkzA==") //I stored the property in my header file
            let blobClient: AZSCloudBlobClient = account.getBlobClient()
            let blobContainer: AZSCloudBlobContainer = blobClient.containerReferenceFromName("cdn/customer_profile_images")
            blobContainer.createContainerIfNotExistsWithAccessType(AZSContainerPublicAccessType.Container, requestOptions: nil, operationContext: nil) { (NSError, Bool) -> Void in
                let blob: AZSCloudBlockBlob = blobContainer.blockBlobReferenceFromName(Appconstant.mobileno+".png") //If you want a random name, I used let imageName = CFUUIDCreateString(nil, CFUUIDCreate(nil))
                print(Appconstant.mobileno)
                
                let imageData = UIImageJPEGRepresentation(self.profileimg.image!,1)
                blob.uploadFromData(imageData!, completionHandler: {(NSError) -> Void in
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    print("Ok, uploaded !")
                    
                })
                
            }
        }
        catch {
            print(error)
            dispatch_async(dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
            }
            print("very bad")
        }
        
    }

    
    
    @IBAction func notificationBtnAction(sender: AnyObject) {
        
    }
    
    @IBAction func logoutBtnAction(sender: AnyObject) {
//        let attributedString = NSAttributedString(string: "Are you sure want to logout?", attributes: [
//            NSFontAttributeName : UIFont.systemFontOfSize(15),                    //your font here,
//            NSForegroundColorAttributeName : UIColor.redColor()
//            ])
        
        let alert = UIAlertController(title: "Are you sure want to logout?", message: "", preferredStyle: UIAlertControllerStyle.Alert)
//        alert.setValue(attributedString, forKey: "attributedTitle")
        alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { alertAction in
            
        }))
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: { alertAction in
            Appconstant.notificationcount = 0
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(Appconstant.notificationcount, forKey: "badgecount")
            defaults.setObject("", forKey: "security_q")
            defaults.setObject("", forKey: "addr1")
            defaults.setObject("", forKey: "addr2")
        DBHelper().cippyDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
        let databasePath = databaseURL.absoluteString
        let cippyDB = FMDatabase(path: databasePath as String)
        
        if cippyDB.open() {
                Appconstant.mainbalance = ""
                let deleteSQL =  "DELETE FROM PROFILE_INFO WHERE CUSTOMER_ID =" + Appconstant.customerid
            
            let deleteSQL2 =  "DELETE FROM CUSTOMERDETAIL WHERE CUSTOMER_ID =" + Appconstant.customerid
            let delete3 = "DELETE FROM TRANSACTIONS"
            let delete4 = "DELETE FROM NOTIFICATION"
            
            let result = cippyDB.executeUpdate(deleteSQL,
                withArgumentsInArray: nil)
            let result2 = cippyDB.executeUpdate(deleteSQL2,
                withArgumentsInArray: nil)
            let result3 = cippyDB.executeUpdate(delete3,
                withArgumentsInArray: nil)
            let result4 = cippyDB.executeUpdate(delete4,
                withArgumentsInArray: nil)
            
            if !result && !result2 && !result3 && !result4{
                    //   status.text = "Failed to add contact"
                    print("Error: \(cippyDB.lastErrorMessage())")
                }
                else{
                dispatch_async(dispatch_get_main_queue()) {
                Appconstant.customerid = ""
                Appconstant.mobileno = ""
                Appconstant.pwd = ""
                Appconstant.firstname = ""
                Appconstant.lastname = ""
                Appconstant.email = ""
                Appconstant.otp = ""
                Appconstant.customername = ""
                
                Appconstant.dummycustomer = false
                Appconstant.newcustomer = false
                Appconstant.mainbalance = ""
                Appconstant.notificationcount = 0
                Appconstant.gcmid = ""
                Appconstant.Url = ""
                Appconstant.profileimg.removeAll()
                Appconstant.latitude = ""
                Appconstant.longitude = ""
                Appconstant.signuplat = ""
                Appconstant.signuplong = ""
                Appconstant.unreadcount = 0
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.setObject("0", forKey: "badgecount")
                    defaults.setObject("0.00", forKey: "cippybalance")
                    self.performSegueWithIdentifier("home_initial", sender: self)
                    }
            }
        }
        cippyDB.close()
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func getbalancefromServer(url: String){
        
            let request = NSMutableURLRequest(URL: NSURL(string: url)!)
            request.HTTPMethod = "GET"
            request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
            request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
                { data, response, error in
                    guard error == nil && data != nil else {                                                          // check for fundamental networking error
                        if Reachability.isConnectedToNetwork() == true {
                        } else {
                            print("Internet connection FAILED")
                            self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                            
                        }
                        return
                    }
                    
                    if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        print("response = \(response)")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        }
                    }
                    else{
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                    let json = JSON(data: data!)
                    
                    for item in json["result"].arrayValue{
                        dispatch_async(dispatch_get_main_queue()) {
                            let balance_two_decimal = String(format: "%.2f", item["balance"].doubleValue)
                            let checkbalancedecimal = balance_two_decimal.componentsSeparatedByString(".")
                            if(checkbalancedecimal[1].characters.count == 1){
                                self.balancelbl.text = balance_two_decimal + "0"
                                Appconstant.mainbalance = self.balancelbl.text!
                            }
                            else{
                            self.balancelbl.text = balance_two_decimal
                                Appconstant.mainbalance = self.balancelbl.text!
                                
                                let defaults = NSUserDefaults.standardUserDefaults()
                                defaults.setObject(Appconstant.mainbalance, forKey: "cippybalance")
                            }
                        }
                    }
                    if(self.refresh_balnc == 1){
                        self.refresh_balnc = 0
                        dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("So Fresh! Balance in your wallet is refreshed.", message: ""),animated: true,completion: nil)
                        }
                    }
                        
                }
            }
        
            task.resume()
            
        
        }
    
    func getrecenttransaction(url: String){
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                    }
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                self.trans_date.removeAll()
                self.transactionamt.removeAll()
                self.beneficiaryname.removeAll()
                for item1 in json["result"].arrayValue{
                    let item = item1["transaction"]
                   
                    let balance_two_decimal = String(format: "%.2f", item["amount"].doubleValue)
                        let amount = balance_two_decimal.componentsSeparatedByString(".")
                    if(amount[1].characters.count == 1){
                        let finalamount = balance_two_decimal + "0"
                        self.transactionamt.append(finalamount)
                    }
                    else{
                        self.transactionamt.append(balance_two_decimal)
                    }
                    
                    
                        self.beneficiaryname.append(item["beneficiaryName"].stringValue)
                        let date = NSDate(timeIntervalSince1970: item["time"].doubleValue/1000.0)
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "dd MMM"
                    dateFormatter.timeZone = NSTimeZone(name: "UTC")
                    let dateString = dateFormatter.stringFromDate(date)
                    if(!dateString.isEmpty){
                    let datearray = dateString.componentsSeparatedByString(" ")
                        if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                            let correctdate = datearray[0] + "st " + datearray[1]
                            self.trans_date.append(correctdate)
                        }
                        else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                            let correctdate = datearray[0] + "nd " + datearray[1]
                            self.trans_date.append(correctdate)
                        }
                        else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                            let correctdate = datearray[0] + "rd " + datearray[1]
                            self.trans_date.append(correctdate)
                        }
                        else{
                            let correctdate = datearray[0] + "th " + datearray[1]
                            self.trans_date.append(correctdate)
                        }
                    
                    }
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.setvaluefortransaction()
                }
                
              self.transactiontable()
        }
        
        task.resume()
    }
    func setvaluefortransaction(){
        if(self.trans_date.count == 0){
            self.transactionlbl.hidden = false
            self.transactionbtn1.hidden = true
            self.transactionbtn2.hidden = true
            self.transactionbtn3.hidden = true
            self.transactionamount1lbl.hidden = true
            self.transactionamount2lbl.hidden = true
            self.transactionamount3lbl.hidden = true
            self.transactionname1lbl.hidden = true
            self.transactionname2lbl.hidden = true
            self.transactionname3lbl.hidden = true
        }
        else{
            self.transactionlbl.hidden = true
            self.transactionbtn1.hidden = false
            self.transactionbtn2.hidden = false
            self.transactionbtn3.hidden = false
            self.transactionamount1lbl.hidden = false
            self.transactionamount2lbl.hidden = false
            self.transactionamount3lbl.hidden = false
            self.transactionname1lbl.hidden = false
            self.transactionname2lbl.hidden = false
            self.transactionname3lbl.hidden = false
            self.transactionbtn1.setTitle(self.trans_date[0], forState: .Normal)
            self.transactionamount1lbl.text = self.transactionamt[0] + " @"
            self.transactionname1lbl.text = self.beneficiaryname[0]
            
            if(self.trans_date.count == 1){
                self.transactionbtn2.hidden = true
                self.transactionamount2lbl.hidden = true
                self.transactionname2lbl.hidden = true
            }
            else{
                self.transactionbtn2.setTitle(self.trans_date[1], forState: .Normal)
                self.transactionamount2lbl.text = self.transactionamt[1] + " @"
                self.transactionname2lbl.text = self.beneficiaryname[1]
            }
            
            if(self.trans_date.count < 3){
                self.transactionbtn3.hidden = true
                self.transactionamount3lbl.hidden = true
                self.transactionname3lbl.hidden = true
            }
            else{
                self.transactionbtn3.setTitle(self.trans_date[2], forState: .Normal)
                self.transactionamount3lbl.text = self.transactionamt[2] + " @"
                self.transactionname3lbl.text = self.beneficiaryname[2]
            }
        }
        if(refresh_balnc == 1){
            getbalancefromServer(Appconstant.BASE_URL+Appconstant.URL_FETCH_MULTI_BALANCE_INFO+Appconstant.customerid)
        }
    }
    
    func gettentransaction(url: String){
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                    }
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                
                self.transactionamt.removeAll()
                self.trans_date.removeAll()
                self.beneficiaryname.removeAll()
                var i = 0
                for item1 in json["result"].arrayValue{
                    let item = item1["transaction"]
                    
                    
                    
                    
                    
                    let balance_two_decimal = String(format: "%.2f", item["amount"].doubleValue)
                    let amount = balance_two_decimal.componentsSeparatedByString(".")
                    if(amount[1].characters.count == 1){
                        let finalamount = balance_two_decimal + "0"
                        self.transactionamt.append(finalamount)
                    }
                    else{
                        self.transactionamt.append(balance_two_decimal)
                    }
                    
                    let mainbalance_two_decimal = String(format: "%.2f", item["balance"].doubleValue)
                    let mainamount = mainbalance_two_decimal.componentsSeparatedByString(".")
                    if(mainamount[1].characters.count == 1){
                        let finalamount = balance_two_decimal + "0"
                        self.balance.append(finalamount)
                    }
                    else{
                        self.balance.append(mainbalance_two_decimal)
                    }
                    
                    
                    let date = NSDate(timeIntervalSince1970: item["time"].doubleValue/1000.0)
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "dd MMM"
                    dateFormatter.timeZone = NSTimeZone(name: "UTC")
                    let dateString = dateFormatter.stringFromDate(date)
                    if(!dateString.isEmpty){
                        let datearray = dateString.componentsSeparatedByString(" ")
                        if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                            let correctdate = datearray[0] + "st " + datearray[1]
                            self.trans_date.append(correctdate)
                        }
                        else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                            let correctdate = datearray[0] + "nd " + datearray[1]
                            self.trans_date.append(correctdate)
                        }
                        else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                            let correctdate = datearray[0] + "rd " + datearray[1]
                            self.trans_date.append(correctdate)
                        }
                        else{
                            let correctdate = datearray[0] + "th " + datearray[1]
                            self.trans_date.append(correctdate)
                        }
                        
                    }
                    let matches = self.matchesForRegexInText("[0-9]", text: item["description"].stringValue)
                    let desc = matches.joinWithSeparator("")
                    DBHelper().cippyDB()
                    let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
                    let databasePath = databaseURL.absoluteString
                    let cippyDB = FMDatabase(path: databasePath as String)
                    if cippyDB.open() {
                        
                        let insert = "INSERT INTO TRANSACTIONS (AMOUNT,BENEFICIARY_ID,TRANSACTION_TYPE,TYPE,TIME,TRANSACTION_STATUS,TX_REF,BENEFICIARY_NAME,DESCRIPTION,OTHER_PARTY_NAME,OTHER_PARTY_ID,TXN_ORIGIN) VALUES"
                        let value0 =  "('"+self.transactionamt[i]+"','\(item["beneficiaryId"].stringValue)','\(item["transactionType"].stringValue)','\(item["type"].stringValue)',"
                        let value1 = "'"+self.trans_date[i]+"','\(item["transactionStatus"].stringValue)','\(item["txRef"].stringValue)','\(item["beneficiaryName"].stringValue)',"
                        let value2 = "'\(desc)','\(item["otherPartyName"].stringValue)','\(item["otherPartyId"].stringValue)','\(item["txnOrigin"].stringValue)')"
                        let insertsql = insert+value0+value1+value2
                        let result = cippyDB.executeUpdate(insertsql,
                            withArgumentsInArray: nil)
                        
                        if !result {
                            //   status.text = "Failed to add contact"
                            print("Error: \(cippyDB.lastErrorMessage())")
                            dispatch_async(dispatch_get_main_queue()) {
                                self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                                
                            }
                        }
                        cippyDB.close()
                    }
                    i++
                    
                }
            }
        
        task.resume()
    }
    
    
    func matchesForRegexInText(regex: String, text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text,
                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    
    
    func getplanlist(url: String){
        print(url)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                }
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            
            for item in json["result"].arrayValue{
                if(item["operatorType"].stringValue == "PREPAID"){
                    Operators.pre_operatorCode.append(item["operatorCode"].stringValue)
                    Operators.pre_operatorName.append(item["operatorName"].stringValue)
                    Operators.pre_operatorId.append(item["operatorId"].stringValue)
                    Operators.pre_operatorType.append(item["operatorType"].stringValue)
                    Operators.pre_special.append(item["special"].boolValue)
                }
                else if(item["operatorType"].stringValue == "POSTPAID"){
                    Operators.post_operatorCode.append(item["operatorCode"].stringValue)
                    Operators.post_operatorName.append(item["operatorName"].stringValue)
                    Operators.post_operatorId.append(item["operatorId"].stringValue)
                    Operators.post_operatorType.append(item["operatorType"].stringValue)
                    Operators.post_special.append(item["special"].boolValue)
                }
                else if(item["operatorType"].stringValue == "DTH"){
                    Operators.dth_operatorCode.append(item["operatorCode"].stringValue)
                    Operators.dth_operatorName.append(item["operatorName"].stringValue)
                    Operators.dth_operatorId.append(item["operatorId"].stringValue)
                    Operators.dth_operatorType.append(item["operatorType"].stringValue)
                    Operators.dth_special.append(item["special"].boolValue)
                }
                
                
                if (Operators.pre_operatorId.count > 0 || Operators.post_operatorId.count > 0 || Operators.dth_operatorId.count > 0){
                    DBHelper().cippyDB()
                    let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
                    let databasePath = databaseURL.absoluteString
                    let cippyDB = FMDatabase(path: databasePath as String)
                    if cippyDB.open() {
                        if !Operators.deleteDB{
                            Operators.deleteDB = true
                            let delete = "DELETE FROM OPERATORS"
                            
                            let delresult = cippyDB.executeUpdate(delete,
                                                                  withArgumentsInArray: nil)
                            if !delresult {
                                //   status.text = "Failed to add contact"
                                print("Error: \(cippyDB.lastErrorMessage())")
                            }
                        }
                        let insertsql = "INSERT INTO OPERATORS (OPERATORCODE,OPERATORNAME,OPERATORID,OPERATORTYPE,SPECIAL) VALUES ('\(item["operatorCode"].stringValue)','\(item["operatorName"].stringValue)','\(item["operatorId"].stringValue)','\(item["operatorType"].stringValue)','\(item["special"].boolValue)')"
                        
                        let result = cippyDB.executeUpdate(insertsql,
                                                           withArgumentsInArray: nil)
                        
                        if !result {
                            //   status.text = "Failed to add contact"
                            print("Error: \(cippyDB.lastErrorMessage())")
                            
                        }
                    }
                }
                
            }
            if Operators.pre_operatorId.count == 0{
                DBHelper().cippyDB()
                let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
                let databasePath = databaseURL.absoluteString
                let cippyDB = FMDatabase(path: databasePath as String)
                if cippyDB.open() {
                    let selectSQL = "SELECT * FROM OPERATORS"
                    
                    let result:FMResultSet! = cippyDB.executeQuery(selectSQL,
                                                                   withArgumentsInArray: nil)
                    
                    while(result.next()){
                        //                        amount.append(result.stringForColumn("AMOUNT"))
                        if(result.stringForColumn("OPERATORTYPE") == "PREPAID"){
                            Operators.pre_operatorCode.append(result.stringForColumn("OPERATORCODE"))
                            Operators.pre_operatorName.append(result.stringForColumn("OPERATORNAME"))
                            Operators.pre_operatorId.append(result.stringForColumn("OPERATORID"))
                            Operators.pre_operatorType.append(result.stringForColumn("OPERATORTYPE"))
                            Operators.pre_special.append(result.boolForColumn("SPECIAL"))
                        }
                        else if(result.stringForColumn("OPERATORTYPE") == "POSTPAID"){
                            Operators.post_operatorCode.append(result.stringForColumn("OPERATORCODE"))
                            Operators.post_operatorName.append(result.stringForColumn("OPERATORNAME"))
                            Operators.post_operatorId.append(result.stringForColumn("OPERATORID"))
                            Operators.post_operatorType.append(result.stringForColumn("OPERATORTYPE"))
                            Operators.post_special.append(result.boolForColumn("SPECIAL"))
                        }
                        else if(result.stringForColumn("OPERATORTYPE") == "DTH"){
                            Operators.dth_operatorCode.append(result.stringForColumn("OPERATORCODE"))
                            Operators.dth_operatorName.append(result.stringForColumn("OPERATORNAME"))
                            Operators.dth_operatorId.append(result.stringForColumn("OPERATORID"))
                            Operators.dth_operatorType.append(result.stringForColumn("OPERATORTYPE"))
                            Operators.dth_special.append(result.boolForColumn("SPECIAL"))
                        }
                        
                    }
                }
            }
        }
        
        task.resume()
        
        
    }
    

    
    

    
//    @IBAction func splitbillBtnAction(sender: AnyObject) {
//        dispatch_async(dispatch_get_main_queue()) {
//            self.presentViewController(Alert().alert("Coming Soon...", message: ""),animated: true,completion: nil)
//            
//        }
//    }
    
    
    
}










