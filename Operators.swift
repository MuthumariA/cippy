//
//  Operators.swift
//  Cippy
//
//  Created by Vertace on 06/04/17.
//  Copyright © 2017 vertace. All rights reserved.
//

import Foundation
import UIKit


struct Operatorss{
    static var pre_operatorName = [String]()
    static var pre_operatorCode = [String]()
    static var pre_operatorType = [String]()
    static var pre_special = [Bool]()
    static var pre_minimumbillamt = [String]()
    static var pre_auth1 = [String]()
    static var pre_auth2 = [String]()
    static var pre_auth3 = [String]()
    
    static var post_operatorName = [String]()
    static var post_operatorCode = [String]()
    static var post_operatorType = [String]()
    static var post_special = [Bool]()
    static var post_minimumbillamt = [String]()
    static var post_auth1 = [String]()
    static var post_auth2 = [String]()
    static var post_auth3 = [String]()
    
    static var dth_operatorName = [String]()
    static var dth_operatorCode = [String]()
    static var dth_operatorType = [String]()
    static var dth_special = [Bool]()
    static var dth_minimumbillamt = [String]()
    static var dth_auth1 = [String]()
    static var dth_auth2 = [String]()
    static var dth_auth3 = [String]()
    
    static var landline_operatorName = [String]()
    static var landline_operatorCode = [String]()
    static var landline_operatorType = [String]()
    static var landline_special = [Bool]()
    static var landline_minimumbillamt = [String]()
    static var landline_auth1 = [String]()
    static var landline_auth2 = [String]()
    static var landline_auth3 = [String]()
    
    static var insurance_operatorName = [String]()
    static var insurance_operatorCode = [String]()
    static var insurance_operatorType = [String]()
    static var insurance_special = [Bool]()
    static var insurance_minimumbillamt = [String]()
    static var insurance_auth1 = [String]()
    static var insurance_auth2 = [String]()
    static var insurance_auth3 = [String]()
    
    static var electricity_operatorName = [String]()
    static var electricity_operatorCode = [String]()
    static var electricity_operatorType = [String]()
    static var electricity_special = [Bool]()
    static var electricity_minimumbillamt = [String]()
    static var electricity_auth1 = [String]()
    static var electricity_auth2 = [String]()
    static var electricity_auth3 = [String]()
    
    static var gas_operatorName = [String]()
    static var gas_operatorCode = [String]()
    static var gas_operatorType = [String]()
    static var gas_special = [Bool]()
    static var gas_minimumbillamt = [String]()
    static var gas_auth1 = [String]()
    static var gas_auth2 = [String]()
    static var gas_auth3 = [String]()
    
    static var tax_operatorName = [String]()
    static var tax_operatorCode = [String]()
    static var tax_operatorType = [String]()
    static var tax_special = [Bool]()
    static var tax_minimumbillamt = [String]()
    static var tax_auth1 = [String]()
    static var tax_auth2 = [String]()
    static var tax_auth3 = [String]()
    
    static var corporate_operatorName = [String]()
    static var corporate_operatorCode = [String]()
    static var corporate_operatorType = [String]()
    static var corporate_special = [Bool]()
    static var corporate_minimumbillamt = [String]()
    static var corporate_auth1 = [String]()
    static var corporate_auth2 = [String]()
    static var corporate_auth3 = [String]()
    
    static var water_operatorName = [String]()
    static var water_operatorCode = [String]()
    static var water_operatorType = [String]()
    static var water_special = [Bool]()
    static var water_minimumbillamt = [String]()
    static var water_auth1 = [String]()
    static var water_auth2 = [String]()
    static var water_auth3 = [String]()
}





