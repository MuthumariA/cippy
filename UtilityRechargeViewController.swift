//
//  UtilityRechargeViewController.swift
//  Cippy
//
//  Created by Vertace on 05/04/17.
//  Copyright © 2017 vertace. All rights reserved.
//

import UIKit

class UtilityRechargeViewController: UIViewController , UITextFieldDelegate{

    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var badgebtn: UIButton!
    @IBOutlet weak var providerbtn: UIButton!
    @IBOutlet weak var textfield1: UITextField!
    @IBOutlet weak var textfield2: UITextField!
    @IBOutlet weak var textfield3: UITextField!
    @IBOutlet weak var textfiled4: UITextField!
    @IBOutlet weak var rechargebtn: UIButton!
    @IBOutlet weak var rechargeconstrain: NSLayoutConstraint!    
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    
    
    var titlename = ""
    var placeholder1 = ""
    var placeholder2 = ""
    var placeholder3 = ""
    var placeholder4 = ""
    
    var operatorName = [String]()
    var operatorCode = [String]()
    var operatorType = [String]()
    var special = [String]()
    var minimumbillamt = [String]()
    var auth1 = [String]()
    var auth2 = [String]()
    var auth3 = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titlelbl.text = self.titlename
        // Do any additional setup after loading the view.
        settextfield()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func settextfield(){
        
        label3.hidden = true
        label4.hidden = true
        textfield3.hidden = true
        textfiled4.hidden = true
        
    }
   

}
