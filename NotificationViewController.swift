//
//  NotificationViewController.swift
//  Cippy
//
//  Created by apple on 08/12/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var notificationlbl: UILabel!
    @IBOutlet weak var addmoneybtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var refreshbtn: UIButton!
    

    var amount = [String]()
    var fromid = [String]()
    var toid = [String]()
    var time = [String]()
    var isread = [Bool]()
    var phonenumber = [String]()
    var temp_phoneno = [String]()
    var request_amt_to_us = [Bool]()
    var entityid = [String]()
    var count = 0
    var approveordecline = [String]()
    var fundnumber = [String]()
    var temp_amount = [String]()
    var fromotp = false
    var index = 0
    var fundno = ""
    var status = [String]()
    var characterCountLimit = 4
    var badgecount = 0
    var swipe = true
    var passwordtxt = ""
//    var refresh_btn = true
    var refreshControl = UIRefreshControl()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        refreshControl.addTarget(self, action: "refreshactioncall", forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(refreshControl)
        
        if fromotp{
            tableView.hidden = false
            notificationlbl.text = "Welcome to your Notifications.\nWe will remind you about your friend's & your pending actions.\nBut before that - you go try out the Cippy's awesome features..."
            let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
            addmoneybtn.titleLabel?.attributedText = NSAttributedString(string: "+ Add Money", attributes: underlineAttribute)
            addmoneybtn.hidden = false
            notificationlbl.hidden = true
            approverequestAPI()
        }
        else{
            initializefunc()
            if Appconstant.updatenotification{
                refreshaction()
            }
            else{
                calldatabase()
            }
//            refreshaction()
        }
    }
    
    func initializefunc(){
        notificationlbl.text = "Welcome to your Notifications.\nWe will remind you about your friend's & your pending actions.\nBut before that - you go try out the Cippy's awesome features..."
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        addmoneybtn.titleLabel?.attributedText = NSAttributedString(string: "+ Add Money", attributes: underlineAttribute)
        
    }
    func refresh(sender:AnyObject) {
        // Code to refresh table view
        //pageNo=1&pageSize=10
        
        refreshaction()
        
        
    }
    
    func calldatabase(){
        DBHelper().cippyDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
        let databasePath = databaseURL.absoluteString
        let cippyDB = FMDatabase(path: databasePath as String)
        if cippyDB.open() {
            badgecount = 0
            let selectSQL = "SELECT * FROM NOTIFICATION"
            
            let result:FMResultSet! = cippyDB.executeQuery(selectSQL,
                withArgumentsInArray: nil)
    
            while(result.next()){
                amount.append(result.stringForColumn("AMOUNT"))
                time.append(result.stringForColumn("DATE"))
//                isread.append(result.boolForColumn("ISREAD"))
                phonenumber.append(result.stringForColumn("PHONENUMBER"))
                print(result.intForColumn("REQ_TO_US"))
                if result.intForColumn("REQ_TO_US") == 1{
                    request_amt_to_us.append(true)
                    if result.stringForColumn("APPROVE_DECLINE") != "true"
                    {
                        badgecount++
                    }
                    isread.append(false)
                }
                else{
                    request_amt_to_us.append(false)
                    isread.append(true)
                }
                print(result.stringForColumn("APPROVE_DECLINE"))
                approveordecline.append(result.stringForColumn("APPROVE_DECLINE"))
                fundnumber.append(result.stringForColumn("FUND_REQ_NUMBER"))
                status.append(result.stringForColumn("STATUS"))
            }
            print(approveordecline)
            print(status)
            Appconstant.notificationcount = self.badgecount
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(Appconstant.notificationcount, forKey: "badgecount")
            if(self.amount.count == 0){
                addmoneybtn.hidden = true
                tableView.hidden = true
                notificationlbl.hidden = false
                refreshaction()
//                self.getNotificationFromServer(Appconstant.BASE_URL+Appconstant.URL_FUNDREQUEST+Appconstant.customerid)
            }
            else{
                addmoneybtn.hidden = false
                tableView.hidden = false
                notificationlbl.hidden = true
                self.tableView.reloadData()
            }
        }
        print(phonenumber)
        cippyDB.close()
    }

    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell!
        
        let desc_lbl = cell.viewWithTag(1) as! UILabel
        let timelbl = cell.viewWithTag(2) as! UILabel
        let approvebtn = cell.viewWithTag(3) as! UIButton
        let declinebtn = cell.viewWithTag(4) as! UIButton
        let statuslbl = cell.viewWithTag(10) as! UILabel
        statuslbl.hidden = true
        print(indexPath.row)
        print(amount)
        print(phonenumber)
        if(approveordecline[indexPath.row] == "true"){
            approvebtn.hidden = true
            declinebtn.hidden = true
            statuslbl.hidden = false
            timelbl.text = self.time[indexPath.row]
            desc_lbl.text = self.phonenumber[indexPath.row] + " has requested for: ₹" + self.amount[indexPath.row]
            dispatch_async(dispatch_get_main_queue()) {
//                self.activityIndicator.stopAnimating()
            }
            if status[indexPath.row] == "APPROVED"{
                statuslbl.text = "APPROVED"
                statuslbl.textColor = UIColor(red: 128.0/255.0, green: 187.0/255.0, blue: 65.0/255.0, alpha: 1)
                
            }
            else{
                statuslbl.text = "DECLINED"
                statuslbl.textColor = UIColor.redColor()
            }
            
//            if isread[indexPath.row]{
                cell.backgroundColor = UIColor.whiteColor()
//            }
//            else{
//                cell.backgroundColor = UIColor(red: 229.0/255.0, green: 238.0/255.0, blue: 255.0/255.0, alpha: 1)
//            }
        }
        else{
        if request_amt_to_us[indexPath.row]{
            approvebtn.hidden = false
            declinebtn.hidden = false
            cell.backgroundColor = UIColor(red: 229.0/255.0, green: 238.0/255.0, blue: 255.0/255.0, alpha: 1)
            desc_lbl.text = self.phonenumber[indexPath.row] + " has requested for: ₹" + self.amount[indexPath.row]
        }
        else{
            approvebtn.hidden = true
            declinebtn.hidden = true
            cell.backgroundColor = UIColor.whiteColor()
            desc_lbl.text = "You have requested ₹" + self.amount[indexPath.row] + " from" + self.phonenumber[indexPath.row]
            if isread[indexPath.row]{
                cell.backgroundColor = UIColor.whiteColor()
            }
            else{
                self.isread[indexPath.row] = true
                if Appconstant.unreadcount > 0 {
                    Appconstant.unreadcount = Appconstant.unreadcount - 1
                }
                cell.backgroundColor = UIColor(red: 229.0/255.0, green: 238.0/255.0, blue: 255.0/255.0, alpha: 1)
            }
        }
            
        timelbl.text = self.time[indexPath.row]
        approvebtn.layer.cornerRadius = 19
        approvebtn.layer.borderWidth = 3
        approvebtn.layer.borderColor = UIColor(red: 128.0/255.0, green: 187.0/255.0, blue: 65.0/255.0, alpha: 1).CGColor
        declinebtn.layer.cornerRadius = 19
        declinebtn.layer.borderWidth = 3
        declinebtn.layer.borderColor = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1).CGColor
        }
        cell.layer.borderColor = UIColor(red: 203.0/255.0, green: 203.0/255.0, blue: 203.0/255.0, alpha: 1).CGColor
      
        cell.layer.borderWidth = 0.5
        cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(self.amount.count)
        return self.amount.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let selectionColor = UIView() as UIView
        selectionColor.layer.borderWidth = 1
        selectionColor.layer.borderColor = UIColor.clearColor().CGColor
        selectionColor.backgroundColor = UIColor.clearColor()
        cell.selectedBackgroundView = selectionColor
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        //        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if request_amt_to_us[indexPath.row]{
            return 115
        }
        else{
            return 80
        }
        return 80
    }

    
    @IBAction func refreshBtnAction(sender: AnyObject) {
//        if refresh_btn{
//            refresh_btn = false
        refreshbtn.userInteractionEnabled = false
        refreshaction()
//        }
    }
    func refreshactioncall(){
//        tableView.translatesAutoresizingMaskIntoConstraints = true
        if swipe{
            swipe = false
            refreshaction()
        }
        else{
            self.refreshControl.endRefreshing()
        }
    }
    func refreshaction(){
        view.userInteractionEnabled = false
        self.activityIndicator.startAnimating()
        if tableView.hidden {
            addmoneybtn.hidden = false
            tableView.hidden = false
            notificationlbl.hidden = true
        }
    
        self.count = 0
        characterCountLimit = 4
        badgecount = 0
        index = 0
        DBHelper().cippyDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
        let databasePath = databaseURL.absoluteString
        let cippyDB = FMDatabase(path: databasePath as String)
        
        if cippyDB.open() {
            
            let delete = "DELETE FROM NOTIFICATION"
            let result = cippyDB.executeUpdate(delete,
                withArgumentsInArray: nil)
            if !result {
                //   status.text = "Failed to add contact"
                print("Error: \(cippyDB.lastErrorMessage())")
            }
    
        self.getNotificationFromServer(Appconstant.BASE_URL+Appconstant.URL_FUNDREQUEST+Appconstant.customerid)
        }
    }
    
    func getNotificationFromServer(url: String){
        activityIndicator.startAnimating()
        print(Appconstant.BASE_URL+Appconstant.URL_FUNDREQUEST+Appconstant.customerid)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {
                    dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                    }
                    self.view.userInteractionEnabled = true
                        print("Internet connection FAILED")
                     dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    self.view.userInteractionEnabled = true
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
               
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                self.fromid.removeAll()
                self.toid.removeAll()
                self.time.removeAll()
                self.isread.removeAll()
                self.request_amt_to_us.removeAll()
                self.entityid.removeAll()
                self.approveordecline.removeAll()
                self.fundnumber.removeAll()
                self.status.removeAll()
                self.badgecount = 0
                self.amount.removeAll()
                for item in json["result"].arrayValue{
                    if(item["status"].stringValue == "CREATED"){
                    let date = NSDate(timeIntervalSince1970: item["created"].doubleValue/1000.0)
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "d/MM/yyyy HH:mm"
//                    dateFormatter.timeZone = NSTimeZone(name: "UTC")
                    let dateString = dateFormatter.stringFromDate(date)
                    
                    self.time.append(dateString)
                    self.fromid.append(item["fromEntityId"].stringValue)
                    self.toid.append(item["toEntityId"].stringValue)
                    
                    self.fundnumber.append(item["fundRequestNumber"].stringValue)
                    self.status.append("")
                    self.approveordecline.append("false")

                    if(item["fromEntityId"].stringValue == Appconstant.customerid){
                        self.request_amt_to_us.append(true)
                        self.isread.append(false)
                        self.badgecount++
                    }
                    else if(item["toEntityId"].stringValue == Appconstant.customerid){
                        self.request_amt_to_us.append(false)
                        self.isread.append(true)
                    }
                    let balance_two_decimal = String(format: "%.2f", item["amount"].doubleValue)
                    let amt = balance_two_decimal.componentsSeparatedByString(".")
                    if(amt[1].characters.count == 1){
                        let finalamount = balance_two_decimal + "0"
                        self.amount.append(finalamount)
//                        self.temp_amount.append(finalamount)
                    }
                    else{
                        self.amount.append(balance_two_decimal)
//                        self.temp_amount.append(balance_two_decimal)
                    }
                    if(item["fromEntityId"].stringValue == Appconstant.customerid){
                        self.entityid.append(item["toEntityId"].stringValue)
                    }
                    else if(item["toEntityId"].stringValue == Appconstant.customerid){
                        self.entityid.append(item["fromEntityId"].stringValue)
                    }
                  }
                }
                if self.isread.count>0 && self.isread.count>=Appconstant.unreadcount{
                    for(var i=0; i<Appconstant.unreadcount; i++){
                        self.isread[i] = false
                    }
                }
                
                Appconstant.notificationcount = self.badgecount
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(Appconstant.notificationcount, forKey: "badgecount")
                
                self.getmerchantdetails()

        }
        
        task.resume()
    }
    
    func getmerchantdetails(){
        if entityid.count == 0{
            dispatch_async(dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                self.tableView.hidden = true
                self.notificationlbl.hidden = false
                self.view.userInteractionEnabled = true
            }
        }
        if entityid.count > 0 {
            print(Appconstant.BASE_URL+Appconstant.URL_FETCH_MERCHANT_DETAILS+self.entityid[count])
            self.getMerchantDetailFromServer(Appconstant.BASE_URL+Appconstant.URL_FETCH_MERCHANT_DETAILS+entityid[count])
        }
        else{
            self.tableView.hidden = true
            self.notificationlbl.hidden = false
            self.view.userInteractionEnabled = true
            dispatch_async(dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
            }
        }
       
    }
    
    func getMerchantDetailFromServer(url1: String){
        
        let request1 = NSMutableURLRequest(URL: NSURL(string: url1)!)
        print(request1)
        request1.HTTPMethod = "GET"
        request1.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request1.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request1.addValue("application/json", forHTTPHeaderField: "Content-Type")
        print(request1)
        let task1 = NSURLSession.sharedSession().dataTaskWithRequest(request1)
            { data, response, error in
                guard error == nil && data != nil else {
                   self.activityIndicator.stopAnimating()
                    self.view.userInteractionEnabled = true
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    self.refreshControl.endRefreshing()
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                   self.activityIndicator.stopAnimating()
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    self.view.userInteractionEnabled = true
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                self.refreshControl.endRefreshing()
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                let item = json["result"]
                self.temp_phoneno.append(item["contactNo"].stringValue)
                
                DBHelper().cippyDB()
                let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
                let databasePath = databaseURL.absoluteString
                let cippyDB = FMDatabase(path: databasePath as String)
                
                var req_to_us_value = 0
                if self.request_amt_to_us[self.count]{
                    req_to_us_value = 1
                }
                else{
                    req_to_us_value = 0
                }

                print(self.temp_phoneno)
                if cippyDB.open() {
                    let insertsql = "INSERT INTO NOTIFICATION (AMOUNT,DATE,FUND_REQ_NUMBER,STATUS,APPROVE_DECLINE,REQ_TO_US,ISREAD,PHONENUMBER) VALUES ('\(self.amount[self.count])','\(self.time[self.count])','\(self.fundnumber[self.count])','"+self.status[self.count]+"','\(self.approveordecline[self.count])','\(req_to_us_value)','\(self.isread[self.count])','\(self.temp_phoneno[self.count])')"
                    print(insertsql)
                    let result = cippyDB.executeUpdate(insertsql,
                        withArgumentsInArray: nil)
                    
                    if !result {
                        //   status.text = "Failed to add contact"
                        print("Error: \(cippyDB.lastErrorMessage())")
//                        dispatch_async(dispatch_get_main_queue()) {
//                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, in DB", message: ""),animated: true,completion: nil)
//                            
//                        }
                    }
                }

                
                if(self.count < self.entityid.count){
                    self.count++
                    if(self.count == self.entityid.count){
                        dispatch_async(dispatch_get_main_queue(), {
                            self.activityIndicator.stopAnimating()
                            self.tableView.reloadData()
                           
                        self.count  = 0
                        self.phonenumber.removeAll()
                        self.phonenumber = self.temp_phoneno
                        self.refreshbtn.userInteractionEnabled = true
                        self.view.userInteractionEnabled = true
                        })
                        
                    }
                    else{
                        self.getMerchantDetailFromServer(Appconstant.BASE_URL+Appconstant.URL_FETCH_MERCHANT_DETAILS+self.entityid[self.count])
                    }
                }
                
        }
        
        task1.resume()
    }
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if(textFieldToChange.placeholder == "Enter 4 digit Password"){
            characterCountLimit = 4
        }
        
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
    }
    @IBAction func declineBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        print(indexPath.row)
        index = indexPath.row
        status[indexPath.row] = "REJECTED"
        self.passwordtxt = Appconstant.pwd
        fundno = self.fundnumber[indexPath.row]
        approverequestAPI()
    }

    
    @IBAction func approveBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        index = indexPath.row
        print(index)
        self.passwordtxt = ""
        status[indexPath.row] = "APPROVED"
        fundno = self.fundnumber[indexPath.row]
        if(Double(self.amount[indexPath.row]) > Double(Appconstant.mainbalance)!){
            dispatch_async(dispatch_get_main_queue()) {
                self.presentViewController(Alert().alert("Ouch! Insufficient Funds..Add some money instantly, it’s simple!", message: ""),animated: true,completion: nil)
            }
        }
        else{
            alertforproceed()
        }
    }
    
    func alertforproceed(){
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "PASSWORD",
            message: "Please enter your 4 digit secret password. This is necessary to initiate every transaction",
            preferredStyle: .Alert)
        alertController!.addTextFieldWithConfigurationHandler(
            {(textField: UITextField!) in
                
                textField.placeholder = "Enter 4 digit Password"
                textField.delegate = self
                textField.secureTextEntry  = true
                textField.keyboardType = UIKeyboardType.NumberPad
                
        })
        let action = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            if let textFields = alertController?.textFields{
                let theTextFields = textFields as [UITextField]
                 self!.passwordtxt = theTextFields[0].text!
                if((self!.passwordtxt == "") || (self!.passwordtxt.characters.count < 4)){
                    print(self!.passwordtxt)
                    print(Appconstant.pwd)
                    self!.alert()
                }
                else{
                    self!.approverequestAPI()
                }
                
            }
            })
        let action1 = UIAlertAction(title: "Forgot?", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            print(Appconstant.BASE_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
            self!.sendrequesttoserverForForgotPassword(Appconstant.BASE_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
            
            })
        let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            })
        
        alertController?.addAction(action)
        alertController?.addAction(action1)
        alertController?.addAction(action2)
        self.presentViewController(alertController!, animated: true, completion: nil)
    }
    func alert(){
        dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(Alert().alert("Please enter a valid Password", message: ""),animated: true,completion: nil)
        }
    }
    
    func approverequestAPI(){
        let approvemodel = ApprovalViewModel.init(fundRequestNumber: fundno, status: status[index], yapcode: self.passwordtxt)!
        let serializedjson  = JSONSerializer.toJson(approvemodel)
        print(fundno)
        print(index)
        print(serializedjson)
        self.activityIndicator.startAnimating()
        sendrequesttoserverForApprove(Appconstant.BASE_URL+Appconstant.URL_APPROVE_REQUEST_FUNDS, values: serializedjson)
        
    }
    
    func sendrequesttoserverForApprove(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {
                   // check for fundamental networking error
                    print(error)
                    self.activityIndicator.stopAnimating()
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
//                    self.activityIndicator.stopAnimating()
//                    dispatch_async(dispatch_get_main_queue()) {
//                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
//                    }
                }
               
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                    let json = JSON(data: data!)
                    let items = json["result"]
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                
//                    if items["exception"] == "" && items["txId"] != ""{
                print(json["result"])
                if json["result"] != nil{
                    dispatch_async(dispatch_get_main_queue()) {
                        Appconstant.notificationcount = Appconstant.notificationcount - 1
                        let defaults = NSUserDefaults.standardUserDefaults()
                        defaults.setObject(Appconstant.notificationcount, forKey: "badgecount")
                        print(self.index)
                    self.approveordecline[self.index] = "true"
                        DBHelper().cippyDB()
                        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
                        let databasePath = databaseURL.absoluteString
                        let cippyDB = FMDatabase(path: databasePath as String)
                        if cippyDB.open() {
                            let updateSQL1 = "UPDATE NOTIFICATION SET APPROVE_DECLINE = '"+self.approveordecline[self.index]+"' WHERE FUND_REQ_NUMBER=" + self.fundno
                            let updateSQL2 = "UPDATE NOTIFICATION SET STATUS = '"+self.status[self.index]+"' WHERE FUND_REQ_NUMBER=" + self.fundno
                            print(updateSQL1)
                            print(updateSQL2)
                            let result1 = cippyDB.executeUpdate(updateSQL1,
                                withArgumentsInArray: nil)
                            let result2 = cippyDB.executeUpdate(updateSQL2,
                                withArgumentsInArray: nil)
                            if !result1 && !result2 {
                                //   status.text = "Failed to add contact"
                                print("Error: \(cippyDB.lastErrorMessage())")
                            }
//                            else{
                            self.isread[self.index] = true
                            Appconstant.notificationcount = Appconstant.notificationcount - 1
                            let defaults = NSUserDefaults.standardUserDefaults()
                            defaults.setObject(Appconstant.notificationcount, forKey: "badgecount")
                            
                                let indexPath = NSIndexPath(forRow: self.index, inSection: 0)
                                self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
//                            }
                        }
                        cippyDB.close()

                    
                        self.updatetransactionDB()
                    }
                }
                else if json["exception"] != ""{
                    let item = json["exception"]
                    if item["errorCode"].stringValue == "Y104"{
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Incorrect Password", message: ""),animated: true,completion: nil)
                        }
                    }
                        else if item["errorCode"].stringValue == "Y202"{
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Invalid Data", message: ""),animated: true,completion: nil)
                        }
                        }
                    else if item["errorCode"].stringValue != ""{
                        dispatch_async(dispatch_get_main_queue()) {
                            var msg = item["shortMessage"].stringValue
                            if msg == "Invalid Yapcode"{
                                msg = "Incorrect Password"
                            }
                            self.presentViewController(Alert().alert(msg, message: ""),animated: true,completion: nil)
                        }
                        }
                }
                else{
                    self.activityIndicator.stopAnimating()
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        }
                }
                
        }
        
        task.resume()
        
    }
    
    
    func sendrequesttoserverForForgotPassword(url : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                
                let item = json["result"]
                if(item["success"].stringValue == "true"){
                    dispatch_async(dispatch_get_main_queue()) {
                        Appconstant.otp = item["otp"].stringValue
                        self.performSegueWithIdentifier("notification_otp", sender: self)
                    }
                }
                    
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        }
                    }
                }
        }
        
        task.resume()
        
    }
    
    func updatetransactionDB(){
        DBHelper().cippyDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
        let databasePath = databaseURL.absoluteString
        let cippyDB = FMDatabase(path: databasePath as String)
        
        if cippyDB.open() {
            let delete = "DELETE FROM TRANSACTIONS"
            
            let result = cippyDB.executeUpdate(delete,
                withArgumentsInArray: nil)
            
            if !result{
                //   status.text = "Failed to add contact"
                print("Error: \(cippyDB.lastErrorMessage())")
            }
            
        }
        cippyDB.close()
        
        let request = NSMutableURLRequest(URL: NSURL(string: Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=10")!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    self.view.userInteractionEnabled = true
                    
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                DBHelper().cippyDB()
                let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
                let databasePath = databaseURL.absoluteString
                let cippyDB = FMDatabase(path: databasePath as String)
                
                for item1 in json["result"].arrayValue{
                    let item = item1["transaction"]
                    
                    var transactionamt = ""
                    var ben_id = ""
                    var trans_date = ""
                    var descriptions = ""
                    let balance_two_decimal = String(format: "%.2f", item["amount"].doubleValue)
                    let amt = balance_two_decimal.componentsSeparatedByString(".")
                    if(amt[1].characters.count == 1){
                        let finalamount = balance_two_decimal + "0"
                        transactionamt = finalamount
                    }
                    else{
                        transactionamt = balance_two_decimal
                    }
                    if(!item["beneficiaryId"].stringValue.isEmpty){
                        let benid = item["beneficiaryId"].stringValue.componentsSeparatedByString("+91")
                        print(benid)
                        var i = 0
                        for(i=0; i<benid.count; i++){
                            
                        }
                        ben_id = benid[i-1]
                    }
                    else{
                        ben_id = item["beneficiaryId"].stringValue
                    }
                    let date = NSDate(timeIntervalSince1970: item["time"].doubleValue/1000.0)
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "dd MMM"
                    dateFormatter.timeZone = NSTimeZone(name: "UTC")
                    let dateString = dateFormatter.stringFromDate(date)
                    if(!dateString.isEmpty){
                        let datearray = dateString.componentsSeparatedByString(" ")
                        if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                            let correctdate = datearray[0] + "st " + datearray[1]
                            trans_date = correctdate
                        }
                        else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                            let correctdate = datearray[0] + "nd " + datearray[1]
                            trans_date = correctdate
                        }
                        else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                            let correctdate = datearray[0] + "rd " + datearray[1]
                            trans_date = correctdate
                        }
                        else{
                            let correctdate = datearray[0] + "th " + datearray[1]
                            trans_date = correctdate
                        }
                        
                    }
                    let matches = self.matchesForRegexInText("[0-9]", text: item["description"].stringValue)
                    let desc = matches.joinWithSeparator("")
                    descriptions = desc
                    
                    if cippyDB.open() {
                        
                        let insert = "INSERT INTO TRANSACTIONS (AMOUNT,BENEFICIARY_ID,TRANSACTION_TYPE,TYPE,TIME,TRANSACTION_STATUS,TX_REF,BENEFICIARY_NAME,DESCRIPTION,OTHER_PARTY_NAME,OTHER_PARTY_ID,TXN_ORIGIN) VALUES"
                        let value0 =  "('"+transactionamt+"','\(ben_id)','\(item["transactionType"].stringValue)','\(item["type"].stringValue)',"
                        let value1 = "'"+trans_date+"','\(item["transactionStatus"].stringValue)','\(item["txRef"].stringValue)','\(item["beneficiaryName"].stringValue)',"
                        let value2 = "'\(descriptions)','\(item["otherPartyName"].stringValue)','\(item["otherPartyId"].stringValue)','\(item["txnOrigin"].stringValue)')"
                        let insertsql = insert+value0+value1+value2
                        let result = cippyDB.executeUpdate(insertsql,
                            withArgumentsInArray: nil)
                        
                        if !result {
                            //   status.text = "Failed to add contact"
                            print("Error: \(cippyDB.lastErrorMessage())")
                            dispatch_async(dispatch_get_main_queue()) {
                                self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                                
                            }
                        }
                        cippyDB.close()
                        
                    }
                }
        }
        
        task.resume()
        
        
    }
    func matchesForRegexInText(regex: String, text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text,
                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "notification_otp") {
            let nextview = segue.destinationViewController as! OTPViewController
            nextview.fromnotification = true
            nextview.fundno = fundno
            nextview.status = status
            nextview.notificationamount = self.amount
            nextview.fromid = fromid
            nextview.toid = toid
            nextview.time = time
            nextview.isread = isread
            nextview.phonenumber = phonenumber
            nextview.request_amt_to_us = request_amt_to_us
            nextview.entityid = entityid
            nextview.approveordecline = approveordecline
            nextview.fundnumber = fundnumber
            nextview.index = index
        }
    }

}
