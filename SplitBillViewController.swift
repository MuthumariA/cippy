 //
//  SplitBillViewController.swift
//  Cippy
//
//  Created by apple on 19/12/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
import AddressBook

class SplitBillViewController: UIViewController, CNContactPickerDelegate, UITextFieldDelegate, UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var existingtrans_view: UIView!
    @IBOutlet weak var extistingtrans_btn: UIButton!
    @IBOutlet weak var amounttxtField: UITextField!
    @IBOutlet weak var remarkstxtField: UITextField!
    @IBOutlet weak var badgebtn: UIButton!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var symbollbl: UILabel!
    @IBOutlet weak var addbtn: UIButton!
    @IBOutlet weak var splitbillbtn: UIButton!
    @IBOutlet weak var mobilenotxtField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titlelbl: UILabel!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var trans_id_lbl: UILabel!
    @IBOutlet weak var usernamelbl: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var original_amt_lbl: UILabel!
    @IBOutlet weak var bottomview: UIView!
    @IBOutlet weak var splitwithfrndlbl: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var tableviewheight: NSLayoutConstraint!
    
    var splitbillitems = [SplitbillViewModel]()
    var splitmodelitem = [SplitModelItems]()
    var origin_y: CGFloat!
    var count = 0
    var temp_phonenumber = [String]()
    var phonenumber = [String]()
    var name = [String]()
    var splitamount = [String]()
    var successresultcount = 0
    var fromtransactionview = false
    var fromslip = false
    var txref = ""
    var ben_name = ""
    var time = ""
    var mainamount = ""
    var tempname = ""
    var cancelusernumber = false
    var tableheight: CGFloat = 50
    var characterCountLimit = 10
    var namefromcontact = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        origin_y = self.splitwithfrndlbl.frame.origin.y + 40
        
//        dispatch_async(dispatch_get_main_queue()) {
//            self.symbollbl.text = "₹"
//            self.titlelbl.text = "Split Bill"
//        }
        self.tableView.scrollEnabled = false
        initialize()
        
        
    }
    
    func initialize(){
        if fromtransactionview || fromslip{
            detailView.hidden = false
            existingtrans_view.hidden = true
            phonenumber.append(Appconstant.mobileno)
            name.append(Appconstant.customername)
            dispatch_async(dispatch_get_main_queue()) {
                self.symbollbl.text = "\u{20B9}"
                self.titlelbl.text = "Split Bill"
                self.trans_id_lbl.text = self.trans_id_lbl.text! + self.txref
                self.usernamelbl.text = self.ben_name
                self.datelbl.text = self.time
                self.original_amt_lbl.text = self.mainamount
                
                
            }
            
            addarray()
        }
        else{
            detailView.hidden = true
            existingtrans_view.hidden = false
            self.titlelbl.text = "Split Bill"
        }
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.splitwithfrndlbl.frame.origin.y + self.tableviewheight.constant)
        splitbillbtn.layer.cornerRadius = 10
        addbtn.layer.cornerRadius = 10
        extistingtrans_btn.layer.cornerRadius = 10
        badgebtn.layer.cornerRadius  = self.badgebtn.frame.size.height/2
        if(Appconstant.notificationcount > 0){
            badgebtn.hidden = false
            badgebtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
            badgebtn.userInteractionEnabled = false
        }
        else{
            
            badgebtn.hidden = true
        }
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, amounttxtField.frame.height - 1 , amounttxtField.frame.width, 1.0)
        bottomLine.backgroundColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        amounttxtField.borderStyle = UITextBorderStyle.None
        amounttxtField.layer.addSublayer(bottomLine)
        
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, remarkstxtField.frame.height - 1 , remarkstxtField.frame.width, 1.0)
        bottomLine1.backgroundColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        remarkstxtField.borderStyle = UITextBorderStyle.None
        remarkstxtField.layer.addSublayer(bottomLine1)
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, mobilenotxtField.frame.height - 1 , mobilenotxtField.frame.width, 1.0)
        bottomLine2.backgroundColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        mobilenotxtField.borderStyle = UITextBorderStyle.None
        mobilenotxtField.layer.addSublayer(bottomLine2)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func selectexistingtransBtnAction(sender: AnyObject) {
        self.performSegueWithIdentifier("splitbill_trans", sender: self)
    }
    func dismissKeyboard(){
        self.view.endEditing(true)
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.splitwithfrndlbl.frame.origin.y + self.tableviewheight.constant + 80)
        
    }
    @IBAction func addBtnAction(sender: AnyObject) {
        if !fromslip && !fromtransactionview{
            mainamount = self.amounttxtField.text!
        }
        if mainamount == ""{
            self.presentViewController(Alert().alert("Sorry! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
            
        else if phonenumber.count < 10{
            
            let contactPickerViewController = CNContactPickerViewController()
            
            contactPickerViewController.predicateForEnablingContact = NSPredicate(format: "Phonenumber != nil")
            
            contactPickerViewController.delegate = self
            
            presentViewController(contactPickerViewController, animated: true, completion: nil)
            
            
            
        }
        else if phonenumber.count == 10{
            self.presentViewController(Alert().alert("This is a classic problem of many!", message: ""),animated: true,completion: nil)
        }
        
    }
    func contactPicker(picker: CNContactPickerViewController, didSelectContact contact: CNContact) {
        
        var phonenos = [String]()
        let names: String? = CNContactFormatter.stringFromContact(contact, style: .FullName)
        tempname = names!
        for phoneno:CNLabeledValue in contact.phoneNumbers{
            let numVal = phoneno.value as! CNPhoneNumber
            phonenos.append(numVal.stringValue)
            
        }
        
        
        setcontactnumber(phonenos)
//        navigationController?.popViewControllerAnimated(true)
    }
    
    func setcontactnumber(numbers: [String]){
        temp_phonenumber.removeAll()
        for number1 in numbers {
            var number = ""
            number = number1.stringByReplacingOccurrencesOfString("+", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            number = number.stringByReplacingOccurrencesOfString("(", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            number = number.stringByReplacingOccurrencesOfString(")", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            number = number.stringByReplacingOccurrencesOfString("-", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            number = number.stringByReplacingOccurrencesOfString(") ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
             number = number.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            number = number.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            number = number.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            number = number.stringByReplacingOccurrencesOfString("[^0-9+]", withString: "", options: NSStringCompareOptions.RegularExpressionSearch, range:nil)
             
            
            if(number.characters.count == 10){
                temp_phonenumber.append(number)
            }
            else{
                var str = number.componentsSeparatedByString("91")
                print(str.count)
                if(str.count == 1 || str.count > 2){
                    temp_phonenumber.append(number)
                }
                else if(str[1].characters.count > 5){
                    
                    temp_phonenumber.append(str[1])
                }
                else{
                    temp_phonenumber.append(number)
                }
            }
            
        }
        if(phonenumber.count == 0 && !cancelusernumber && Appconstant.mobileno != temp_phonenumber[0]){
            phonenumber.append(Appconstant.mobileno)
            name.append(Appconstant.customername)
            addarray()
            
            
        }
        if(temp_phonenumber.count == 1){
            if Appconstant.mobileno == temp_phonenumber[0] && phonenumber.count == 1 && !cancelusernumber{
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Oops! It seems you typed your number by mistake", message: ""),animated: true,completion: nil)
                }
            }
            else{
                var alreadyexist = false
                for(var i = 0; i<phonenumber.count; i++){
                    if phonenumber[i] == temp_phonenumber[0]{
                        alreadyexist = true
                    }
                }
                if alreadyexist{
                    if((phonenumber.count == 1)&&(Appconstant.mobileno == temp_phonenumber[0])){
                    }
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Number already added, try again with other number.", message: ""),animated: true,completion: nil)
                    }
                }
                else{
                    phonenumber.append(temp_phonenumber[0])
                    name.append(tempname)
                    addarray()
                }
            }
            
        }
        else{
            let alertView: UIAlertView = UIAlertView()
            alertView.delegate = self
            alertView.title = "Pick a number.."
            for(var i = 0; i<temp_phonenumber.count; i++){
                alertView.addButtonWithTitle(temp_phonenumber[i])
            }
            alertView.show()
        }
    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        var alreadyexist = false
        for(var i = 0; i<phonenumber.count; i++){
            print(phonenumber[i])
            print(temp_phonenumber[buttonIndex])
            if phonenumber[i] == temp_phonenumber[buttonIndex]{
                alreadyexist = true
            }
        }
        if alreadyexist{
            self.presentViewController(Alert().alert("Number already added, try again with other number.", message: ""),animated: true,completion: nil)
        }
        else{
            phonenumber.append(temp_phonenumber[buttonIndex])
            name.append(tempname)
            addarray()
        }
        
    }
    
    func addarray(){
        
        origin_y = origin_y + 30
        //        tableView.frame.size.height = tableView.frame.size.height + 150
        self.tableviewheight.constant = self.tableviewheight.constant + 72
        if(count>0){
            splitamount.removeAll()
            print(phonenumber.count)
            print(Double(mainamount)!)
            let tempamt = Double(mainamount)! % Double(phonenumber.count)
            let amt = Int(tempamt)
            for(var i = 0; i<phonenumber.count; i++){
                if i == 0{
                    let tempsplitamt = Double(mainamount)!/Double(phonenumber.count)
                    let splitamt = Int(tempsplitamt) + amt
                    splitamount.append("\(splitamt)")
                }
                else{
                    let tempsplitamt = Double(mainamount)!/Double(phonenumber.count)
                    let splitamt = Int(tempsplitamt)
                    splitamount.append("\(splitamt)")
                }
            }
        }
        
        
        count++
        print(splitamount)
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.splitwithfrndlbl.frame.origin.y + self.tableviewheight.constant + 300)
        self.mobilenotxtField.text = ""
        
        if(count-1 > 0 && phonenumber.count>0){
            print(count)
            print(phonenumber)
            let contact_no = "+91" + phonenumber[count-1]
            let checkcustomerViewModel = CheckRegistrationViewModel.init(business: "DCBWALLET", businessId: contact_no)!
            let serializedjsons  = JSONSerializer.toJson(checkcustomerViewModel)
            print(serializedjsons)
            sendrequesttoserverForCheckCustomer(Appconstant.BASE_URL+Appconstant.URL_CHECK_CUSTOMER_REGISTRATION, values: serializedjsons)
        }
        
        
        self.tableView.reloadData()
    }
    
    @IBAction func splitbillBtnAction(sender: AnyObject) {
        
        if fromtransactionview || fromslip{
            if phonenumber.count < 2{
                self.presentViewController(Alert().alert("Need more names please", message: ""),animated: true,completion: nil)
            }
            else{
                count = 0
                splitbillaction()
            }
        }
        else{
            if (amounttxtField.text!.isEmpty){
                self.presentViewController(Alert().alert("Sorry! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else if Double(amounttxtField.text!)! < 2.0 {
                self.presentViewController(Alert().alert("Please select value more than \u{20B9}2", message: ""),animated: true,completion: nil)
            }
                
            else if phonenumber.count < 2{
                phonenumber.append(Appconstant.mobileno)
                name.append(Appconstant.customername)
                addarray()
                self.presentViewController(Alert().alert("Need more names please", message: ""),animated: true,completion: nil)
            }
            else{
                count = 0
                splitbillaction()
            }
        }
        
        
        
    }
    
    func splitbillaction(){
        if fromslip || fromtransactionview{
            splitbillfromtransaction()
        }
        else{
            splibillfromthisview()
        }
    }
    
    
    func splibillfromthisview(){
        if(count < phonenumber.count){
            self.activityIndicator.startAnimating()
            if phonenumber[count] == Appconstant.mobileno{
                count += 1
                splitbillaction()
            }
            else{
                let businessid = "+91" + self.phonenumber[self.count]
                
                let askmoneymodel = AskMoneyViewModel.init(amount: splitamount[count], description: self.remarkstxtField.text!, toEntityId: Appconstant.customerid, businessId: businessid, productId: "GENERAL", transactionType: "C2C", transactionOrigin: "MOBILE")!
                let serializedjson  = JSONSerializer.toJson(askmoneymodel)
                print(serializedjson)
                self.sendrequesttoserverForAskMoney(Appconstant.BASE_URL+Appconstant.URL_REQUEST_FUNDS, values: serializedjson)
            }
        }
        else if(count == phonenumber.count){
            self.activityIndicator.stopAnimating()
            //            if(successresultcount == phonenumber.count){
            
            dispatch_async(dispatch_get_main_queue()) {
                var alertController:UIAlertController?
                alertController?.view.tintColor = UIColor.blackColor()
                alertController = UIAlertController(title: "Your split request has been sent successsfully.",
                    message: "",
                    preferredStyle: .Alert)
                
                let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                    self!.performSegueWithIdentifier("splitbill_home", sender: self)
                    
                    
                    })
                
                alertController?.addAction(action)
                self.presentViewController(alertController!, animated: true, completion: nil)
                
            }
        }

    }
    
    func splitbillfromtransaction(){
        for(var i=0; i<phonenumber.count; i++){
            if(phonenumber[i] != Appconstant.mobileno){
                let businessid = "+91" + self.phonenumber[i]
                let splitbillmodel = SplitModelItems.init(toEntityId: Appconstant.customerid, name: self.ben_name, businessId: businessid, productId: "GENERAL", description: "Split Request", transactionType: "C2C", transactionOrigin: "MOBILE", amount: splitamount[i], IsEdited: false)!
                self.splitmodelitem += [splitbillmodel];
            }
        }
        let splitmodel = SplitbillViewModel.init(result: self.splitmodelitem)!
        let serializedjson  = JSONSerializer.toJson(splitmodel)
        print(serializedjson)
        let encodedString : NSData = (serializedjson as NSString).dataUsingEncoding(NSUTF8StringEncoding)!
        
        let json = JSON(data: encodedString)
        let item = json["result"]
        print(item)
//        let serializedjson1  = JSONSerializer.toJson(item)

//        print(Appconstant.BASE_URL+Appconstant.URL_SPLIT_TRANSACTION+self.txref)
        sendrequesttoserverToSplitBillFromTransaction(Appconstant.BASE_URL+Appconstant.URL_SPLIT_TRANSACTION+self.txref, value: String(item))
    }
    
    func sendrequesttoserverToSplitBillFromTransaction(url : String, value : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        print(url)
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = value.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {
                    self.activityIndicator.stopAnimating()// check for fundamental networking error
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    self.activityIndicator.stopAnimating()
                    print(value)
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                self.activityIndicator.stopAnimating()
                let item = json["result"]
                if(!item["splitId"].stringValue.isEmpty){
                    
                dispatch_async(dispatch_get_main_queue()) {
                    
                    if Appconstant.unreadcount < 10{
                        if (((Appconstant.unreadcount+self.splitmodelitem.count)>10) && ((Appconstant.notificationcount+self.splitmodelitem.count)>10)){
                            Appconstant.notificationcount = 10
                            Appconstant.unreadcount = 10
                            let defaults = NSUserDefaults.standardUserDefaults()
                            defaults.setObject(Appconstant.notificationcount, forKey: "badgecount")
                        }
                        else{
                            Appconstant.notificationcount = Appconstant.notificationcount + self.splitmodelitem.count
                            Appconstant.unreadcount = Appconstant.unreadcount + self.splitmodelitem.count
                            let defaults = NSUserDefaults.standardUserDefaults()
                            defaults.setObject(Appconstant.notificationcount, forKey: "badgecount")
                        }
                    }
                    
                    var alertController:UIAlertController?
                    alertController?.view.tintColor = UIColor.blackColor()
                    alertController = UIAlertController(title: "Your split request has been sent successsfully.",
                        message: "",
                        preferredStyle: .Alert)
                    
                    let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                        
                        self!.performSegueWithIdentifier("splitbill_home", sender: self)
                        
                        
                        })
                    
                    alertController?.addAction(action)
                    self.presentViewController(alertController!, animated: true, completion: nil)
                    
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Bummer!An error has happened. Please Retry.", message: ""),animated: true,completion: nil)
                    }
                }
                
                
        }
        
        task.resume()
        
    }

    
    func sendrequesttoserverForCheckCustomer(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        print(url)
        print(values)
        
        request.addValue("Basic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print(error)
                    self.activityIndicator.stopAnimating()
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    self.activityIndicator.stopAnimating()
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                else{
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                    let json = JSON(data: data!)
                    
                    let item = json["result"]
                    if(item["existingCustomer"].stringValue == "true"){
                        //                        dispatch_async(dispatch_get_main_queue()) {
                        //                            var amount = ""
                        //                            let amount_two_decimal = String(format: "%.2f", Double(self.splitamount[self.count])!)
                        //                            let checkamountdecimal = amount_two_decimal.componentsSeparatedByString(".")
                        //                            if(checkamountdecimal[1].characters.count == 1){
                        //                                amount = amount_two_decimal + "0"
                        //                            }
                        //                            else{
                        //                                amount = amount_two_decimal
                        //                            }
                        //                            let businessid = "+91" + self.phonenumber[self.count]
                        //
                        //                            let askmoneymodel = AskMoneyViewModel.init(amount: amount, description: self.remarkstxtField.text!, toEntityId: Appconstant.customerid, businessId: businessid, productId: "GENERAL", transactionType: "C2C", transactionOrigin: "MOBILE")!
                        //                            let serializedjson  = JSONSerializer.toJson(askmoneymodel)
                        //                            print(serializedjson)
                        //                            self.sendrequesttoserverForAskMoney(Appconstant.BASE_URL+Appconstant.URL_REQUEST_FUNDS, values: serializedjson)
                        //                        }
                        
                    }
                    else if(item["existingCustomer"].stringValue == "false"){
                        dispatch_async(dispatch_get_main_queue()) {
                            let contact_no = "+91" + self.phonenumber[self.count-1]
                            print(contact_no)
                            let dummyregistermodel = DummyRegisterViewModel.init(contactNo: contact_no, firstName: "merchant", lastName: "name", business: "DCBWALLET_DIRECT")!
                            let serializedjson  = JSONSerializer.toJson(dummyregistermodel)
                            self.sendrequesttoserverForDummyRegister(Appconstant.BASE_URL+Appconstant.URL_REGISTER, values: serializedjson)
                        }
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        }
                    }
                }
        }
        
        task.resume()
        
    }
    func sendrequesttoserverForDummyRegister(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print(error)
                    self.activityIndicator.stopAnimating()
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                else{
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                    let json = JSON(data: data!)
                    
                    let item = json["result"]
                    if(item["entityId"].stringValue != ""){
                        //                        dispatch_async(dispatch_get_main_queue()) {
                        //                            var amount = ""
                        //                            let amount_two_decimal = String(format: "%.2f", Double(self.splitamount[self.count])!)
                        //                            let checkamountdecimal = amount_two_decimal.componentsSeparatedByString(".")
                        //                            if(checkamountdecimal[1].characters.count == 1){
                        //                                amount = amount_two_decimal + "0"
                        //                            }
                        //                            else{
                        //                                amount = amount_two_decimal
                        //                            }
                        //
                        //                            //                            let businessid = "+91" + self.phonenumber[self.count]
                        //                            //
                        //                            //                            let askmoneymodel = AskMoneyViewModel.init(amount: amount, description: self.remarkstxtField.text!, toEntityId: Appconstant.customerid, businessId: businessid, productId: "GENERAL", transactionType: "C2C", transactionOrigin: "MOBILE")!
                        //                            //                            let serializedjson  = JSONSerializer.toJson(askmoneymodel)
                        //                            //                            print(serializedjson)
                        //                            //                            self.sendrequesttoserverForAskMoney(Appconstant.BASE_URL+Appconstant.URL_REQUEST_FUNDS, values: serializedjson)
                        //                        }
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        }
                    }
                }
        }
        
        task.resume()
        
    }
    
    func sendrequesttoserverForAskMoney(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print(error)
                    self.activityIndicator.stopAnimating()
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    
//                    dispatch_async(dispatch_get_main_queue()) {
//                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
//                    }
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                let item1 = json["result"]
                let item = item1["requestfunds"]
                let item2 = json["exception"]
                if(item["pkey"].stringValue != ""){
                    self.successresultcount += 1
                }
                if(item2["detailMessage"].stringValue != ""){
                    dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert(item2["detailMessage"].stringValue, message: ""),animated: true,completion: nil)
                    }
                }
                Appconstant.updatenotification = true
                if Appconstant.unreadcount<10 && Appconstant.notificationcount<10{
                    Appconstant.notificationcount = Appconstant.notificationcount+1
                    Appconstant.unreadcount = Appconstant.unreadcount + 1
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.setObject(Appconstant.notificationcount, forKey: "badgecount")
                }
                self.count += 1
                self.splitbillaction()
                
        }
        
        task.resume()
        
    }
    
    var adbk : ABAddressBook!
    
    func createAddressBook() -> Bool {
        if self.adbk != nil {
            return true
        }
        var err : Unmanaged<CFError>? = nil
        let adbk : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &err).takeRetainedValue()
        if adbk == nil {
            print(err)
            self.adbk = nil
            return false
        }
        self.adbk = adbk
        return true
    }
    
    func requestForAccess(completionHandler: (accessGranted: Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatusForEntityType(CNEntityType.Contacts)
        
        switch authorizationStatus {
        case .Authorized:
            completionHandler(accessGranted: true)
            
        case .Denied, .NotDetermined:
            self.contactStore.requestAccessForEntityType(CNEntityType.Contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(accessGranted: access)
                }
                else {
                    if authorizationStatus == CNAuthorizationStatus.Denied {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            let message = "\(accessError!.localizedDescription)\n\nPlease allow the app to access your contacts through the Settings."
                            self.ShowMessage("Cippy",message: message)
                        })
                    }
                }
            })
            
        default:
            completionHandler(accessGranted: false)
        }
    }
    var contactStore = CNContactStore()
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == amounttxtField{
            self.view.endEditing(true)
        }
        if textField == mobilenotxtField{
            self.view.endEditing(true)
        }
        
        return true // We do not want UITextField to insert line-breaks.
    }
//    func textFieldDidChange(textField: UITextField) {
//        
//        
//    }
    func ShowMessage(title : String,  message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        
        let cancelAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        
        presentViewController(alertController, animated: true, completion: nil)
        
    }
    @IBAction func textAddBtnAction(sender: AnyObject) {
        scrollView.setContentOffset(CGPoint(x: 0, y: self.tableView.frame.size.height-20), animated: false)
        if mobilenotxtField.text == ""{
            self.presentViewController(Alert().alert("Sorry! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else if mobilenotxtField.text?.characters.count != 10{
            self.presentViewController(Alert().alert("Uh - Oh! Something is wrong, please type the number and try again", message: ""),animated: true,completion: nil)
        }
        else if(Appconstant.mobileno == mobilenotxtField.text!  && !cancelusernumber && phonenumber.count != 0){
            mobilenotxtField.text = ""
            self.presentViewController(Alert().alert("Oops! It seems you typed your number by mistake", message: ""),animated: true,completion: nil)
            
        }
        else if phonenumber.count == 10{
            self.presentViewController(Alert().alert("This is a classic problem of many!", message: ""),animated: true,completion: nil)
        }
        else{
            var alreadyexist = false
            if phonenumber.count == 0 && !cancelusernumber{
                phonenumber.append(Appconstant.mobileno)
                name.append(Appconstant.customername)
                count += 1
                
            }
            if !fromslip && !fromtransactionview{
                mainamount = self.amounttxtField.text!
            }
            if mainamount == ""{
                self.presentViewController(Alert().alert("Sorry! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
            }
            else{
                print("already exists\(self.phonenumber)")
                for(var i = 0; i<phonenumber.count; i++){
                    if phonenumber[i] == mobilenotxtField.text!{
                        alreadyexist = true
                    }
                }
                if alreadyexist{
                    print(phonenumber.count)
                    if((phonenumber.count == 1)&&(Appconstant.mobileno == mobilenotxtField.text!)){
                        count--
                        addarray()
                    }
                        
                    else{
                    mobilenotxtField.text = ""
                    self.presentViewController(Alert().alert("Number already added, try again with other number.", message: ""),animated: true,completion: nil)
                    }
                }
                else{
                    phonenumber.append(self.mobilenotxtField.text!)
                    name.append(namefromcontact)
                    addarray()
                }
                
            }
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell!
        
        let namelbl = cell.viewWithTag(1) as! UILabel
        let phonelbl = cell.viewWithTag(2) as! UILabel
        let amountlbl = cell.viewWithTag(3) as! UILabel
//        let deletebtn = cell.viewWithTag(4) as! UIButton
    
        namelbl.text = name[indexPath.row]
        phonelbl.text = phonenumber[indexPath.row]
        if splitamount.count == 0{
            amountlbl.text = mainamount
        }
        else{
            amountlbl.text = splitamount[indexPath.row]
        }
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.lightGrayColor().CGColor

//        scrollView.setContentOffset(CGPoint(x: 0, y: self.tableView.frame.size.height-20), animated: true)
        
        return cell
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phonenumber.count
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let selectionColor = UIView() as UIView
        selectionColor.layer.borderWidth = 1
        selectionColor.layer.borderColor = UIColor.clearColor().CGColor
        selectionColor.backgroundColor = UIColor.clearColor()
        cell.selectedBackgroundView = selectionColor
    }
    func searchForContactUsingPhoneNumber(phoneNumber: String) {
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0), { () -> Void in
            self.requestForAccess { (accessGranted) -> Void in
                if accessGranted {
                    let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactImageDataKey, CNContactPhoneNumbersKey]
                    var contacts = [CNContact]()
                    var message: String!
                    
                    let contactsStore = CNContactStore()
                    do {
                        try contactsStore.enumerateContactsWithFetchRequest(CNContactFetchRequest(keysToFetch: keys)) {
                            (contact, cursor) -> Void in
                            if (!contact.phoneNumbers.isEmpty) {
                                let phoneNumberToCompareAgainst = phoneNumber.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet).joinWithSeparator("")
                                for phoneNumber in contact.phoneNumbers {
                                    if let phoneNumberStruct = phoneNumber.value as? CNPhoneNumber {
                                        let phoneNumberString = phoneNumberStruct.stringValue
                                        let phoneNumberToCompare = phoneNumberString.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet).joinWithSeparator("")
                                        if phoneNumberToCompare == phoneNumberToCompareAgainst {
                                            print(contact)
                                            contacts.append(contact)
                                            self.namefromcontact = contact.givenName
                                        }
                                    }
                                }
                            }
                        }
                        
                        if contacts.count == 0 {
                            self.namefromcontact = ""
                            message = "No contacts were found matching the given phone number."
                        }
                    }
                    catch {
                        message = "Unable to fetch contacts."
                    }
                    
                    if message != nil {
//                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                            self.showMessage(message)
//                        })
                    }
                    else {
                        // Success
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            // Do someting with the contacts in the main queue, for example
                            /*
                            self.delegate.didFetchContacts(contacts) <= which extracts the required info and puts it in a tableview
                            */
                            print(contacts) // Will print all contact info for each contact (multiple line is, for example, there are multiple phone numbers or email addresses)
                            let contact = contacts[0] // For just the first contact (if two contacts had the same phone number)
                            print(contact.givenName) // Print the "first" name
                            print(contact.familyName) // Print the "last" name
                            if contact.isKeyAvailable(CNContactImageDataKey) {
                                if let contactImageData = contact.imageData {
                                    print(UIImage(data: contactImageData)) // Print the image set on the contact
                                }
                            }
                        })
                    }
                }
            }
        })
    }
 

    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if(textFieldToChange == mobilenotxtField){
            characterCountLimit = 10
            if(mobilenotxtField.text?.characters.count == 9){
            searchForContactUsingPhoneNumber(mobilenotxtField.text!+string)
            }
//            requestForAccess { (accessGranted) -> Void in
//                var predicate = NSPredicate()
//                var keys = []
//                var contacts = []
//                if accessGranted {
//                    if #available(iOS 9.0, *) {
//                        predicate = CNContact.predicateForContactsMatchingName(self.mobilenotxtField.text!)
//                    
//                    } else {
//                        // Fallback on earlier versions
//                    }
//                    if #available(iOS 9.0, *) {
//                        keys = [CNContactGivenNameKey,CNContactPhoneNumbersKey]
//                    } else {
//                        // Fallback on earlier versions
//                    }
//                    if #available(iOS 9.0, *) {
//                        contacts = [CNContact]()
//                    } else {
//                        // Fallback on earlier versions
//                        contacts = ABAddressBookCopyArrayOfAllPeople(self.adbk).takeRetainedValue() as NSArray as [ABRecord]
//                        
//                    }
//                    
//                    var message: String!
//                    let contactsStore = self.contactStore
//                    do {
//                        if #available(iOS 9.0, *) {
//                            
//                            contacts = try contactsStore.unifiedContactsMatchingPredicate(predicate, keysToFetch: keys as! [CNKeyDescriptor])
//                            if contacts.count == 0 {
//                                print(contacts)
//                                message = "No contacts were found matching the given name."
//                            }
//                            else{
//                                let formatter = CNContactFormatter()
//                                formatter.style = .FullName
//                                print(contacts)
//                                for contact in contacts {
//                                    print(CNContactFormatter.stringFromContact(contact as! CNContact, style: .FullName))
//                                }
//                                self.mobilenotxtField.text = "\(contacts[0].phoneNumbers)"
//                                
//                            }
//                        }
//                    }
//                    catch {
//                        message = "Unable to fetch contacts."
//                    }
//                    if message != nil {
//                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                            self.ShowMessage("Cippy",message: message)
//                        })
//                    }
//                    else {
//                        
//                    }
//                }
//            }

        }
        else if(textFieldToChange == amounttxtField){
            let amt = self.amounttxtField.text! + string
            if(Double(amt) > 20000.0){
                var alertController:UIAlertController?
                alertController?.view.tintColor = UIColor.blackColor()
                alertController = UIAlertController(title: "Pick any amount up to Rs 20,000",
                    message: "",
                    preferredStyle: .Alert)
                
                let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                      return false
                    
                    
                    })
                
                alertController?.addAction(action)
                self.presentViewController(alertController!, animated: true, completion: nil)
            }
            else{
                return true
            }
        }
        else{
            characterCountLimit = 50
        }
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        self.tableView.scrollEnabled = true
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.tableviewheight.constant + self.splitwithfrndlbl.frame.origin.y + 300)
        
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if !fromslip && !fromtransactionview{
            if(amounttxtField.text!.isEmpty){
                
            }
            else if Double(amounttxtField.text!)! < 2.0 {
                self.presentViewController(Alert().alert("Please select value more than \u{20B9}2", message: ""),animated: true,completion: nil)
                amounttxtField.text = ""
            }
        }
    }
    
    @IBAction func cancelNoBtnAction(sender: AnyObject) {
      
            let point = sender.convertPoint(CGPointZero, toView: tableView)
            let indexPath = self.tableView.indexPathForRowAtPoint(point)!
            if(indexPath.row == phonenumber.count-1){
                phonenumber.removeAtIndex(indexPath.row)
                name.removeAtIndex(indexPath.row)
                count--
            }
            else{
                var i = 0
                for(i = indexPath.row; i<phonenumber.count-1; i++){
                    phonenumber[i] = phonenumber[i+1]
                    name[i] = name[i+1]
                }
                phonenumber.removeAtIndex(i)
                name.removeAtIndex(i)
                count--
            }
            origin_y = origin_y - 30
            
            
            if(count>0){
                splitamount.removeAll()
                print(phonenumber.count)
                print(Double(mainamount)!)
                let tempamt = Double(mainamount)! % Double(phonenumber.count)
                let amt = Int(tempamt)
                for(var i = 0; i<phonenumber.count; i++){
                    if i == 0{
                        let tempsplitamt = Double(mainamount)!/Double(phonenumber.count)
                        let splitamt = Int(tempsplitamt) + amt
                        splitamount.append("\(splitamt)")
                    }
                    else{
                        let tempsplitamt = Double(mainamount)!/Double(phonenumber.count)
                        let splitamt = Int(tempsplitamt)
                        splitamount.append("\(splitamt)")
                    }
                }
            }
        if indexPath.row == 0 {
            cancelusernumber = true
        }
            
        
            self.tableviewheight.constant = self.tableviewheight.constant - 72
            scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.tableviewheight.constant + self.splitwithfrndlbl.frame.origin.y + 300)
            self.mobilenotxtField.text = ""
            self.tableView.reloadData()
            
      
    }
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "splitbill_trans") {
            let nextview = segue.destinationViewController as! TransactionViewController
            nextview.fromsplitbill = true
        }
    }
    
    
}
