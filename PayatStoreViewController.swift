//
//  PayatStoreViewController.swift
//  Cippy
//
//  Created by apple on 28/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit
import AVFoundation

class PayatStoreViewController: UIViewController, UITextFieldDelegate, QRCodeReaderViewControllerDelegate {
    
    
    @IBOutlet weak var badgebtn: UIButton!
    @IBOutlet weak var merchantcodetxtField: UITextField!
    @IBOutlet weak var balancelbl: UILabel!
    @IBOutlet weak var remarkstxtField: UITextField!
    @IBOutlet weak var amounttxtField: UITextField!
    @IBOutlet weak var paybtn: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var merchant_citylbl: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tiptxtField: UITextField!
    @IBOutlet weak var tiplinelbl: UILabel!
    @IBOutlet weak var tiptxtlbl: UILabel!
    @IBOutlet weak var remarksconstraint: NSLayoutConstraint!
    @IBOutlet weak var qrcardimg: UIImageView!
    @IBOutlet weak var merchantidTopconstrain: NSLayoutConstraint!
    
    
    var fromotp = false
    var characterCountLimit = 4
    var merchantcode = ""
    var paystoreamount = ""
    var payremarks = ""
    var qrdata = ""
    var passwordtxt = ""
    
    var cardno = ""
    var merchantname = ""
    var mcc = ""
    var cityname = ""
    var countrycode = ""
    var indianrscode = ""
    var terminalid = ""
    var defaultvalue = ""
    var fromoffer = false
    var mvisaid = ""
    var masterid = ""
    var RuPayId = ""
    var trans_amt = ""
    var tipid = ""
    var fixedtipamt = ""
    var FixedTipPercentage = ""
    var ismvisa = false
    var imgname = ""
    lazy var reader: QRCodeReaderViewController = {
        let builder = QRCodeViewControllerBuilder { builder in
            builder.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode])
            builder.showTorchButton = true
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        qrcardimg.hidden = true
        //merchantidTopconstrain.constant = 26
        if imgname != "" || imgname.isEmpty{
            qrcardimg.hidden = false
            //merchantidTopconstrain.constant = 46
            qrcardimg.image = UIImage(named: imgname)
        }
        badgebtn.userInteractionEnabled = false
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        scrollView.addGestureRecognizer(tap)
        badgebtn.layer.cornerRadius = self.badgebtn.frame.size.height/2
        paybtn.layer.cornerRadius = 10
        balancelbl.text = self.balancelbl.text! + " \u{20B9} " + Appconstant.mainbalance
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 400)
        remarksconstraint.constant = 78
        tiptxtField.hidden = false
        tiplinelbl.hidden = false
        tiptxtlbl.hidden = false
        funcforcallserver()
    }
    func funcforcallserver(){
        if(Appconstant.notificationcount > 0){
            badgebtn.hidden = false
            badgebtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
        }
        else{
            badgebtn.hidden = true
        }
        if fromotp{
            merchantcodetxtField.text = merchantcode
            amounttxtField.text = paystoreamount
            remarkstxtField.text = payremarks
            if ismvisa{
                remarksconstraint.constant = 6
                tiptxtField.hidden = true
                tiplinelbl.hidden = true
                tiptxtlbl.hidden = true
            }
            else{
                remarksconstraint.constant = 78
                tiptxtField.hidden = false
                tiplinelbl.hidden = false
                tiptxtlbl.hidden = false
                tiptxtField.text = self.fixedtipamt
            }
            activityIndicator.startAnimating()
            callserverForPay()
        }
        if(!merchantcode.isEmpty || !paystoreamount.isEmpty || !payremarks.isEmpty){
            merchantcodetxtField.text = merchantcode
            amounttxtField.text = paystoreamount
            remarkstxtField.text = payremarks
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == merchantcodetxtField{
            amounttxtField.becomeFirstResponder()
        }
        if textField == amounttxtField{
            remarkstxtField.becomeFirstResponder()
        }
        
        
        return true // We do not want UITextField to insert line-breaks.
    }
    func textFieldShouldBeginEditing(state: UITextField) -> Bool {
        if tiptxtField.hidden{
            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 530)
        }
        else{
            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 570)
        }
        return true
    }
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if tiptxtField.hidden{
            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 530)
        }
        else{
            scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 570)
        }
        if(textFieldToChange.placeholder == "Enter 4 digit Password"){
            characterCountLimit = 4
        }
        else if(textFieldToChange == remarkstxtField){
            characterCountLimit = 32
        }
        else if (textFieldToChange == amounttxtField){
            if((amounttxtField.text?.rangeOfString(".")) != nil){
                let strcount = amounttxtField.text! + string
                let strarray = strcount.componentsSeparatedByString(".")
                for(var i = 0; i<strarray.count; i++){
                    if i == 1{
                        if strarray[1].isEmpty{
                            
                        }
                        else{
                            if strarray[1].characters.count == 3{
                                return false
                            }
                            else{
                                return true
                            }
                        }
                    }
                }
            }
        }
            
        else{
            characterCountLimit = 25
        }
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
    }
    
    @IBAction func payBtnAction(sender: AnyObject) {
        if(merchantcodetxtField.text!.isEmpty){
            self.presentViewController(Alert().alert("Sorry! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else if(amounttxtField.text!.isEmpty){
            self.presentViewController(Alert().alert("Please enter valid amount", message: ""),animated: true,completion: nil)
        }
        else if(Double(amounttxtField.text!)! < 5.0 || Double(amounttxtField.text!)! > 5000.0){
            self.presentViewController(Alert().alert("Please enter amount between Rs.5 - Rs.5000", message: ""),animated: true,completion: nil)
        }
        else if(Double(amounttxtField.text!)! > Double(Appconstant.mainbalance)!){
            self.presentViewController(Alert().alert("Ouch! Insufficient Funds.. Add some money instantly, it's simple!", message: ""),animated: true,completion: nil)
        }
        else{
            if !ismvisa{
                if tiptxtField.text!.isEmpty || Double(tiptxtField.text!) <= 0.0 {
                    tiptxtField.text = "0"
                    alertforproceed()
                }
                else if Double(tiptxtField.text!)>Double(self.amounttxtField.text!){
                    self.presentViewController(Alert().alert("Tips/Convenience fee must be less then transaction amount", message: ""),animated: true,completion: nil)
                }
                else{
                    alertforproceed()
                }
            }
            else{
                alertforproceed()
            }
        }
    }
    
    func alertforproceed(){
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: "PASSWORD",
                                            message: "Please enter your 4 digit secret password. This is necessary to initiate every transaction",
                                            preferredStyle: .Alert)
        alertController!.addTextFieldWithConfigurationHandler(
            {(textField: UITextField!) in
                
                textField.placeholder = "Enter 4 digit Password"
                textField.delegate = self
                textField.secureTextEntry  = true
                textField.keyboardType = UIKeyboardType.NumberPad
                
        })
        let action = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            if let textFields = alertController?.textFields{
                let theTextFields = textFields as [UITextField]
                self!.passwordtxt = theTextFields[0].text!
                if((self!.passwordtxt == "") || (self!.passwordtxt.characters.count < 4)){
                    self!.alertforinvalidpwd()
                }
                else{
                    self!.callserverForPay()
                }
                
            }
            })
        let action1 = UIAlertAction(title: "Forgot?", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            self!.activityIndicator.startAnimating()
            print(Appconstant.BASE_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
            self!.sendrequesttoserverForForgotPassword(Appconstant.BASE_URL+Appconstant.URL_GENERATE_OTP+Appconstant.mobileno)
            
            })
        let action2 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            
            })
        
        alertController?.addAction(action)
        alertController?.addAction(action1)
        alertController?.addAction(action2)
        self.presentViewController(alertController!, animated: true, completion: nil)
    }
    func alertforinvalidpwd(){
        dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(Alert().alert("Please enter a valid Password", message: ""),animated: true,completion: nil)
        }
    }
    func sendrequesttoserverForForgotPassword(url : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                self.activityIndicator.stopAnimating()
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                self.activityIndicator.stopAnimating()
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            self.activityIndicator.stopAnimating()
            let item = json["result"]
            if(item["success"].stringValue == "true"){
                dispatch_async(dispatch_get_main_queue()) {
                    Appconstant.otp = item["otp"].stringValue
                    self.performSegueWithIdentifier("pay_otp", sender: self)
                }
            }
                
            else{
                dispatch_async(dispatch_get_main_queue()) {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
            }
        }
        
        task.resume()
        
    }
    
    func callserverForPay(){
        if ismvisa{
            payusingMvisaQR()
        }
        else{
            payusingBharathQR()
        }
    }
    
    func payusingMvisaQR(){
        let payviewmodel = PayAtStoreMvisaViewModel.init(amount: self.amounttxtField.text!, description: self.remarkstxtField.text!, fromEntityId: Appconstant.customerid, toEntityId: merchantcodetxtField.text!, productId: "GENERAL", yapcode: self.passwordtxt, transactionType: "PURCHASE", transactionOrigin: "MOBILE", businessId: "", business: "DCBWALLET", businessType: "DCBWALLET", qrData: qrdata, merchantData: qrdata)!
        let serializedjson  = JSONSerializer.toJson(payviewmodel)
        print(serializedjson)
        self.activityIndicator.startAnimating()
        self.sendrequesttoserverForPay(Appconstant.BASE_URL+Appconstant.URL_PAY_STORE, values: serializedjson)
        
    }
    
    func payusingBharathQR(){
        let payviewmodel = PayAtStoreBharathViewModel.init(amount: self.amounttxtField.text!, description: self.remarkstxtField.text!, fromEntityId: Appconstant.customerid, toEntityId: merchantcodetxtField.text!, productId: "GENERAL", yapcode: self.passwordtxt, transactionType: "PURCHASE", transactionOrigin: "MOBILE", businessId: "", business: "DCBWALLET", businessType: "DCBWALLET", qrData: qrdata, merchantData: qrdata,tipAmount: self.tiptxtField.text!)!
        let serializedjson  = JSONSerializer.toJson(payviewmodel)
        print(serializedjson)
        self.activityIndicator.startAnimating()
        self.sendrequesttoserverForPay(Appconstant.BASE_URL+Appconstant.URL_PAY_STORE, values: serializedjson)
        
    }
    
    func sendrequesttoserverForPay(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print(error)
                self.activityIndicator.stopAnimating()
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            self.updatetransactionDB()
            let json = JSON(data: data!)
            self.activityIndicator.stopAnimating()
            let item = json["result"]
            if json["result"] != nil{
                if(item["txId"].stringValue != ""){
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        self.performSegueWithIdentifier("pay_success", sender: self)
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
            }
            else{
                
                let item1 = json["exception"]
                if item1["errorCode"].stringValue == "Y104"{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Incorrect Password", message: ""),animated: true,completion: nil)
                    }
                }
                else if item1["errorCode"].stringValue == "Y202"{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Invalid Data", message: ""),animated: true,completion: nil)
                    }
                }
                else if item1["errorCode"].stringValue != ""{
                    dispatch_async(dispatch_get_main_queue()) {
                        var msg = item1["shortMessage"].stringValue
                        if msg == "Invalid Yapcode"{
                            msg = "Incorrect Password"
                        }
                        self.presentViewController(Alert().alert(msg, message: ""),animated: true,completion: nil)
                    }
                }
                    
                else{
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
            }
            
        }
        
        task.resume()
        
    }
    
    
    @IBAction func qrscanBtnAction(sender: AnyObject) {
        
        if QRCodeReader.supportsMetadataObjectTypes() {
            reader.modalPresentationStyle = .FormSheet
            reader.delegate = self
            dispatch_async(dispatch_get_main_queue()) {
                self.reader.completionBlock = { (result: QRCodeReaderResult?) in
                    if let result = result {
                        print("Completion with result: \(result.value) of type \(result.metadataType)")
                    }
                }
            }
            presentViewController(reader, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            
            presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    func reader(reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        
        self.dismissViewControllerAnimated(true, completion: { [weak self] in
            self!.getQRDataFromJSON(result.value)
            print(result)
            
            })
    }
    
    func readerDidCancel(reader: QRCodeReaderViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    //    func getQRDataFromJSON(str: String)
    //    {
    //        let nsData = (str as NSString).dataUsingEncoding(NSUTF8StringEncoding)
    //        let reader = JSON(data: nsData!)
    //        print(reader)
    //    }
    
    func checkForValidMerchantID(merchantID : String) -> Bool
    {
        let badCharacters = NSCharacterSet.decimalDigitCharacterSet().invertedSet
        
        if merchantID.rangeOfCharacterFromSet(badCharacters) == nil {
            return true
        } else {
            return false
        }
    }
    
    
    func getQRDataFromJSON(str: String)
    {
        
        //        let nsData = (str as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        //        let reader = JSON(data: nsData!)
        //        let merchantID = str
        
        let qrimg = str.substringWithRange(str.startIndex.advancedBy(0)..<str.startIndex.advancedBy(4))
        
        if qrimg == "0002"{
            //merchantidTopconstrain.constant = 46
            qrcardimg.image = UIImage(named: "VISA.png")
            imgname = "VISA.png"
            print("visa")
        }
        else if qrimg == "0004"{
            //merchantidTopconstrain.constant = 46
            qrcardimg.image = UIImage(named: "Master-Card.png")
            imgname = "Master-Card.png"
            print("mastercard")
        }
        else if qrimg == "0006"{
            //merchantidTopconstrain.constant = 46
            qrcardimg.image = UIImage(named: "rupay-logo.png")
            imgname = "rupay-logo.png"
            print("rupay")
        }
        else{
            let qrimg1 = str.substringWithRange(str.startIndex.advancedBy(0)..<str.startIndex.advancedBy(2))
            
            if qrimg1 == "00"{
                //merchantidTopconstrain.constant = 46
                qrcardimg.image = UIImage(named: "mVisa.png")
                imgname = "mVisa.png"
                print("mvisa")
            }
            else{
                imgname = ""
                qrcardimg.hidden = true
                //merchantidTopconstrain.constant = 26
            }
        }
        let qr = str.substringWithRange(str.startIndex.advancedBy(0)..<str.startIndex.advancedBy(2))
        print(qr)
        if qr == "00"{
            ismvisa = false
            getBharathQRdata(str)
        }
        else{
            ismvisa = true
            getMVisaQRData(str)
        }
        
    }
    
    //Parsing QR data from Bharath format
    func getBharathQRdata(str: String)
    {
        print(str)
        qrdata = str
        var alphaNumeric = "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var characters = [Character](str.characters)
        
        let n = str.characters.count
        //        print (n)
        var i:Int = 0;
        //        print(i)
        if str == "" || str.characters.count <= 10
        {
            // txtMerchantID.text = "1";
        }
        
        if str.characters.first  != "0"
        {
            // txtMerchantID.text = "1";
        }
        else
        {
            var startindex = 0
            
            while(i < n-1)
            {
                var len = 0
                var tmp  = ""
                let current_pos = str.substringWithRange(str.startIndex.advancedBy(startindex)..<str.startIndex.advancedBy(startindex+2))
                print("POS:\(current_pos)")
                startindex = startindex + 2
                let len_value = str.substringWithRange(str.startIndex.advancedBy(startindex)..<str.startIndex.advancedBy(startindex+2))
                var num = Int(len_value)
                if num == nil {
                    callalertfunc()
                    break
                }
                len = Int(len_value)!
                startindex = startindex + len + 2
                print("LEN:\(len)")
                len = len + 1
                if len<2{
                    break
                }
                else{
                    i = i + 2
                    for j in 2...len
                    {
                        tmp = tmp + String(characters[i+j])
                        //                        print(tmp)
                    }
                    print("VALUE\(tmp)")
                    
                    
                    
                    let no = Int(current_pos)!
                    
                    
                    
                    if no == 0
                    {
                        
                        
                        //                        defaultvalue = tmp
                        //                        merchantcodetxtField.text = defaultvalue
                    }
                        
                    else if no == 2
                    {
                        cardno = tmp ;
                        terminalid = tmp
                        mvisaid = tmp
                        merchantcodetxtField.text = tmp
                        //                        merchantname = tmp
                        
                    }
                    else if no == 4
                    {
                        if merchantcodetxtField.text!.isEmpty{
                            merchantcodetxtField.text = tmp
                        }
                        cardno = tmp
                        defaultvalue = tmp
                        masterid = tmp
                        
                        
                    }
                    else if no == 6
                    {
                        if merchantcodetxtField.text!.isEmpty{
                            merchantcodetxtField.text = tmp
                        }
                        cardno = tmp
                        defaultvalue = tmp
                        RuPayId = tmp
                        
                    }
                    else if no == 52
                    {
                        mcc = tmp
                        
                    }
                    else if no == 53
                    {
                        indianrscode = tmp
                    }
                    else if no == 54{
                        trans_amt = tmp
                        amounttxtField.text = tmp
                    }
                    else if no == 55{
                        tipid = tmp
                    }
                    else if no == 56{
                        fixedtipamt = tmp
                        tiptxtField.text = tmp
                    }
                    else if no == 57{
                        FixedTipPercentage = tmp
                    }
                    else if no == 58
                    {
                        countrycode = tmp
                        //                        terminalid = tmp
                    }
                    else if no == 59
                    {
                        merchantname = tmp
                        
                        //                        if str.containsString("M2PY")
                        
                    }
                    else if no == 60{
                        cityname = tmp
                    }
                    
                    i = (i+1) + len;
                    print (tmp)
                }
                print(merchantname + "-" + cityname)
                
                merchant_citylbl.text = merchantname + "-" + cityname
                qrcardimg.hidden = false
            }
        }
        remarksconstraint.constant = 78
        tiptxtField.hidden = false
        tiplinelbl.hidden = false
        tiptxtlbl.hidden = false
        
    }
    
    
    
    
    
    
    
    //Parsing QR data from mVisa format
    func getMVisaQRData(str: String)
    {
        print(str)
        qrdata = str
        var alphaNumeric = "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var characters = [Character](str.characters)
        
        let n = str.characters.count
        //        print (n)
        var i:Int = 0;
        //        print(i)
        if str == "" || str.characters.count <= 10
        {
            // txtMerchantID.text = "1";
        }
        
        if str.characters.first  != "0"
        {
            // txtMerchantID.text = "1";
        }
        else
        {
            while(i < n )
            {
                var tmp  = "";
                
                var len:Int = 0
                if characters[i+1] == "A" {      len = 10      }
                else if characters[i+1] == "B" {      len = 11      }
                else if characters[i+1] == "C" {      len = 12      }
                else if characters[i+1] == "D" {      len = 13      }
                else if characters[i+1] == "E" {      len = 14      }
                else if characters[i+1] == "F" {      len = 15      }
                else if characters[i+1] == "G" {      len = 16      }
                else if characters[i+1] == "H" {      len = 17      }
                else if characters[i+1] == "I" {      len = 18      }
                else if characters[i+1] == "J" {      len = 19      }
                else if characters[i+1] == "K" {      len = 20      }
                else if characters[i+1] == "L" {      len = 21      }
                else if characters[i+1] == "M" {      len = 22      }
                else if characters[i+1] == "N" {      len = 23      }
                else if characters[i+1] == "O" {      len = 24      }
                else if characters[i+1] == "P" {      len = 25      }
                else if characters[i+1] == "Q" {      len = 26      }
                else if characters[i+1] == "R" {      len = 27      }
                else if characters[i+1] == "S" {      len = 28      }
                else if characters[i+1] == "T" {      len = 29      }
                else if characters[i+1] == "U" {      len = 30      }
                else if characters[i+1] == "V" {      len = 31      }
                else if characters[i+1] == "W" {      len = 32      }
                else if characters[i+1] == "X" {      len = 33      }
                else if characters[i+1] == "Y" {      len = 34      }
                else if characters[i+1] == "Z" {      len = 35      }
                else if characters[i+1] == "1" {      len = 1      }
                else if characters[i+1] == "2" {      len = 2      }
                else if characters[i+1] == "3" {      len = 3      }
                else if characters[i+1] == "4" {      len = 4      }
                else if characters[i+1] == "5" {      len = 5      }
                else if characters[i+1] == "6" {      len = 6      }
                else if characters[i+1] == "7" {      len = 7      }
                else if characters[i+1] == "8" {      len = 8      }
                else if characters[i+1] == "9" {      len = 9      }
                
                // print(alphaNumeric.startIndex.distanceTo((alphaNumeric.rangeOfString(characters[i+1])?.startIndex)!))
                
                // var len = alphaNumeric.startIndex.distanceTo((alphaNumeric.rangeOfString(characters[i+1])?.startIndex)!)+2;
                //  var len = str.startIndex.distanceToq((alphaNumeric.rangeOfString(characters[i+1])?.startIndex)!) + 2;
                
                
                //  print(len)
                len = len + 1
                print(len)
                if len < 2{
                    callalertfunc()
                    break
                }
                else{
                    for j in 2...len
                    {
                        //print(j)
                        
                        tmp = tmp + String(characters[i+j])
                        print(tmp)
                    }
                    
                    let c = characters[i];
                    
                    
                    if str.containsString("M2PY")
                    {
                        print(c)
                        if c == "N"
                        {
                            
                            defaultvalue = tmp
                            //                        merchantcodetxtField.text = defaultvalue
                            
                            //                        let merchantIDCount = tmp.characters.count;
                            //                        let substring = merchantIDCount - 2
                            //                        merchantcodetxtField.text = (tmp as NSString).substringToIndex(substring);
                            //                        print("merchantID"+merchantcodetxtField.text!)
                            break;
                            // self.txtMerchantID.text = tmp
                            
                        }
                    }
                    else{
                        
                        print(c)
                        if c == "0"
                        {
                            cardno = tmp ;
                            defaultvalue = tmp
                            merchantcodetxtField.text = defaultvalue
                        }
                    }
                    if c == "0"
                    {
                        merchantcodetxtField.text = tmp
                    }
                    if c == "1"
                    {
                        merchantname = tmp
                        
                    }
                    if c == "2"
                    {
                        mcc = tmp
                    }
                    if c == "3"
                    {
                        cityname = tmp
                    }
                    if c == "4"
                    {
                        countrycode = tmp
                    }
                    if c == "5"
                    {
                        indianrscode = tmp
                    }
                    if c == "N"
                    {
                        terminalid = tmp
                    }
                    if c == "6"
                    {
                        let amt = Int(tmp)!
                        self.amounttxtField.text = "\(amt)"
                        if str.containsString("M2PY")
                        {
                        }
                        else{
                            break;
                        }
                    }
                    
                    i = (i+1) + len;
                    print (tmp)
                }
                print(merchantname + "-" + cityname)
                
                merchant_citylbl.text = merchantname + "-" + cityname
                qrcardimg.hidden = false
            }
        }
        remarksconstraint.constant = 6
        
        tiptxtField.hidden = true
        tiplinelbl.hidden = true
        tiptxtlbl.hidden = true
    }
    func callalertfunc(){
        self.presentViewController(Alert().alert("Invalid QR!", message: ""),animated: true,completion: nil)
    }
    func dismissKeyboard(){
        view.endEditing(true)
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 400)
    }
    
    func updatetransactionDB(){
        DBHelper().cippyDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
        let databasePath = databaseURL.absoluteString
        let cippyDB = FMDatabase(path: databasePath as String)
        
        if cippyDB.open() {
            let delete = "DELETE FROM TRANSACTIONS"
            
            let result = cippyDB.executeUpdate(delete,
                                               withArgumentsInArray: nil)
            
            if !result{
                //   status.text = "Failed to add contact"
                print("Error: \(cippyDB.lastErrorMessage())")
            }
            
        }
        cippyDB.close()
        
        let request = NSMutableURLRequest(URL: NSURL(string: Appconstant.BASE_URL+Appconstant.URL_FETCH_RECENT_TRANSACTIONS+Appconstant.customerid+"?pageNo=1&pageSize=10")!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                self.view.userInteractionEnabled = true
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    
                }
                
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            DBHelper().cippyDB()
            let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
            let databasePath = databaseURL.absoluteString
            let cippyDB = FMDatabase(path: databasePath as String)
            
            for item1 in json["result"].arrayValue{
                let item = item1["transaction"]
                
                var transactionamt = ""
                var ben_id = ""
                var trans_date = ""
                var descriptions = ""
                let balance_two_decimal = String(format: "%.2f", item["amount"].doubleValue)
                let amt = balance_two_decimal.componentsSeparatedByString(".")
                if(amt[1].characters.count == 1){
                    let finalamount = balance_two_decimal + "0"
                    transactionamt = finalamount
                }
                else{
                    transactionamt = balance_two_decimal
                }
                if(!item["beneficiaryId"].stringValue.isEmpty){
                    let benid = item["beneficiaryId"].stringValue.componentsSeparatedByString("+91")
                    print(benid)
                    var i = 0
                    for(i=0; i<benid.count; i++){
                        
                    }
                    ben_id = benid[i-1]
                }
                else{
                    ben_id = item["beneficiaryId"].stringValue
                }
                let date = NSDate(timeIntervalSince1970: item["time"].doubleValue/1000.0)
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd MMM"
                dateFormatter.timeZone = NSTimeZone(name: "UTC")
                let dateString = dateFormatter.stringFromDate(date)
                if(!dateString.isEmpty){
                    let datearray = dateString.componentsSeparatedByString(" ")
                    if(datearray[0] != "11" && datearray[0].characters.last == "1"){
                        let correctdate = datearray[0] + "st " + datearray[1]
                        trans_date = correctdate
                    }
                    else if(datearray[0] != "12" && datearray[0].characters.last == "2"){
                        let correctdate = datearray[0] + "nd " + datearray[1]
                        trans_date = correctdate
                    }
                    else if(datearray[0] != "13" && datearray[0].characters.last == "3"){
                        let correctdate = datearray[0] + "rd " + datearray[1]
                        trans_date = correctdate
                    }
                    else{
                        let correctdate = datearray[0] + "th " + datearray[1]
                        trans_date = correctdate
                    }
                    
                }
                let matches = self.matchesForRegexInText("[0-9]", text: item["description"].stringValue)
                let desc = matches.joinWithSeparator("")
                descriptions = desc
                
                if cippyDB.open() {
                    
                    let insert = "INSERT INTO TRANSACTIONS (AMOUNT,BENEFICIARY_ID,TRANSACTION_TYPE,TYPE,TIME,TRANSACTION_STATUS,TX_REF,BENEFICIARY_NAME,DESCRIPTION,OTHER_PARTY_NAME,OTHER_PARTY_ID,TXN_ORIGIN) VALUES"
                    let value0 =  "('"+transactionamt+"','\(ben_id)','\(item["transactionType"].stringValue)','\(item["type"].stringValue)',"
                    let value1 = "'"+trans_date+"','\(item["transactionStatus"].stringValue)','\(item["txRef"].stringValue)','\(item["beneficiaryName"].stringValue)',"
                    let value2 = "'\(descriptions)','\(item["otherPartyName"].stringValue)','\(item["otherPartyId"].stringValue)','\(item["txnOrigin"].stringValue)')"
                    let insertsql = insert+value0+value1+value2
                    let result = cippyDB.executeUpdate(insertsql,
                                                       withArgumentsInArray: nil)
                    
                    if !result {
                        //   status.text = "Failed to add contact"
                        print("Error: \(cippyDB.lastErrorMessage())")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                    cippyDB.close()
                    
                }
            }
        }
        
        task.resume()
        
        
    }
    func matchesForRegexInText(regex: String, text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text,
                                                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    @IBAction func backBtnAction(sender: AnyObject) {
        if fromoffer{
            self.performSegueWithIdentifier("pay_offer", sender: self)
        }
        else{
            self.performSegueWithIdentifier("pay_home", sender: self)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "pay_otp") {
            let nextview = segue.destinationViewController as! OTPViewController
            nextview.frompay = true
            nextview.merchantcode = self.merchantcodetxtField.text!
            nextview.paystoreamount = self.amounttxtField.text!
            nextview.payremarks = self.remarkstxtField.text!
            nextview.qrdata = self.qrdata
            nextview.ismvisa = self.ismvisa
            nextview.fixedtipamt = self.fixedtipamt
            nextview.imgname = self.imgname
            
        }
    }
}
