//
//  AddMoneyViewController.swift
//  Cippy
//
//  Created by apple on 17/12/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit

class AddMoneyViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var badgebtn: UIButton!
    @IBOutlet weak var symbollbl: UILabel!
    @IBOutlet weak var amounttxtfield: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstbtn: UIButton!
    @IBOutlet weak var secondbtn: UIButton!
    @IBOutlet weak var thirdbtn: UIButton!
    @IBOutlet weak var dcbradiobtn: UIButton!
    @IBOutlet weak var dcbradiobtnimg: UIImageView!
    @IBOutlet weak var otherbankbtn: UIButton!
    @IBOutlet weak var otherbankbtnimg: UIImageView!
    @IBOutlet weak var completepaymentbtn: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
        
    var characterCountLimit = 10
    var radioselected = "DCBBANK"
    var pgtype = "DIRECT"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       initialize()
    }
    
    func initialize(){
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 400)
        self.navigationController?.navigationBarHidden = true
        symbollbl.text = "\u{20B9}"
        amounttxtfield.attributedPlaceholder = NSAttributedString(string:"Amount",
            attributes:[NSForegroundColorAttributeName: UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1)])
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, amounttxtfield.frame.height - 1 , amounttxtfield.frame.width, 1.0)
        bottomLine.backgroundColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        amounttxtfield.borderStyle = UITextBorderStyle.None
        amounttxtfield.layer.addSublayer(bottomLine)
        if(Appconstant.notificationcount > 0){
            badgebtn.hidden = false
            badgebtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
            badgebtn.userInteractionEnabled = false
        }
        else{
            badgebtn.hidden = true
        }
        badgebtn.layer.cornerRadius  = self.badgebtn.frame.size.height/2
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        scrollView.addGestureRecognizer(tap)
        let height = self.firstbtn.frame.size.width/3
        firstbtn.frame.size.height = height
        secondbtn.frame.size.height = height
        thirdbtn.frame.size.height = height
        firstbtn.setTitle("\u{20B9}"+"500" , forState: .Normal)
        secondbtn.setTitle("\u{20B9}"+"2000" , forState: .Normal)
        thirdbtn.setTitle("\u{20B9}"+"5000" , forState: .Normal)
        firstbtn.layer.cornerRadius = 10
        secondbtn.layer.cornerRadius = 10
        thirdbtn.layer.cornerRadius = 10
        firstbtn.layer.borderWidth = 1
        secondbtn.layer.borderWidth = 1
        thirdbtn.layer.borderWidth = 1
        firstbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        secondbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        thirdbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        
        completepaymentbtn.layer.cornerRadius = 10
    }
    
    func dismissKeyboard(){
        self.view.endEditing(true)
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 400)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func firstBtnAction(sender: AnyObject) {
        firstbtnaction()
    }
    func firstbtnaction(){
        firstbtn.layer.backgroundColor = UIColor(red: 42.0/255.0, green: 203.0/255.0, blue: 229.0/255.0, alpha: 1).CGColor
        firstbtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        firstbtn.layer.borderColor = UIColor.whiteColor().CGColor
        secondbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        secondbtn.titleLabel?.textColor = UIColor.lightGrayColor()
        secondbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        thirdbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        thirdbtn.titleLabel?.textColor = UIColor.lightGrayColor()
        thirdbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        amounttxtfield.text = "500"
    }
    @IBAction func secondBtnAction(sender: AnyObject) {
        secondbtnaction()
    }
    func secondbtnaction(){
        secondbtn.layer.backgroundColor = UIColor(red: 42.0/255.0, green: 203.0/255.0, blue: 229.0/255.0, alpha: 1).CGColor
        secondbtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        secondbtn.layer.borderColor = UIColor.whiteColor().CGColor
        firstbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        firstbtn.titleLabel?.textColor = UIColor.lightGrayColor()
        firstbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        thirdbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        thirdbtn.titleLabel?.textColor = UIColor.lightGrayColor()
        thirdbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        amounttxtfield.text = "2000"
    }
    @IBAction func thirdBtnAction(sender: AnyObject) {
        thirdbtnaction()
    }
    func thirdbtnaction(){
        thirdbtn.layer.backgroundColor = UIColor(red: 42.0/255.0, green: 203.0/255.0, blue: 229.0/255.0, alpha: 1).CGColor
        thirdbtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        thirdbtn.layer.borderColor = UIColor.whiteColor().CGColor
        firstbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        firstbtn.titleLabel?.textColor = UIColor.lightGrayColor()
        firstbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        secondbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        secondbtn.titleLabel?.textColor = UIColor.lightGrayColor()
        secondbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        amounttxtfield.text = "5000"
    }
    
    @IBAction func dcbRadioBtnAction(sender: AnyObject) {
        dcbradiobtnimg.image = UIImage(named: "radio-on-button.png")
        otherbankbtnimg.image = UIImage(named: "Radio.png")
        radioselected = "DCBBANK"
        pgtype = "DIRECT"
    }
    
    @IBAction func otherbankRadioBtnAction(sender: AnyObject) {
        dcbradiobtnimg.image = UIImage(named: "Radio.png")
        otherbankbtnimg.image = UIImage(named: "radio-on-button.png")
        radioselected = ""
        pgtype = ""
    }
    
    func textFieldShouldBeginEditing(state: UITextField) -> Bool {
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 630)
        return true
    }
    
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 630)
        print(string)
        print(self.amounttxtfield.text)
        let txtvalue = Double(self.amounttxtfield.text! + string)!
        
        if(txtvalue == 500 && string != ""){
            firstbtnaction()
            firstbtn.titleLabel?.textColor = UIColor.whiteColor()
            amounttxtfield.text = "50"
        }
        else if(string == "" && amounttxtfield.text! == "5000"){
            firstbtnaction()
            firstbtn.titleLabel?.textColor = UIColor.whiteColor()
            amounttxtfield.text = "5000"
        }
        else if(txtvalue == 2000 && string != ""){
            secondbtnaction()
            secondbtn.titleLabel?.textColor = UIColor.whiteColor()
            amounttxtfield.text = "200"
        }
        else if(string == "" && amounttxtfield.text! == "20000"){
            secondbtnaction()
            secondbtn.titleLabel?.textColor = UIColor.whiteColor()
            amounttxtfield.text = "20000"
        }
        else if(txtvalue == 5000 && string != ""){
            thirdbtnaction()
            thirdbtn.titleLabel?.textColor = UIColor.whiteColor()
            amounttxtfield.text = "500"
        }
        else if(string == "" && amounttxtfield.text! == "50000"){
            thirdbtnaction()
            thirdbtn.titleLabel?.textColor = UIColor.whiteColor()
            amounttxtfield.text = "50000"
        }
        else{
            firstbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
            secondbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
            thirdbtn.layer.borderColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
            firstbtn.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
            secondbtn.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
            thirdbtn.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
            firstbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
            secondbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
            thirdbtn.layer.backgroundColor = UIColor.whiteColor().CGColor
        }
        if(textFieldToChange == amounttxtfield){
            characterCountLimit = 10
        }
        
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
    }
    
    @IBAction func completepaymentBtnAction(sender: AnyObject) {

        if (amounttxtfield.text! == ""){
            self.presentViewController(Alert().alert("Sorry! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else if((Int(amounttxtfield.text!)) == 0){
            self.presentViewController(Alert().alert("Pick any amount upto "+"\u{20B9}"+"20,000", message: ""),animated: true,completion: nil)
        }
        else if((Double(amounttxtfield.text!)) > 20000){
            self.presentViewController(Alert().alert("You cannot have balance more than 20000. Please load money accordingly!", message: ""),animated: true,completion: nil)
        }
        else if((Double(amounttxtfield.text!)! + Double(Appconstant.mainbalance)!) > 20000.0){
            self.presentViewController(Alert().alert("You cannot have balance more than 20000. Please load money accordingly!", message: ""),animated: true,completion: nil)
        }
        else{
            self.activityIndicator.startAnimating()
            let addmoneyviewmodel = AddMoneyViewModel.init(entityId: Appconstant.customerid, amount: Float(amounttxtfield.text!)!, pgType: pgtype, bankName: radioselected)!
            let serializedjson  = JSONSerializer.toJson(addmoneyviewmodel)
            print(serializedjson)
            sendrequesttoserverBillDeskIntegration(Appconstant.BASE_URL+Appconstant.URL_BILL_DESK_INTEGRATION, values: serializedjson)
            
            
        }
    }
    func sendrequesttoserverBillDeskIntegration(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    print(error)
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        }
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                else{
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                    let json = JSON(data: data!)
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    let item = json["result"]
                    print(json["result"])
                    print(json["result"].stringValue)
                    print(item["requestMessage"].stringValue)
                    if json["result"].isEmpty{
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("You cannot have balance more than 20000. Please load money accordingly!", message: ""),animated: true,completion: nil)
                        }
                    }
                    else{
                        if self.radioselected == ""{
                            Appconstant.Url = item["targetUrl"].stringValue + "?msg=" + item["requestMessage"].stringValue
                        }
                        else if self.radioselected == "DCBBANK"{
                            Appconstant.Url = item["targetUrl"].stringValue + "&msg=" + item["requestMessage"].stringValue
                        }
                        dispatch_async(dispatch_get_main_queue()) {
                            self.performSegueWithIdentifier("web", sender: self)
                        }
                    }
            }
        }
        
        task.resume()
        
    }

}
