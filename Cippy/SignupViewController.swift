
//
//  SignupViewController.swift
//  Cippy
//
//  Created by apple on 15/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController, UITextFieldDelegate {
    
    
    
    @IBOutlet weak var showhidebtn: UIButton!
    @IBOutlet weak var signinbtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var signupbtn: UIButton!

    
    @IBOutlet weak var checkbtn: UIButton!
    @IBOutlet weak var mobilenolbl: UITextField!
    @IBOutlet weak var passwordlbl: UITextField!
    @IBOutlet weak var firstnamelbl: UITextField!
    @IBOutlet weak var lastnamelbl: UITextField!
    @IBOutlet weak var emaillbl: UITextField!
    
    @IBOutlet weak var signupBluebtn: UIButton!
    
    
    var characterCountLimit = 10
    var showpwd = false
    var segue = false
    var agreeterms = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        signupbtn.titleLabel?.attributedText = NSAttributedString(string: "Sign up", attributes: underlineAttribute)
        signupBluebtn.layer.cornerRadius = 9
        settextfield()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        scrollView.addGestureRecognizer(tap)
        mobilenolbl.delegate = self
        passwordlbl.delegate = self
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 500)
        showhidebtn.layer.cornerRadius = 8
        passwordlbl.secureTextEntry = true
        self.navigationController?.navigationBarHidden = true
    }
    
    func settextfield(){
        mobilenolbl.attributedPlaceholder = NSAttributedString(string:"10 Digit Mobile No",
            attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, mobilenolbl.frame.height - 1 , mobilenolbl.frame.width, 1.0)
        bottomLine.backgroundColor = UIColor.whiteColor().CGColor
        mobilenolbl.borderStyle = UITextBorderStyle.None
        mobilenolbl.layer.addSublayer(bottomLine)
        passwordlbl.attributedPlaceholder = NSAttributedString(string:"Select 4 digit password",
            attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, passwordlbl.frame.height - 1 , passwordlbl.frame.width, 1.0)
        bottomLine1.backgroundColor = UIColor.whiteColor().CGColor
        passwordlbl.borderStyle = UITextBorderStyle.None
        passwordlbl.layer.addSublayer(bottomLine1)
        firstnamelbl.attributedPlaceholder = NSAttributedString(string:"First Name",
            attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRectMake(0.0, firstnamelbl.frame.height - 1 , firstnamelbl.frame.width, 1.0)
        bottomLine2.backgroundColor = UIColor.whiteColor().CGColor
        firstnamelbl.borderStyle = UITextBorderStyle.None
        firstnamelbl.layer.addSublayer(bottomLine2)
        lastnamelbl.attributedPlaceholder = NSAttributedString(string:"Last Name",
            attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        let bottomLine3 = CALayer()
        bottomLine3.frame = CGRectMake(0.0, lastnamelbl.frame.height - 1 , lastnamelbl.frame.width, 1.0)
        bottomLine3.backgroundColor = UIColor.whiteColor().CGColor
        lastnamelbl.borderStyle = UITextBorderStyle.None
        lastnamelbl.layer.addSublayer(bottomLine3)
        emaillbl.attributedPlaceholder = NSAttributedString(string:"Email",
            attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        let bottomLine4 = CALayer()
        bottomLine4.frame = CGRectMake(0.0, emaillbl.frame.height - 1 , emaillbl.frame.width, 1.0)
        bottomLine4.backgroundColor = UIColor.whiteColor().CGColor
        emaillbl.borderStyle = UITextBorderStyle.None
        emaillbl.layer.addSublayer(bottomLine4)
        if segue {
            self.mobilenolbl.text! = Appconstant.mobileno
            self.passwordlbl.text! = Appconstant.pwd
            self.firstnamelbl.text! = Appconstant.firstname
            self.lastnamelbl.text! = Appconstant.lastname
            self.emaillbl.text! = Appconstant.email
        }
    }

    @IBAction func signupBtnAction(sender: AnyObject) {
        signupbtn.titleLabel!.font =  UIFont.systemFontOfSize(18, weight: UIFontWeightBold)
        signinbtn.titleLabel!.font =  UIFont.systemFontOfSize(18, weight: UIFontWeightMedium)
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        signupbtn.titleLabel?.attributedText = NSAttributedString(string: "Sign up", attributes: underlineAttribute)
        let underlineAttribute1 = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleNone.rawValue]
        signinbtn.titleLabel?.attributedText = NSAttributedString(string: "Sign in", attributes: underlineAttribute1)
    
    }
    
    @IBAction func signinBtnAction(sender: AnyObject) {
        signupbtn.titleLabel!.font =  UIFont.systemFontOfSize(18, weight: UIFontWeightMedium)
        signinbtn.titleLabel!.font =  UIFont.systemFontOfSize(18, weight: UIFontWeightBold)
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        signinbtn.titleLabel?.attributedText = NSAttributedString(string: "Sign in", attributes: underlineAttribute)
        let underlineAttribute1 = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleNone.rawValue]
        signupbtn.titleLabel?.attributedText = NSAttributedString(string: "Sign up", attributes: underlineAttribute1)
    }
    

//    func textFieldDidEndEditing(textField: UITextField) {
//        if(textField == firstnamelbl) {
//            animateViewMoving(false, moveValue: 100)
//        }
//        if(textField == lastnamelbl) {
//            animateViewMoving(false, moveValue: 200)
//        }
//        if(textField == emaillbl) {
//            animateViewMoving(false, moveValue: 200)
//        }
//        
//    }
//    func animateViewMoving (up:Bool, moveValue :CGFloat){
//        let movementDuration:NSTimeInterval = 0.3
//        let movement:CGFloat = ( up ? -moveValue : moveValue)
//        UIView.beginAnimations( "animateView", context: nil)
//        UIView.setAnimationBeginsFromCurrentState(true)
//        UIView.setAnimationDuration(movementDuration )
//        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
//        UIView.commitAnimations()
//        
//    }

    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 790)
//        if(textField == firstnamelbl) {
//            animateViewMoving(true, moveValue: 100)
//        }
//        if(textField == lastnamelbl) {
//            animateViewMoving(true, moveValue: 200)
//        }
//        if(textField == emaillbl) {
//            animateViewMoving(true, moveValue: 200)
//        }
        
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == firstnamelbl{
            lastnamelbl.becomeFirstResponder()
        }
        if textField == lastnamelbl{
            emaillbl.becomeFirstResponder()
        }
        if textField == passwordlbl{
            firstnamelbl.becomeFirstResponder()
        }
        return true // We do not want UITextField to insert line-breaks.
    }
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 790)
        if(textFieldToChange == mobilenolbl){
            characterCountLimit = 10
            
        }
        if(mobilenolbl.text?.characters.count == 9){
            let number = self.mobilenolbl.text! + string
            print(Appconstant.BASE_URL+Appconstant.MANAGER_BUSINESS_ENTITY+Appconstant.URL_CHECK_CUSTOMER+number)
            sendrequesttoserverForCheckCustomer(Appconstant.BASE_URL+Appconstant.MANAGER_BUSINESS_ENTITY+Appconstant.URL_CHECK_CUSTOMER+number)
            
            
        }
        
        if(textFieldToChange == passwordlbl){
            characterCountLimit = 4
        }
        else if(textFieldToChange != passwordlbl && textFieldToChange != mobilenolbl){
            characterCountLimit = 30
        }
//        if(string == ""){
//           if textFieldToChange == firstnamelbl{
//                lastnamelbl.becomeFirstResponder()
//            }
//            
//        }
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
    
    }
    func sendrequesttoserverForCheckCustomer(url : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                    }
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                
                let item = json["result"]
                if((item["registered"].stringValue == "true")&&(item["isPassCodeSet"].stringValue == "true")){
                    dispatch_async(dispatch_get_main_queue()) {
                        self.performSegueWithIdentifier("to_signin", sender: self)
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.passwordlbl.becomeFirstResponder()
                    }
                }
        }
        
        task.resume()
        
    }


    
    @IBAction func showhideBtnAction(sender: AnyObject) {
        if(!showpwd){
            showpwd = true
            showhidebtn.setTitle("hide", forState: UIControlState.Normal)
            passwordlbl.secureTextEntry = false
        }
        else{
            showpwd = false
            showhidebtn.setTitle("show", forState: UIControlState.Normal)
            passwordlbl.secureTextEntry = true
        }
        
    }
    @IBAction func termsandconditions(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: "http://yappay.in/app-static/dcbwallet/dcb_tnc.html")!)
    }
    
    
    func dismissKeyboard(){
        self.view.endEditing(true)
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 500)
    }
    
    
    
    @IBAction func signupBlueBtnAction(sender: AnyObject) {
        Appconstant.mobileno = self.mobilenolbl.text!
        Appconstant.pwd = self.passwordlbl.text!
        Appconstant.firstname = self.firstnamelbl.text!
        Appconstant.lastname = self.lastnamelbl.text!
        Appconstant.email = self.emaillbl.text!
        
        
        if(Appconstant.mobileno.isEmpty || Appconstant.pwd.isEmpty || Appconstant.firstname.isEmpty || Appconstant.lastname.isEmpty || Appconstant.email.isEmpty) {
            self.presentViewController(Alert().alert("Sorry! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else if(Appconstant.mobileno.characters.count != 10){
            self.presentViewController(Alert().alert("Uh – Oh! Something is wrong, please check the number and try again.", message: ""),animated: true,completion: nil)
        }
        else if(Appconstant.pwd.characters.count != 4){
            self.presentViewController(Alert().alert("Please enter a 4 digit Password", message: ""),animated: true,completion: nil)
        }
        
            
        else {
             let check = isValidEmail(Appconstant.email)
                if(check){
                    if(!agreeterms){
                        self.presentViewController(Alert().alert("Please agree on terms and conditions", message: ""),animated: true,completion: nil)
                    }
                    else{
                        
                    print(Appconstant.BASE_URL+Appconstant.URL_GENERATE_OTP+self.mobilenolbl.text!)
                    self.sendrequesttoserverForGenerateOTP(Appconstant.BASE_URL+Appconstant.URL_GENERATE_OTP+self.mobilenolbl.text!)
                    }
                }
                else{
                    self.presentViewController(Alert().alert("Please select valid mail id", message: ""),animated: true,completion: nil)
            }
            }
        
        
    }
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func sendrequesttoserverForGenerateOTP(url : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                
                let item = json["result"]
                if((item["success"].stringValue == "true")&&(item["isCustomerExists"].stringValue == "true")){
                    dispatch_async(dispatch_get_main_queue()) {
                        Appconstant.otp = item["otp"].stringValue
                        Appconstant.customerid = item["customerId"].stringValue
                        Appconstant.customername = item["customerName"].stringValue
                        Appconstant.dummycustomer = true
                        Appconstant.newcustomer = false
                        self.performSegueWithIdentifier("to_otp", sender: self)
                    }
                }
                else if((item["success"].stringValue == "true")&&(item["isCustomerExists"].stringValue == "false")){
                    dispatch_async(dispatch_get_main_queue()) {
                        Appconstant.otp = item["otp"].stringValue
                        Appconstant.newcustomer = true
                        Appconstant.dummycustomer = false
                        self.performSegueWithIdentifier("to_otp", sender: self)
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        
                    }
                }
        }
        
        task.resume()
        
    }
    
    @IBAction func checkBtnAction(sender: AnyObject) {
        if(agreeterms){
            agreeterms = false
            checkbtn.setImage(UIImage(named: "uncheck.png"), forState: UIControlState.Normal)
        }
        else{
            agreeterms = true
            checkbtn.setImage(UIImage(named: "check.png"), forState: UIControlState.Normal)
        }
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "to_signin") {
            let nextview = segue.destinationViewController as! SigninViewController
            nextview.fromsignup = true
            nextview.mobileno = self.mobilenolbl.text!
        }
    }
}
