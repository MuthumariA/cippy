//
//  AppDelegate.swift
//  Cippy
//
//  Created by apple on 15/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit
import Contacts
import Google
import InsertFramework.InsertManager
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GGLInstanceIDDelegate, GCMReceiverDelegate  {

    var window: UIWindow?
    var connectedToGCM = false
    var subscribedToTopic = false
    var gcmSenderID: String = "684097176981"
    var registrationToken: String?
    var registrationOptions = [String: AnyObject]()
    
    let registrationKey = "onRegistrationCompleted"
    let messageKey = "onMessageReceived"
    let subscriptionTopic = "/topics/global"
//    func application(application: UIApplication, open url: NSURL, sourceApplication: String?, annotation: Any) -> Bool
//    {
////        if url.scheme.range(of: "insert") != nil {
//            InsertManager.sharedManager().initWithUrl(url)
//        
//            return true
////        }
//        // your code here...
//        //return true
//    }
    func application(application: UIApplication, handleOpenURL url: NSURL) -> Bool {
        InsertManager.sharedManager().initWithUrl(url)
        return true
    }
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
//       FIRApp.configure()
        
       
//        UILabel.appearance().font = UIFont(name: "Quicksand", size: .NaN)
//        UIButton.appearance().titleLabel?.font = UIFont(name: "Quicksand", size: .NaN)!
        
        
        if #available(iOS 8.0, *) {
            InsertManager.sharedManager().initSDK("f9fdf7bd842489f674d70b092bf70b8f990fb8837a3c31617ccf6f5418606ee60421a7131dcda404656db0dac5c71c7bd6417b65c9748c12491ff5a36dc18a2f.d872536c9e874dbff1b7dd1630dfe7ae.3ce3af369d3095d6f3716cae043a818a174784029bd632d6f6eb674e44a1fc58",companyName:"dcbbankindia")
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        } else {
            // Fallback
            let types: UIRemoteNotificationType = [.Alert, .Badge, .Sound]
            application.registerForRemoteNotificationTypes(types)
        }
        let gcmConfig = GCMConfig.defaultConfig()
        gcmConfig.receiverDelegate = self
        GCMService.sharedInstance().startWithConfig(gcmConfig)
        return true
    }
//    func UpdateAppFunction(){
//        
//        var alertController:UIAlertController?
//        alertController?.view.tintColor = UIColor.blackColor()
//        alertController = UIAlertController(title: "A new version of DCB Wallet is available",
//            message: "",
//            preferredStyle: .Alert)
//        
//        let action = UIAlertAction(title: "Update", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
//            dispatch_async(dispatch_get_main_queue()) {
//               
//            }
//            
//            })
//        let action1 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
//            
//            
//            })
//        
//        alertController?.addAction(action)
//        alertController?.addAction(action1)
//        let Controller = UIApplication.sharedApplication().windows[0].rootViewController!
//    
//        let activeViewController = Controller.presentedViewController
//        
//        activeViewController!.presentViewController(alertController!, animated: true, completion: nil)
//        
//    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken
        deviceToken: NSData ) {
            // [END receive_apns_token]
            // [START get_gcm_reg_token]
            // Create a config and set a delegate that implements the GGLInstaceIDDelegate protocol.
            let instanceIDConfig = GGLInstanceIDConfig.defaultConfig()
            instanceIDConfig.delegate = self
            // Start the GGLInstanceID shared instance with that config and request a registration
            // token to enable reception of notifications
            GGLInstanceID.sharedInstance().startWithConfig(instanceIDConfig)
            registrationOptions = [kGGLInstanceIDRegisterAPNSOption:deviceToken,
                kGGLInstanceIDAPNSServerTypeSandboxOption:true]
            GGLInstanceID.sharedInstance().tokenWithAuthorizedEntity(gcmSenderID,
                scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)

            let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
            var tokenString = ""
            
            for i in 0..<deviceToken.length {
                tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
            }
            
            print("Device Token:", tokenString)
            Appconstant.gcmid = tokenString
            print(Appconstant.gcmid)
//            // [END get_gcm_reg_token]
    }
    func onTokenRefresh() {
        // A rotation of the registration tokens is happening, so the app needs to request a new token.
        print("The GCM registration token needs to be changed.")
        GGLInstanceID.sharedInstance().tokenWithAuthorizedEntity(gcmSenderID,
            scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
    }
    
    func registrationHandler(registrationToken: String!, error: NSError!) {
        if (registrationToken != nil) {
            self.registrationToken = registrationToken
            print("Registration Token: \(registrationToken)")
            self.subscribeToTopic()
            let userInfo = ["registrationToken": registrationToken]
            NSNotificationCenter.defaultCenter().postNotificationName(
                self.registrationKey, object: nil, userInfo: userInfo)
        } else {
            print("Registration to GCM failed with error: \(error.localizedDescription)")
            let userInfo = ["error": error.localizedDescription]
            NSNotificationCenter.defaultCenter().postNotificationName(
                self.registrationKey, object: nil, userInfo: userInfo)
        }
    }
    
    func subscribeToTopic() {
        // If the app has a registration token and is connected to GCM, proceed to subscribe to the
        // topic
        if(registrationToken != nil && connectedToGCM) {
            GCMPubSub.sharedInstance().subscribeWithToken(self.registrationToken, topic: subscriptionTopic,
                options: nil, handler: {(error:NSError?) -> Void in
                    if let error = error {
                        // Treat the "already subscribed" error more gently
                        if error.code == 3001 {
                            print("Already subscribed to \(self.subscriptionTopic)")
                        } else {
                            print("Subscription failed: \(error.localizedDescription)");
                        }
                    } else {
                        self.subscribedToTopic = true;
                        NSLog("Subscribed to \(self.subscriptionTopic)");
                    }
            })
        }
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        application.applicationIconBadgeNumber = 0
    }
        
    func application( application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        if application.applicationState == UIApplicationState.Active {
            if let info = userInfo as? Dictionary<String,String> {
                let req_no = info["RequestNo"]!.dataUsingEncoding(NSUTF8StringEncoding)
                let reqno  = JSON(data: req_no!)
                let msg1 = info["BroadcastMsg"]!.dataUsingEncoding(NSUTF8StringEncoding)
                let msg  = JSON(data: msg1!)
                let amt1 = info["Amount"]!.dataUsingEncoding(NSUTF8StringEncoding)
                let amt  = JSON(data: amt1!)
                let phonenoarray = msg.stringValue.componentsSeparatedByString(" has")
                let phoneno = phonenoarray[0]
                
                var alertController:UIAlertController?
                alertController?.view.tintColor = UIColor.blackColor()
                alertController = UIAlertController(title: "Fund Request",
                    message: "\(msg)",
                    preferredStyle: .Alert)
                
                let action = UIAlertAction(title: "Approve", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                  
                    let date = NSDate()
                    let formatter = NSDateFormatter()
                    
                    formatter.dateFormat = "d/MM/yyyy HH:mm"
                    
                    
                    let approvemodel = ApprovalViewModel.init(fundRequestNumber: reqno.stringValue, status: "APPROVED", yapcode: Appconstant.pwd)!
                    let serializedjson  = JSONSerializer.toJson(approvemodel)
                    print(serializedjson)
                    self!.sendrequesttoserverForApprove(Appconstant.BASE_URL+Appconstant.URL_APPROVE_REQUEST_FUNDS, values: serializedjson)
                    
                    let result = formatter.stringFromDate(date)
//                    let dateFormatter = NSDateFormatter()
//                    dateFormatter.dateFormat = "d/MM/yyyy HH:mm"
//                    let dateString = dateFormatter.stringFromDate(date)
                    DBHelper().cippyDB()
                    let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
                    let databasePath = databaseURL.absoluteString
                    let cippyDB = FMDatabase(path: databasePath as String)
                    if cippyDB.open() {
                        let insertsql = "INSERT INTO NOTIFICATION (AMOUNT,DATE,FUND_REQ_NUMBER,STATUS,APPROVE_DECLINE,REQ_TO_US,ISREAD,PHONENUMBER) VALUES ('\(amt.stringValue)','\(result)','\(reqno.stringValue)','\("APPROVED")','\("true")','\("true")','\("false")','\(phoneno)')"
                        print(insertsql)
                        let result = cippyDB.executeUpdate(insertsql,
                            withArgumentsInArray: nil)
                        
                        if !result {
                            //   status.text = "Failed to add contact"
                            print("Error: \(cippyDB.lastErrorMessage())")
                        }
                    }

                
            })
                let action1 = UIAlertAction(title: "Decline", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                    
                    let date = NSDate()
                    let formatter = NSDateFormatter()
                    
                    formatter.dateFormat = "d/MM/yyyy HH:mm"
                    
                    let result = formatter.stringFromDate(date)
                    
                    DBHelper().cippyDB()
                    let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
                    let databasePath = databaseURL.absoluteString
                    let cippyDB = FMDatabase(path: databasePath as String)
                    if cippyDB.open() {
                        let insertsql = "INSERT INTO NOTIFICATION (AMOUNT,DATE,FUND_REQ_NUMBER,STATUS,APPROVE_DECLINE,REQ_TO_US,ISREAD,PHONENUMBER) VALUES ('\(amt.stringValue)','\(result)','\(reqno.stringValue)','\("DECLINED")','\("true")','\("true")','\("false")','\(phoneno)')"
                        print(insertsql)
                        let result = cippyDB.executeUpdate(insertsql,
                            withArgumentsInArray: nil)
                        
                        if !result {
                            //   status.text = "Failed to add contact"
                            print("Error: \(cippyDB.lastErrorMessage())")
                        }
                    }
                    
                    
                    })
                
                alertController?.addAction(action)
                alertController?.addAction(action1)
                self.window?.rootViewController?.presentViewController(alertController!, animated: true, completion: nil)
            }

        }
            print("Notification received: \(userInfo)")
    
    }
    
    
    func sendrequesttoserverForApprove(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {
                // check for fundamental networking error
                print(error)
                
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.window?.rootViewController?.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                //                    self.activityIndicator.stopAnimating()
                //                    dispatch_async(dispatch_get_main_queue()) {
                //                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                //                    }
            }
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
            let json = JSON(data: data!)
            let items = json["result"]
           
            if items["txId"].stringValue != ""{
               
            }
            else if json["exception"] != ""{
                let item = json["exception"]
                if item["errorCode"].stringValue == "Y104"{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.window?.rootViewController?.presentViewController(Alert().alert("Incorrect Password", message: ""),animated: true,completion: nil)
                    }
                }
                else if item["errorCode"].stringValue == "Y202"{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.window?.rootViewController?.presentViewController(Alert().alert("Invalid Data", message: ""),animated: true,completion: nil)
                    }
                }
                else if item["errorCode"].stringValue != ""{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.window?.rootViewController?.presentViewController(Alert().alert(item["shortMessage"].stringValue, message: ""),animated: true,completion: nil)
                    }
                }
            }
            else{
                dispatch_async(dispatch_get_main_queue()) {
                    self.window?.rootViewController?.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            
        }
        
        task.resume()
        
    }



    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }


}

