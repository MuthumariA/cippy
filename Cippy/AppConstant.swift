//
//  AppConstant.swift
//  payableApp
//
//  Created by apple on 15/09/16.
//  Copyright © 2016 apple. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration





struct Appconstant
{

    
//    static let BASE_URL = "https://sit-secure.yappay.in/Yappay";         //test check new
//    static let VERSION_CHECK = "http://yappay.in/test-app-info/DCBWALLETIOS" //test check
    
    static let BASE_URL = "https://wallet.yappay.in/Yappay";         // Current Live url (working) new
    static let VERSION_CHECK = "http://yappay.in/prod-app-info/DCBWALLETIOS" //Current Production url(working) new
    
//      static let BASE_URL = "http://yapcustomer.yappay.in:6563/Yappay";//Production
//       static let BASE_URL = "http://sit-new.yappay.in:8080/Yappay";         //test check old
//       static let WEB_URL = "http://dcbwallet.sit.yappay.in:9000/Yappay" //test check old
//        static let BASE_URL = "https://dcbwallet.yappay.in/Yappay" //Current Production URL(working) old
//    static let BASE_URL = "http://oynk.yappay.in:6363/"   // production url(not working)
//    static let BASE_URL = "http://dcbwallet.yappay.in:6563/Yappay"   // IOS production url(working) old

    
    static let TENANT = "DCBWALLET"
    
    //check customer already register
    static let MANAGER_BUSINESS_ENTITY = "/business-entity-manager/";
    static let MANAGER_OTP = "/otp-manager/";
    
    
    static let URL_CHECK_CUSTOMER = "otp/+91";
    static let URL_GENERATE_OTP = MANAGER_OTP + "generate/+91";
    
    static let MANAGER_REGISTRATION = "/registration-manager/";
    static let URL_REGISTER = MANAGER_REGISTRATION + "register";
    
    static let MANAGER_CUSTOMER = "/customer-manager/";
    static let URL_FETCH_CUSTOMER_DETAILS_BY_MOBILENO = MANAGER_CUSTOMER + "fetch/mobileno/+91";
    
    // update customer details
    static let URL_UPDATE_ENTITY = MANAGER_BUSINESS_ENTITY + "updateentity";
    
    // forgot password
    static let URL_FORGOT_PASSCODE = MANAGER_CUSTOMER + "yapcode/forgot";
    
    // signin url
    static let URL_VERIFY_EXIST_CUSTOMER = MANAGER_REGISTRATION + "existingCustomer";
    
    // get balance
    static let URL_FETCH_MULTI_BALANCE_INFO = MANAGER_BUSINESS_ENTITY + "fetchbalance/";
    
    
    // transaction details
    static let MANAGER_TRANSACTION = "/txn-manager/";
    static let URL_FETCH_RECENT_TRANSACTIONS = MANAGER_TRANSACTION + "fetch/success/entity/";
    static let URL_REQUEST_FUNDS = MANAGER_TRANSACTION + "requestfunds";
    
    // check customer for ask money
    static let URL_CHECK_CUSTOMER_REGISTRATION = MANAGER_REGISTRATION + "checkCustomer";
    
    // send money
    static let URL_PAY_MERCHANT = MANAGER_TRANSACTION + "create";
    
    // for get plans
    static let URL_GETOPEARTORS = "/recharge-manager/operators";
    
    static let URL_RECHARGE = "/recharge-manager/recharge";
    static let URL_FETCH_BILL = "/bill-manager/billFetch"
    
    // pay @ store
    static let URL_PAY_STORE = "/payment-manager/payment";
    
    //notification
    static let URL_FUNDREQUEST = MANAGER_TRANSACTION + "fetch/fundsrequest/";
    
    static let URL_FETCH_MERCHANT_DETAILS = MANAGER_BUSINESS_ENTITY + "fetchbyentityid/";
    static let URL_APPROVE_REQUEST_FUNDS = MANAGER_TRANSACTION + "authorize/requestfunds";
    
    
    
    // Add Money
    static let URL_BILL_DESK_INTEGRATION = "/pg/billdesk/paymentreqid";
    
    
    // split bill
    static let URL_SPLIT_TRANSACTION = MANAGER_TRANSACTION + "split/";
    
    // Offers
    static let GET_CITYLIST_OFFER = "https://cippy.blob.core.windows.net/cdn/offers/cities.json"
    static let GET_OFFERS_WITH_CITYNAME = "https://cippy.blob.core.windows.net/cdn/offers/"
    
    // Profile Image
    static let GET_PROFILE_IMAGE = "https://cippy.blob.core.windows.net/cdn/"
    
    
  //  http://dcbwallet.sit.yappay.in:9000/Yappay/txn-manager/fetch/success/entity/767258492?pageNumber=2&pageSize=10
    
    
    
//     static let BASIC_AUTHCODE = "Basic YWRtaW46YWRtaW4=";
//    
    
//     static let URL_SET_ENTITY_DETAILS = MANAGER_REGISTRATION + "entityDetails";
     
//     static let URL_RESEND_OTP = "/resendOtp";
    
//     static let MANAGER_CARD = "/ysci-manager/";
//    
//     static let URL_CHANGE_PASSCODE = MANAGER_CUSTOMER + "yapcode/change";
    
//     static let URL_FETCH_PENDING_TRANSACTIONS = MANAGER_TRANSACTION + "fetch/stage/entity/{customerId}?pageNo=1&pageSize=10";
//      let URL_FETCH_COMPLETED_TRANSACTIONS = MANAGER_TRANSACTION + "fetch/success/entity/{customerId}?pageNo=1&pageSize=10";
    
    
    

//     static let URL_GENERATE_OTP_CARD = MANAGER_OTP + "generate/+91{mobileNumber}";
    
    
    
    
    
    
//     static let URL_PAYMENT_INTEGRATION = "/pg/paymentreqid";
//     static let URL_PAYMENT_INTEGRATION_RETURN_URL = "/pg/processor/payment";
//     static let URL_VERSION_UPDATE = "/api-manager?appName=" + "DCBWALLET";
//     static let GetPlanList = "/Test/getPlan";
//     static let Recharge = "/Test/Recharge";
//     static let PlanServerUrl = "http://oynk.yappay.in/getplans/prepaid/";
    
    
    
//     static let URL_PAY_MERCHANT_DIRECT = MANAGER_TRANSACTION + "create/direct";
    
//     static let PayForACauseUrl = "http://yappay.in/app-static/dcbwallet/";
//     static let URL_REQUEST_CARD = "/cardrequest/newcard";
//     static let URL_GET_CARD_DETAILS = "/getcarddetails";
//     static let URL_GET_CARD_STATUS = "/cardrequest/cardstatus";
//     static let URL_LINK_CARD = MANAGER_BUSINESS_ENTITY + "card/link";
    
    
//     static let URL_DIRECT_REGISTER = MANAGER_REGISTRATION + "register";
    
    static var mobileno = ""
    static var pwd = ""
    static var firstname = ""
    static var lastname = ""
    static var email = ""
    static var otp = ""
    static var customerid = ""
    static var customername = ""
    static var profileimg = [UIImage]()
    
    static var dummycustomer = false
    static var newcustomer = false
    static var mainbalance = "0"
    static var notificationcount = 0
    static var gcmid = ""
    static var Url = ""
    static var updatenotification = false
    static var latitude = ""
    static var longitude = ""
    static var signuplat = ""
    static var signuplong = ""
    static var unreadcount = 0
}







public class Reachability: UIViewController {
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}





struct Operators{
    static var pre_operatorCode = [String]()
    static var pre_operatorName = [String]()
    static var pre_operatorId = [String]()
    static var pre_operatorType = [String]()
    static var pre_special = [Bool]()
    
    static var post_operatorCode = [String]()
    static var post_operatorName = [String]()
    static var post_operatorId = [String]()
    static var post_operatorType = [String]()
    static var post_special = [Bool]()
    
    static var dth_operatorCode = [String]()
    static var dth_operatorName = [String]()
    static var dth_operatorId = [String]()
    static var dth_operatorType = [String]()
    static var dth_special = [Bool]()
    static var deleteDB = false
}
