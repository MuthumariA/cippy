//
//  SigninViewController.swift
//  Cippy
//
//  Created by apple on 16/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit

class SigninViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var signupbtn: UIButton!
    @IBOutlet weak var signinbtn: UIButton!
    @IBOutlet weak var mobilenolbl: UITextField!
    @IBOutlet weak var passwordlbl: UITextField!
    @IBOutlet weak var signinBluebtn: UIButton!
    @IBOutlet weak var useotpbtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var showhidebtn: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var characterCountLimit = 10
    var showpwd = false
    var fromsignup = false
    var mobileno =  ""
    var descriptions = ""
    var state = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        signinbtn.titleLabel?.attributedText = NSAttributedString(string: "Sign in", attributes: underlineAttribute)
        signinBluebtn.layer.cornerRadius = 9
        useotpbtn.layer.cornerRadius = 9
        settextfield()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        scrollView.addGestureRecognizer(tap)
        mobilenolbl.delegate = self
        passwordlbl.delegate = self
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 650)
        showhidebtn.layer.cornerRadius = 8
        passwordlbl.secureTextEntry = true
        if(mobileno != ""){
            mobilenolbl.text = mobileno
            self.passwordlbl.becomeFirstResponder()
        }
        self.navigationController?.navigationBarHidden = true
        
    }
    
    func settextfield(){
        mobilenolbl.attributedPlaceholder = NSAttributedString(string:"10 Digit Mobile No",
            attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, mobilenolbl.frame.height - 1 , mobilenolbl.frame.width, 1.0)
        bottomLine.backgroundColor = UIColor.whiteColor().CGColor
        mobilenolbl.borderStyle = UITextBorderStyle.None
        mobilenolbl.layer.addSublayer(bottomLine)
        passwordlbl.attributedPlaceholder = NSAttributedString(string:"Select 4 digit password",
            attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRectMake(0.0, passwordlbl.frame.height - 1 , passwordlbl.frame.width, 1.0)
        bottomLine1.backgroundColor = UIColor.whiteColor().CGColor
        passwordlbl.borderStyle = UITextBorderStyle.None
        passwordlbl.layer.addSublayer(bottomLine1)
 
    }
    
    @IBAction func signupBtnAction(sender: AnyObject) {
        signupbtn.titleLabel!.font =  UIFont.systemFontOfSize(18, weight: UIFontWeightBold)
        signinbtn.titleLabel!.font =  UIFont.systemFontOfSize(18, weight: UIFontWeightMedium)
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        signupbtn.titleLabel?.attributedText = NSAttributedString(string: "Sign up", attributes: underlineAttribute)
        let underlineAttribute1 = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleNone.rawValue]
        signinbtn.titleLabel?.attributedText = NSAttributedString(string: "Sign in", attributes: underlineAttribute1)
        
    }
    
    @IBAction func signinBtnAction(sender: AnyObject) {
        signupbtn.titleLabel!.font =  UIFont.systemFontOfSize(18, weight: UIFontWeightMedium)
        signinbtn.titleLabel!.font =  UIFont.systemFontOfSize(18, weight: UIFontWeightBold)
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        signinbtn.titleLabel?.attributedText = NSAttributedString(string: "Sign in", attributes: underlineAttribute)
        let underlineAttribute1 = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleNone.rawValue]
        signupbtn.titleLabel?.attributedText = NSAttributedString(string: "Sign up", attributes: underlineAttribute1)
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 800)
        
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == mobilenolbl{
            passwordlbl.becomeFirstResponder()
        }
        
        return true // We do not want UITextField to insert line-breaks.
    }
  
    func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if(textFieldToChange == mobilenolbl){
            characterCountLimit = 10
            
        }
        if(mobilenolbl.text?.characters.count == 9){
            let number = self.mobilenolbl.text! + string
            print(Appconstant.BASE_URL+Appconstant.MANAGER_BUSINESS_ENTITY+Appconstant.URL_CHECK_CUSTOMER+"\(self.mobilenolbl.text!)")
            sendrequesttoserver(Appconstant.BASE_URL+Appconstant.MANAGER_BUSINESS_ENTITY+Appconstant.URL_CHECK_CUSTOMER+number)
            
        }
        if(textFieldToChange == passwordlbl){
            characterCountLimit = 4
        }
        
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= characterCountLimit
    }
    
    
    @IBAction func showhideBtnAction(sender: AnyObject) {
        if(!showpwd){
            showpwd = true
            showhidebtn.setTitle("hide", forState: UIControlState.Normal)
            passwordlbl.secureTextEntry = false
        }
        else{
            showpwd = false
            showhidebtn.setTitle("show", forState: UIControlState.Normal)
            passwordlbl.secureTextEntry = true
        }
        
    }
    
    
    @IBAction func termsandcondition(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: "http://yappay.in/app-static/dcbwallet/dcb_tnc.html")!)
    }
    func sendrequesttoserver(url : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        print(request)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print(error)
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        }
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                
                let item = json["result"]

                if((item["registered"].stringValue == "true")&&(item["isPassCodeSet"].stringValue == "true")){
                    dispatch_async(dispatch_get_main_queue()) {
                    self.passwordlbl.becomeFirstResponder()
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        var alertController:UIAlertController?
                        alertController?.view.tintColor = UIColor.blackColor()
                        alertController = UIAlertController(title: "Cippy",
                            message: "Seems like you have not register with us. Please Register!",
                            preferredStyle: .Alert)
                        
                        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                            self?.performSegueWithIdentifier("to_signup", sender: self)
                            })
                        
                        alertController?.addAction(action)
                        self.presentViewController(alertController!, animated: true, completion: nil)
                        
                        
                    }
                }
        }
        
        task.resume()
        
    }

    @IBAction func signinBlue(sender: AnyObject) {
        
        let mobileno = self.mobilenolbl.text!
        Appconstant.mobileno = mobileno
        Appconstant.pwd = self.passwordlbl.text!
        if((mobileno == "") || (Appconstant.pwd == "")){
            self.presentViewController(Alert().alert("Sorry! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else if(mobileno.characters.count != 10){
            self.presentViewController(Alert().alert("Uh – Oh! Something is wrong, please check the number and try again.", message: ""),animated: true,completion: nil)
        }
        else{
            activityIndicator.startAnimating()
            let mobilenumber = "+91" + mobileno
            let passcode = self.passwordlbl.text!
            let signinviewmodel = SigninViewModel.init(yapcode: passcode, businessId: mobilenumber, business: "DCBWALLET", mobileNumber: mobilenumber, passcode: passcode, appGuid: Appconstant.gcmid)!
            let serializedjson  = JSONSerializer.toJson(signinviewmodel)
            print(serializedjson)
            sendrequesttoserverForSignin(Appconstant.BASE_URL+Appconstant.URL_VERIFY_EXIST_CUSTOMER, values: serializedjson)
            
        }

    }
    func sendrequesttoserverForSignin(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print(error)
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    
                        print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                    
                    
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
//                    dispatch_async(dispatch_get_main_queue()) {
//                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
//                    }
                }
                
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                    let json = JSON(data: data!)
                    
                    let item = json["result"]
                    
                if json["result"] != nil{
                    if(item["entityId"].stringValue != ""){
                        Appconstant.customerid = item["entityId"].stringValue
                        dispatch_async(dispatch_get_main_queue()) {
                            self.sendrequesttoserverToFetchDetailbyEntityID(Appconstant.BASE_URL+Appconstant.URL_FETCH_MERCHANT_DETAILS+Appconstant.customerid)
                            
                        }
                        
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Incorrect Password", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                }
                else{
                    
                    let item1 = json["exception"]
                    if item1["errorCode"].stringValue == "Y1000"{
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Incorrect Password", message: ""),animated: true,completion: nil)
                        }
                    }
                    else if item1["errorCode"] == nil && item1["shortMessage"].stringValue != ""{
                        dispatch_async(dispatch_get_main_queue()) {
                            var msg = item1["shortMessage"].stringValue
                            if msg == "Invalid Yapcode"{
                                msg = "Incorrect Password"
                            }
                            self.presentViewController(Alert().alert(msg, message: ""),animated: true,completion: nil)
                        }
                    }
                        
                    else{
                        
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        }
                    }
                }
        }
        
        task.resume()
        
    }
    
    
    func sendrequesttoserverToFetchDetailbyEntityID(url : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                   self.activityIndicator.stopAnimating()
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString for sign = \(responseString)")
                let json = JSON(data: data!)
//                dispatch_async(dispatch_get_main_queue()) {
//                    self.activityIndicator.stopAnimating()
//                }
                let item = json["result"]
                if(item["firstName"].stringValue != ""){
                    self.state = item["state"].stringValue
                    print(self.state)
                    dispatch_async(dispatch_get_main_queue()) {
                        self.sendrequesttoserverForGetCustomerDetail(Appconstant.BASE_URL+Appconstant.URL_FETCH_CUSTOMER_DETAILS_BY_MOBILENO+Appconstant.mobileno)
                        
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        
                    }
                }
            }
        }
        task.resume()
    }
    
    

    func sendrequesttoserverForGetCustomerDetail(url : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                else{
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString for sign = \(responseString)")
                    let json = JSON(data: data!)
//                    dispatch_async(dispatch_get_main_queue()) {
//                        self.activityIndicator.stopAnimating()
//                    }
                    let item = json["result"]
                    if(item["customerId"].stringValue != ""){
                        DBHelper().cippyDB()
                        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
                        let databasePath = databaseURL.absoluteString
                        let cippyDB = FMDatabase(path: databasePath as String)
                        if cippyDB.open() {
                            let email = "empty"
                            let customertype = "WOC"
                            if(item["firstName"].stringValue == item["lastName"].stringValue){
                                Appconstant.customername = item["firstName"].stringValue
                            }
                            else{
                            Appconstant.customername = item["firstName"].stringValue + " " + item["lastName"].stringValue
                            }
                            print(item["dob"].stringValue)
                            let insert = "INSERT INTO PROFILE_INFO (CUSTOMER_ID,YAP_CODE,CUSTOMER_NAME,MOBILE_NUMBER,EMAIL,CUSTOMER_TYPE) VALUES ('\(item["customerId"].stringValue)','\(Appconstant.pwd)','\(Appconstant.customername)','\(Appconstant.mobileno)','\(email)','\(customertype)')"
                            let result = cippyDB.executeUpdate(insert,
                                withArgumentsInArray: nil)
                            var question = "null"
                            var answer = "null"
                            print(item["description"].stringValue)
                            if(item["description"].stringValue != "" && !item["description"].stringValue.isEmpty){
                                if (item["description"].stringValue.rangeOfString("Sign") == nil) {
                                let description = item["description"].stringValue.componentsSeparatedByString(":")
                                for(var i = 0; i<description.count; i++){
                                    if(i == 0){
                                        question = description[0]
                                        self.descriptions = question
                                    }
                                    else if(i == 1){
                                        let desc = description[1].componentsSeparatedByString(";")
                                        answer = desc[0]
                                        self.descriptions = self.descriptions + ":" + answer
                                    }
                                }
                                    
                                    self.descriptions = self.descriptions + ";SignUp:" + Appconstant.latitude + "," + Appconstant.longitude
                            }
                                else if item["description"].stringValue.rangeOfString("Sign") != nil{
                                    let descriptionarr = item["description"].stringValue.componentsSeparatedByString(";")
                                    for(var arr = 0; arr<descriptionarr.count; arr++){
                                        if arr == 0{
                                    if descriptionarr[0].isEmpty || descriptionarr[0] != ""{
                                    let description = descriptionarr[0].componentsSeparatedByString(":")
                                    for(var i = 0; i<description.count; i++){
                                        if(i == 0){
                                            question = description[0]
                                            self.descriptions = question
                                        }
                                        else if(i == 1){
                                            answer = description[1]
                                            self.descriptions = self.descriptions + ":" + answer + ";"
                                        }
                                      }
                                        if descriptionarr[0].rangeOfString("SignUp:") != nil{
                                            question = ""
                                            answer = ""
                                        }
                                    }
                                        }
                                        else if arr == 1{
                                            if !descriptionarr[1].isEmpty || descriptionarr[1] != ""{
                                            let description1 = descriptionarr[1].componentsSeparatedByString(":")
                                            if description1.count > 1{
                                            let description = description1[1].componentsSeparatedByString(",")
                                            for(var i = 0; i<description.count; i++){
                                                if(i == 0){
                                                    
                                                    Appconstant.signuplat = description[0]
                                                    self.descriptions = self.descriptions + "SignUp:" + Appconstant.signuplat
                                                }
                                                else if(i == 1){
                                                    Appconstant.signuplong = description[1]
                                                    self.descriptions = self.descriptions + "," + Appconstant.signuplong
                                                }
                                            }
                                            }
                                        }
                                        }
                                  }
                                }
                            }
                            let imgurl = ""
                            print(item["gender"].stringValue)
                            let defaults = NSUserDefaults.standardUserDefaults()
//                            let secure_q = defaults.stringForKey("security_q")
                            defaults.setObject(question, forKey: "security_q")
                            defaults.setObject(item["address"].stringValue, forKey: "addr1")
                            defaults.setObject(item["address2"].stringValue, forKey: "addr2")
//                            question = question.stringByReplacingOccurrencesOfString("'", withString: "\'", options: NSStringCompareOptions.LiteralSearch, range: nil)
                            let insertsql = "INSERT INTO CUSTOMERDETAIL (CUSTOMER_ID,DATE_OF_BIRTH,CUSTOMER_BANK,IMAGE_PATH,ADDRESS_LINE_1,ADDRESS_LINE_2,CITY,STATE,PIN,SECURITY_QUESTION,SECURITY_ANSWER,SIGNUPLAT,SIGNUPLONG,GENDER) VALUES ('\(Appconstant.customerid)','\(item["dob"].stringValue)','\("DCB")','\(imgurl)','\("")','\("")','\(item["city"].stringValue)','\(self.state)','\(item["pincode"].stringValue)','\("")','\(answer)','\(Appconstant.signuplat)','\(Appconstant.signuplong)','\(item["gender"].stringValue)')"
                            
                            let result2 = cippyDB.executeUpdate(insertsql,
                                withArgumentsInArray: nil)
                            
                            if !result || !result2 {
                                //   status.text = "Failed to add contact"
                                dispatch_async(dispatch_get_main_queue()) {
                                    self.activityIndicator.stopAnimating()
                                }
                                print("Error: \(cippyDB.lastErrorMessage())")
                                dispatch_async(dispatch_get_main_queue()) {
                                    self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                                    
                                }
                            }
                            else{
                                dispatch_async(dispatch_get_main_queue()) {
                                    self.descriptions = self.descriptions + ";SignIn:" + Appconstant.latitude + "," + Appconstant.longitude
                                    print(self.descriptions)
                                    let updateviewmodel = UpdateDescriptionViewModel.init(entityId: Appconstant.customerid, appGuid: Appconstant.gcmid, description: self.descriptions)!
                                    let serializedjson  = JSONSerializer.toJson(updateviewmodel)
                                    print(serializedjson)
                                    self.activityIndicator.startAnimating()
                                    self.sendrequesttoserverForUpdateEntity(Appconstant.BASE_URL+Appconstant.URL_UPDATE_ENTITY, values: serializedjson)
                                }
                            }
                        }
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                            
                        }
                    }
                }
        }
        
        task.resume()
        
    }

    func sendrequesttoserverForUpdateEntity(url : String, values: String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        
        
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = values.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print(error)
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                }
                print("statusCode should be 200, but is \(httpStatus.statusCode)")  // check for http errors
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                
                if(json["result"].stringValue == "true"){
                    dispatch_async(dispatch_get_main_queue()) {
                      self.performSegueWithIdentifier("signin_home", sender: self)
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                    }
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        
                    }
                }
            }
        }
        
        task.resume()
        
    }
    
    
    
    
    @IBAction func useotpbtnAction(sender: AnyObject) {
        let mobileno = self.mobilenolbl.text!
        print(mobileno.characters.count)
        if(mobileno == "" || mobileno.characters.count != 10){
            self.presentViewController(Alert().alert("Sorry! Seems you forgot to fill some field..We need you to fill them and try again!", message: ""),animated: true,completion: nil)
        }
        else{
            Appconstant.mobileno = self.mobilenolbl.text!
            print(Appconstant.BASE_URL+Appconstant.URL_GENERATE_OTP+self.mobilenolbl.text!)
            self.sendrequesttoserverForGenerateOTP(Appconstant.BASE_URL+Appconstant.URL_GENERATE_OTP+self.mobilenolbl.text!)
        }
    }
    func sendrequesttoserverForGenerateOTP(url : String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    if Reachability.isConnectedToNetwork() == true {
                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                
                let item = json["result"]
                if(item["success"].stringValue == "true"){
                    dispatch_async(dispatch_get_main_queue()) {
                        Appconstant.otp = item["otp"].stringValue
                        Appconstant.customerid = item["customerId"].stringValue
                        Appconstant.customername = item["customerName"].stringValue
                        self.performSegueWithIdentifier("signin_otp", sender: self)
                    }
                }
               
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        dispatch_async(dispatch_get_main_queue()) {
                            self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                        }
                    }
                }
        }
        
        task.resume()
        
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "signin_otp") {
            let nextview = segue.destinationViewController as! OTPViewController
            nextview.fromsignin = true
        }
    }
    
    func dismissKeyboard(){
        self.view.endEditing(true)
    }

}
