//
//  DBHelper.swift
//  YAP Customer
//
//  Created by Admin on 4/7/16.
//  Copyright © 2016 yap. All rights reserved.
//

import Foundation

class DBHelper {
    
    var databasePath = NSString()
    func cippyDB()
    {
        let filemgr = NSFileManager.defaultManager()
        //     let dirPaths =
        NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
            .UserDomainMask, true)
        
        //    let docsDir = dirPaths[0]
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
        databasePath = databaseURL.absoluteString
        //databasePath = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("yapcustomer.db")
        // databasePath = docsDir.stringByAppendingPathComponent("yapcustomer.db")
        //   stringByAppendingPathComponent("yapcustomer.db")
        print(databasePath as String)
        if !filemgr.fileExistsAtPath(databasePath as String)
        {
            
            CreateTable();
        }
        
    }
    

    
    internal func CreateTable() {
        let cippyDB = FMDatabase(path: databasePath as String)
        
        if cippyDB.open() {
            
            
            let profile_info_table = "CREATE TABLE IF NOT EXISTS PROFILE_INFO (ID INTEGER PRIMARY KEY AUTOINCREMENT, CUSTOMER_ID TEXT, YAP_CODE TEXT, CUSTOMER_NAME TEXT, MOBILE_NUMBER TEXT, CARD_NUMBER TEXT, EMAIL TEXT, CUSTOMER_TYPE TEXT)"
            if !cippyDB.executeStatements(profile_info_table) {
                print("Error: \(cippyDB.lastErrorMessage())")
            }
            
            let profile_table = "CREATE TABLE IF NOT EXISTS CUSTOMERDETAIL (ID INTEGER PRIMARY KEY AUTOINCREMENT, CUSTOMER_ID TEXT, DATE_OF_BIRTH TEXT, CUSTOMER_BANK TEXT, IMAGE_PATH TEXT, ADDRESS_LINE_1 TEXT, ADDRESS_LINE_2 TEXT, CITY TEXT, STATE TEXT, PIN TEXT, SECURITY_QUESTION TEXT, SECURITY_ANSWER TEXT, SIGNUPLAT TEXT, SIGNUPLONG TEXT, GENDER TEXT)"
            if !cippyDB.executeStatements(profile_table) {
                print("Error: \(cippyDB.lastErrorMessage())")
            }
            
            let recharge_company_table = "CREATE TABLE IF NOT EXISTS RECHARGECOMPANY (ID INTEGER PRIMARY KEY AUTOINCREMENT, TELECOM_NAME TEXT, TELECOM_OPERATORCODE TEXT, TELECOM_OPERATORTYPE TEXT, TELECOM_SPECIAL TEXT)"
            if !cippyDB.executeStatements(recharge_company_table) {
                print("Error: \(cippyDB.lastErrorMessage())")
            }
            
            
            let transaction_create_table = "CREATE TABLE IF NOT EXISTS TRANSACTIONS (ID INTEGER PRIMARY KEY AUTOINCREMENT,  AMOUNT TEXT, TRANSACTION_TYPE TEXT, TYPE TEXT, TIME TEXT, TX_REF TEXT, BENEFICIARY_NAME TEXT, BENEFICIARY_ID TEXT, DESCRIPTION TEXT, OTHER_PARTY_NAME TEXT, OTHER_PARTY_ID TEXT, TXN_ORIGIN TEXT, TRANSACTION_STATUS TEXT)"
            if !cippyDB.executeStatements(transaction_create_table) {
                print("Error: \(cippyDB.lastErrorMessage())")
            }
            
            let notification_table = "CREATE TABLE IF NOT EXISTS NOTIFICATION (ID INTEGER PRIMARY KEY AUTOINCREMENT, AMOUNT TEXT, DATE TEXT, FUND_REQ_NUMBER TEXT, STATUS TEXT, APPROVE_DECLINE, REQ_TO_US TEXT, ISREAD TEXT, PHONENUMBER TEXT)"
            if !cippyDB.executeStatements(notification_table) {
                print("Error: \(cippyDB.lastErrorMessage())")
            }
            
            
            let operators_table = "CREATE TABLE IF NOT EXISTS OPERATORS (ID INTEGER PRIMARY KEY AUTOINCREMENT, OPERATORCODE TEXT, OPERATORNAME TEXT, OPERATORID TEXT, OPERATORTYPE TEXT, SPECIAL TEXT)"
            if !cippyDB.executeStatements(operators_table) {
                print("Error: \(cippyDB.lastErrorMessage())")
            }
            
            cippyDB.close()
        }
        
    }

}

