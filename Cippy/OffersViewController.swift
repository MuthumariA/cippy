//
//  OffersViewController.swift
//  Cippy
//
//  Created by apple on 25/01/17.
//  Copyright © 2017 vertace. All rights reserved.
//

import UIKit

class OffersViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    
    @IBOutlet weak var badgebtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var citybtn: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var name = [String]()
    var city = [String]()
    var address = [String]()
    var disclaimer = [String]()
    var phoneno = [String]()
    var offer = [String]()
    var selected = [Bool]()
    var imgurl = [String]()
    var citiesname = [String]()
    var code = [String]()
    var citycode = [String]()
    var img = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initialize()
        self.activityIndicator.startAnimating()
        getCitieslistFromServer(Appconstant.GET_CITYLIST_OFFER)
    }
    
    func initialize(){
        badgebtn.userInteractionEnabled = false
        badgebtn.layer.cornerRadius = self.badgebtn.frame.size.height/2
        if(Appconstant.notificationcount > 0){
            badgebtn.hidden = false
            badgebtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
        }
        else{
            badgebtn.hidden = true
        }
        citybtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell!
        print(indexPath.row)
        
        let offerimg = cell.viewWithTag(1) as! UIImageView
        let namelbl = cell.viewWithTag(2) as! UILabel
        let citylbl = cell.viewWithTag(3) as! UILabel
        let offerlbl = cell.viewWithTag(5) as! UILabel
        let addrlbl = cell.viewWithTag(6) as! UILabel
        let disclaimerlbl = cell.viewWithTag(7) as! UILabel
        let phonenolbl = cell.viewWithTag(13) as! UILabel
        let payatshopbtn = cell.viewWithTag(9) as! UIButton
        let reportlbl = cell.viewWithTag(11) as! UILabel
        let arrowimg = cell.viewWithTag(12) as! UIImageView
        
        payatshopbtn.layer.cornerRadius = 7
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        phonenolbl.attributedText = NSAttributedString(string: self.phoneno[indexPath.row], attributes: underlineAttribute)
        reportlbl.attributedText = NSAttributedString(string: "Report Offer/Store", attributes: underlineAttribute)
        disclaimerlbl.text = "DISCLAIMER: " + self.disclaimer[indexPath.row]
        addrlbl.text = self.address[indexPath.row]
        offerlbl.text = self.offer[indexPath.row]
        citylbl.text = self.city[indexPath.row]
        namelbl.text = self.name[indexPath.row]
        offerimg.image = img[indexPath.row]
        offerimg.backgroundColor = UIColor.whiteColor()
        if(indexPath.row % 2 == 0){
            cell.backgroundColor = UIColor(red: 212.0/255.0, green: 242.0/255.0, blue: 246.0/255.0, alpha: 1)
            //          offerimg.backgroundColor = UIColor(red: 212.0/255.0, green: 242.0/255.0, blue: 246.0/255.0, alpha: 1)
        }
        else{
            cell.backgroundColor = UIColor(red: 238.0/255.0, green: 247.0/255.0, blue: 250.0/255.0, alpha: 1)
            //          offerimg.backgroundColor = UIColor(red: 238.0/255.0, green: 247.0/255.0, blue: 250.0/255.0, alpha: 1)
        }
        
        if selected[indexPath.row]{
            arrowimg.image = UIImage(named: "up_blue.png")
            payatshopbtn.hidden = false
            phonenolbl.hidden = false
            reportlbl.hidden = false
            disclaimerlbl.hidden = false
            addrlbl.hidden = false
            
        }
        else{
            arrowimg.image = UIImage(named: "down_blue.png")
            payatshopbtn.hidden = true
            phonenolbl.hidden = true
            reportlbl.hidden = true
            disclaimerlbl.hidden = true
            addrlbl.hidden = false
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.img.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let selectionColor = UIView() as UIView
        selectionColor.layer.borderWidth = 1
        selectionColor.layer.borderColor = UIColor.clearColor().CGColor
        selectionColor.backgroundColor = UIColor.clearColor()
        cell.selectedBackgroundView = selectionColor
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if selected[indexPath.row]{
            selected[indexPath.row] = false
        }
        else{
            selected[indexPath.row] = true
        }
        self.tableView.reloadRowsAtIndexPaths([tableView.indexPathForSelectedRow!], withRowAnimation: .Fade)
        if indexPath.row == self.img.count-1{
            self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: indexPath.row, inSection: 0), atScrollPosition: UITableViewScrollPosition.Middle, animated: true)
        }
        
        
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        print(indexPath.row)
        if selected[indexPath.row]{
            return 276
        }
        else{
            return 90
        }
        return 90
    }
    
    func getCitieslistFromServer(url: String){
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {
                self.activityIndicator.stopAnimating()  // check for fundamental networking error
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                dispatch_async(dispatch_get_main_queue(), {
                    self.activityIndicator.stopAnimating()
                })
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                for item in json["Cities"].arrayValue{
                    self.citiesname.append(item["Name"].stringValue)
                    var codename = item["Name"].stringValue.lowercaseString
                    codename = codename.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    self.code.append(codename)
                    self.citycode.append(item["Code"].stringValue)
                    
                }
                if self.citiesname.count>0{
                    dispatch_async(dispatch_get_main_queue(), {
                    self.citybtn.setTitle(self.citiesname[self.citycode.count-1], forState: .Normal)
                        })
                    self.getOfferListFromServer(Appconstant.GET_OFFERS_WITH_CITYNAME+self.code[self.citycode.count-1]+"/offers.json")
                    
                }
                else{
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        self.activityIndicator.stopAnimating()
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    })
                }
            }
        }
        
        task.resume()
        
        
    }
    
    func getOfferListFromServer(url: String){
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                self.activityIndicator.stopAnimating()
                self.tableView.userInteractionEnabled = true
                if Reachability.isConnectedToNetwork() == true {
                } else {
                    print("Internet connection FAILED")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                    }
                    
                }
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                dispatch_async(dispatch_get_main_queue(), {
                    self.activityIndicator.stopAnimating()
                })
                self.tableView.userInteractionEnabled = true
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                }
            }
            else{
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                let json = JSON(data: data!)
                self.name.removeAll()
                self.city.removeAll()
                self.address.removeAll()
                self.disclaimer.removeAll()
                self.phoneno.removeAll()
                self.offer.removeAll()
                self.selected.removeAll()
                self.imgurl.removeAll()
                self.img.removeAll()
                
                for item in json["result"].arrayValue{
                    self.name.append(item["OutletName"].stringValue)
                    self.city.append(item["City"].stringValue)
                    self.address.append(item["Address"].stringValue)
                    self.disclaimer.append(item["Offers"].stringValue)
                    self.phoneno.append(item["OwnerContactNumber"].stringValue)
                    self.offer.append(item["OfferPercentage"].stringValue)
                    self.selected.append(false)
                    let url = item["ImageUrl"].stringValue.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    self.imgurl.append(url)
                    if let data = NSData(contentsOfURL: NSURL(string:url)!){
                        let images =  UIImage(data: NSData(contentsOfURL: NSURL(string:url)!)!)
                        self.img.append(images!)
                    }
                    else{
                        self.img.append(UIImage(named: "add.png")!)
                    }
                }
                print(self.imgurl)
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                self.tableView.userInteractionEnabled = true
                self.tableView.reloadData()
            }
            
        }
        
        task.resume()
        
        
    }
    
    @IBAction func phoneBtnAction(sender: AnyObject) {
        let point = sender.convertPoint(CGPointZero, toView: tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)!
        
        dispatch_async(dispatch_get_main_queue()) {
            
            var alertController:UIAlertController?
            alertController?.view.tintColor = UIColor.blackColor()
            alertController = UIAlertController(title: "Cippy",
                                                message: "Are you sure you want to call?",
                                                preferredStyle: .Alert)
            
            let action = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                dispatch_async(dispatch_get_main_queue()) {
                    
                }
                
                })
            let action1 = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                dispatch_async(dispatch_get_main_queue()) {
                    let phonenumber = "tel://" + self!.phoneno[indexPath.row]
                    let url:NSURL = NSURL(string: phonenumber)!
                    UIApplication.sharedApplication().openURL(url)
                }
                
                })
            
            alertController?.addAction(action)
            alertController?.addAction(action1)
            self.presentViewController(alertController!, animated: true, completion: nil)
        }
        
        
        
    }
    
    @IBAction func cityBtnAction(sender: AnyObject) {
        if self.citiesname.count>0{
            let alertView: UIAlertView = UIAlertView()
            alertView.delegate = self
            for(var i = 0; i<self.citiesname.count; i++){
                alertView.addButtonWithTitle(self.citiesname[i])
            }
            alertView.addButtonWithTitle("Cancel")
            alertView.show()
        }
        else{
            dispatch_async(dispatch_get_main_queue()) {
                self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
            }
        }
    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        if buttonIndex != self.citiesname.count{
            self.activityIndicator.startAnimating()
            self.tableView.userInteractionEnabled = false
            self.citybtn.setTitle(self.citiesname[buttonIndex], forState: .Normal)
            print(Appconstant.GET_OFFERS_WITH_CITYNAME+self.code[buttonIndex]+"/offers.json")
            self.getOfferListFromServer(Appconstant.GET_OFFERS_WITH_CITYNAME+self.code[buttonIndex]+"/offers.json")
        }
        
    }
    @IBAction func payatshopBtnAction(sender: AnyObject) {
        self.performSegueWithIdentifier("offer_pay", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "offer_pay") {
            let nextview = segue.destinationViewController as! PayatStoreViewController
            nextview.fromoffer = true
            
        }
    }
}
