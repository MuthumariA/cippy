//
//  ViewController.swift
//  Cippy
//
//  Created by apple on 15/11/16.
//  Copyright © 2016 vertace. All rights reserved.
//

import UIKit
import StoreKit
import MapKit
import CoreLocation

class ViewController: UIViewController, SKStoreProductViewControllerDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var progressView: UIProgressView!
    
    var poseDuration: Float = 1.0
    var indexProgressBar: Float = 0.0
    var index = 0
    var final = 10
    var timer = NSTimer()
    var today = NSDate()
    var today_date = ""
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        } else {
            print("Location services are not enabled");
        }
//        var currentLocation = CLLocation()
        
//        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.AuthorizedWhenInUse ||
//            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Authorized){
//            
//            currentLocation = locationManager.location!
//            let latitude = currentLocation.coordinate.latitude
//            let longitude = currentLocation.coordinate.longitude
//            Appconstant.latitude = String(format: "%.2f", latitude)
//            Appconstant.longitude = String(format: "%.2f", longitude)
//            print(latitude)
//            print(longitude)
//            
//        }
       
        print(today)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        today_date = dateFormatter.stringFromDate(today)
        
        progressView.layer.cornerRadius = 10
        progressView.progress = 0.0
        self.navigationController?.navigationBarHidden = true
        
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        switch status {
        case .AuthorizedWhenInUse:
            self.locationManager.startUpdatingLocation()
        case .Denied:
            print("denied")
        case .NotDetermined:
             print("not determined")
        default:
            break
        }
    }
    
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        locationManager.stopUpdatingLocation()
//        if ((error) != nil) {
            print(error)
        
    }
    
    func locationManager(manager: CLLocationManager,   didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        Appconstant.latitude = String(format: "%.2f", locValue.latitude)
        Appconstant.longitude = String(format: "%.2f", locValue.longitude)
        locationManager.stopUpdatingLocation()
    }
    
//    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        
//        let location = locations.last
//        
//        let centre = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
//        
//        let region = MKCoordinateRegion(center: centre, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
//        
//        print(region)
//        self.locationManager.stopUpdatingLocation()
//        
//    }
//    
//    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
//        
//        print("Errors: " + error.localizedDescription)
//        
//    }
//    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        timer = NSTimer.scheduledTimerWithTimeInterval(
            0.1, target: self, selector: #selector(ViewController.setProgressBar), userInfo: nil, repeats: true)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        timer = NSTimer.scheduledTimerWithTimeInterval(
            0.1, target: self, selector: #selector(ViewController.setProgressBar), userInfo: nil, repeats: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    
    func setProgressBar()
    {
        if index == final
        {
//           self.calldatabase()
            getversionfromServer(Appconstant.VERSION_CHECK)
            timer.invalidate()
            
            
        }
        progressView.progress = indexProgressBar
        
        // increment the counter
        index += 2
        indexProgressBar += 0.2
    }

    func calldatabase(){
        DBHelper().cippyDB()
        let databaseURL = NSURL(fileURLWithPath:NSTemporaryDirectory()).URLByAppendingPathComponent("cippy.db")
        let databasePath = databaseURL.absoluteString
        let cippyDB = FMDatabase(path: databasePath as String)
        if cippyDB.open() {
            
            let selectSQL = "SELECT * FROM PROFILE_INFO"
            let selectSQL1 = "SELECT * FROM CUSTOMERDETAIL"
            
            let result:FMResultSet! = cippyDB.executeQuery(selectSQL,
                withArgumentsInArray: nil)
            let result1:FMResultSet! = cippyDB.executeQuery(selectSQL1,
                                                           withArgumentsInArray: nil)
            if (result.next() && result1.next()){
                print(result.stringForColumn("YAP_CODE"))
                Appconstant.customerid = result.stringForColumn("CUSTOMER_ID")
                Appconstant.pwd = result.stringForColumn("YAP_CODE")
                Appconstant.customername = result.stringForColumn("CUSTOMER_NAME")
                Appconstant.mobileno = result.stringForColumn("MOBILE_NUMBER")
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("initial_home", sender: self)
                }
                
            }
            else{
                
                if cippyDB.open() {
                    Appconstant.mainbalance = ""
                    let deleteSQL =  "DELETE FROM PROFILE_INFO"
                    
                    let deleteSQL2 =  "DELETE FROM CUSTOMERDETAIL"
                    
                    let result = cippyDB.executeUpdate(deleteSQL,
                                                       withArgumentsInArray: nil)
                    let result2 = cippyDB.executeUpdate(deleteSQL2,
                                                        withArgumentsInArray: nil)
                    if !result && !result2{
                        //   status.text = "Failed to add contact"
                        print("Error: \(cippyDB.lastErrorMessage())")
                    }
                }
                cippyDB.close()
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("to_signin", sender: self)
                }
                
                
//                item["minimumRequiredVersion"].intValue   item["currentVersion"].intValue
            }
            
        }
        cippyDB.close()
    }


    
    
    func getversionfromServer(url: String){
        let defaults = NSUserDefaults.standardUserDefaults()

        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
//        request.addValue("BaYsic YWRtaW46WRtaW4=", forHTTPHeaderField: "Authorization")
//        request.addValue(Appconstant.TENANT, forHTTPHeaderField: "TENANT")
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            { data, response, error in
                guard error == nil && data != nil else {
                    print(error)    // check for fundamental networking error
//                    if Reachability.isConnectedToNetwork() == true {
//                    } else {
                        print("Internet connection FAILED")
                        self.presentViewController(Alert().alert("Internet is being a bummer.. Please check net connections and try again!", message: ""),animated: true,completion: nil)
                        
//                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.presentViewController(Alert().alert("Uh - Oh! Something went wrong, let's start again!", message: ""),animated: true,completion: nil)
                    }
                }
                else{
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                    let json = JSON(data: data!)
                    let item = json["result"]
                    let nsObject = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as! String
                    let appversion = nsObject.stringByReplacingOccurrencesOfString(".", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    print(appversion)
                   if Int(appversion) < item["minimumRequiredVersion"].intValue{
                        dispatch_async(dispatch_get_main_queue()) {
                       
                            var alertController:UIAlertController?
                            alertController?.view.tintColor = UIColor.blackColor()
                            alertController = UIAlertController(title: "You must update DCB Wallet to continue using",
                                message: "",
                                preferredStyle: .Alert)
                            
                            let action = UIAlertAction(title: "Update", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                                    UIApplication.sharedApplication().openURL(NSURL(string: "itms-apps://itunes.apple.com/app/bars/id1191324277")!)
                                
                                })
                            let action1 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
                                    exit(0)
                                
                                })
                            
                            alertController?.addAction(action)
                            alertController?.addAction(action1)
                            self.presentViewController(alertController!, animated: true, completion: nil)
                        }
                    }
                    else if Int(appversion) < item["currentVersion"].intValue{
                        
                        var updatetime = defaults.stringForKey("cippyupdatetime")
                        var temp_date = NSDate()
                        print(updatetime)
                        if(updatetime != nil){
                            let currentdate = NSDate()
                            let calendar = NSCalendar.currentCalendar()
                            let components = calendar.components([.Day , .Month , .Year], fromDate: currentdate)
                            let year =  components.year
                            let month = components.month
                            let day = components.day
//                            let time = components.hour
                            
                            let dateFormatter = NSDateFormatter()
                            let dateFormatter1 = NSDateFormatter()
                            let dateFormatter2 = NSDateFormatter()
                            let dateFormatter3 = NSDateFormatter()
                            let dateFormatter4 = NSDateFormatter()
//                            let dateFormatter5 = NSDateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
                            dateFormatter.timeZone = NSTimeZone(name: "UTC")
                            dateFormatter1.dateFormat = "dd/MM/yy"
                            dateFormatter1.timeZone = NSTimeZone(name: "UTC")
                            dateFormatter2.dateFormat = "yyyy"
                            dateFormatter2.timeZone = NSTimeZone(name: "UTC")
                            dateFormatter3.dateFormat = "MM"
                            dateFormatter3.timeZone = NSTimeZone(name: "UTC")
                            dateFormatter4.dateFormat = "dd"
                            dateFormatter4.timeZone = NSTimeZone(name: "UTC")
//                            dateFormatter5.dateFormat = "HH"
//                            dateFormatter5.timeZone = NSTimeZone(name: "UTC")
                            print(dateFormatter.dateFromString(updatetime!))
                           
                            temp_date = dateFormatter.dateFromString(updatetime!)!
                            let temp_year = Int(dateFormatter2.stringFromDate(temp_date))!
                            let temp_month = Int(dateFormatter3.stringFromDate(temp_date))!
                            let temp_day = Int(dateFormatter4.stringFromDate(temp_date))!
//                            let temp_hour = Int(dateFormatter5.stringFromDate(temp_date))!
                            
                            if(Int(year)<=temp_year && Int(month)<=temp_month && Int(day)<temp_day){
                                self.showalert("A new version of DCB Wallet is available")
                                self.calldatabase()
                            }
                            else{
                                self.calldatabase()
                            }
                            
                        }
                        else{
                            self.calldatabase()
                        }
                        
                        defaults.setObject(self.today_date, forKey: "cippyupdatetime")
                        print(defaults.stringForKey("cippyupdatetime"))
                        
                    }
                    else{
                         self.calldatabase()
                    }
                }
        }
        
        task.resume()
        
        
    }


    
    func showalert(str: String){
        var alertController:UIAlertController?
        alertController?.view.tintColor = UIColor.blackColor()
        alertController = UIAlertController(title: str,
            message: "",
            preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "Update", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
            dispatch_async(dispatch_get_main_queue()) {
                UIApplication.sharedApplication().openURL(NSURL(string: "itms-apps://itunes.apple.com/app/bars/id1191324277")!)
            }
            
            })
        let action1 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {[weak self](paramAction:UIAlertAction!) in
             self!.calldatabase()
            
            })
        
        alertController?.addAction(action)
        alertController?.addAction(action1)
        
        
        self.presentViewController(alertController!, animated: true, completion: nil)
    }

}



class Alert: UIViewController {
    func alert(title: String,message: String)-> UIAlertController
    {
        let alert = UIAlertController(title: title,message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK",style: UIAlertActionStyle.Default, handler: nil))
        return alert
    }
    
}

