


//
//  UtilityViewController.swift
//  Cippy
//
//  Created by Vertace on 05/04/17.
//  Copyright © 2017 vertace. All rights reserved.
//

import UIKit

class UtilityViewController: UIViewController {

    @IBOutlet weak var prepaidbtn: UIButton!
    @IBOutlet weak var postpaidbtn: UIButton!
    @IBOutlet weak var dthbtn: UIButton!
    @IBOutlet weak var landlinebtn: UIButton!
    @IBOutlet weak var insurancebtn: UIButton!
    @IBOutlet weak var electricitybtn: UIButton!
    @IBOutlet weak var gasbtn: UIButton!
    @IBOutlet weak var taxbtn: UIButton!
    @IBOutlet weak var corporatetelecombtn: UIButton!
    @IBOutlet weak var waterbtn: UIButton!
    @IBOutlet weak var badgeBtn: UIButton!
    
    var btnname = ""
    var placeholder1 = ""
    var placeholder2 = ""
    var placeholder3 = ""
    var placeholder4 = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initialize()
        getplanlist()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func initialize(){
        prepaidbtn.layer.cornerRadius = 5
        postpaidbtn.layer.cornerRadius = 5
        dthbtn.layer.cornerRadius = 5
        landlinebtn.layer.cornerRadius = 5
        insurancebtn.layer.cornerRadius = 5
        electricitybtn.layer.cornerRadius = 5
        gasbtn.layer.cornerRadius = 5
        taxbtn.layer.cornerRadius = 5
        corporatetelecombtn.layer.cornerRadius = 5
        waterbtn.layer.cornerRadius = 5
        badgeBtn.userInteractionEnabled = false
        badgeBtn.layer.cornerRadius  = self.badgeBtn.frame.size.height/2
        if(Appconstant.notificationcount > 0){
            badgeBtn.hidden = false
            badgeBtn.setTitle("\(Appconstant.notificationcount)", forState: .Normal)
        }
        else{
            badgeBtn.hidden = true
        }
    }
    
    func getplanlist(){
        let location = NSBundle.mainBundle().pathForResource("Providers", ofType: "txt")
        
        let fileContent = try? NSString(contentsOfFile: location!, encoding: NSUTF8StringEncoding)
        let data = fileContent!.dataUsingEncoding(NSUTF8StringEncoding)
        let json = JSON(data: data!)
        for item in json["result"].arrayValue{
            if(item["operatorType"].stringValue == "PREPAID"){
                Operatorss.pre_operatorName.append(item["operatorName"].stringValue)
                Operatorss.pre_operatorCode.append(item["operatorCode"].stringValue)
                Operatorss.pre_operatorType.append(item["operatorType"].stringValue)
                Operatorss.pre_special.append(item["special"].boolValue)
                Operatorss.pre_minimumbillamt.append(item["operatorId"].stringValue)
                Operatorss.pre_auth1.append(item[""].stringValue)
                Operatorss.pre_auth2.append(item[""].stringValue)
                Operatorss.pre_auth3.append(item[""].stringValue)
            }
            else if(item["operatorType"].stringValue == "POSTPAID"){
                Operators.post_operatorName.append(item["operatorName"].stringValue)
                Operatorss.post_operatorCode.append(item["operatorCode"].stringValue)
                Operatorss.post_operatorType.append(item["operatorType"].stringValue)
                Operatorss.post_special.append(item["special"].boolValue)
                Operatorss.post_minimumbillamt.append(item["operatorId"].stringValue)
                Operatorss.post_auth1.append(item[""].stringValue)
                Operatorss.post_auth2.append(item[""].stringValue)
                Operatorss.post_auth3.append(item[""].stringValue)
            }
            else if(item["operatorType"].stringValue == "DTH"){
                Operatorss.dth_operatorName.append(item["operatorName"].stringValue)
                Operatorss.dth_operatorCode.append(item["operatorCode"].stringValue)
                Operatorss.dth_operatorType.append(item["operatorType"].stringValue)
                Operatorss.dth_special.append(item["special"].boolValue)
                Operatorss.dth_minimumbillamt.append(item["operatorId"].stringValue)
                Operatorss.dth_auth1.append(item[""].stringValue)
                Operatorss.dth_auth2.append(item[""].stringValue)
                Operatorss.dth_auth3.append(item[""].stringValue)
            }
        }

    }
    
    
    @IBAction func prepaidBtnAction(sender: AnyObject) {
        btnname = "MOBILE RECHARGE"
        placeholder1 = "Mobile Number"
        placeholder2 = "Amount"
        self.performSegueWithIdentifier("to_recharge", sender: self)
    }

    @IBAction func postpaidBtnAction(sender: AnyObject) {
        btnname = "MOBILE BILL"
        placeholder1 = "Mobile Number"
        placeholder2 = "Amount"
        self.performSegueWithIdentifier("to_recharge", sender: self)
    }
    
    @IBAction func dthBtnAction(sender: AnyObject) {
        btnname = "DTH"
        placeholder1 = "Customer ID"
        placeholder2 = "Amount"
        self.performSegueWithIdentifier("to_recharge", sender: self)
    }
 
    @IBAction func landlineBtnAction(sender: AnyObject) {
        btnname = "LANDLINE/BROADBAND"
        placeholder1 = "Phone Number"
        placeholder2 = "Amount"
        self.performSegueWithIdentifier("to_recharge", sender: self)
    }
    
    @IBAction func insuranceBtnAction(sender: AnyObject) {
        btnname = "INSURANCE"
        placeholder1 = "Phone Number"
        placeholder2 = "Amount"
        self.performSegueWithIdentifier("to_recharge", sender: self)
    }
    
    @IBAction func electricityBtnAction(sender: AnyObject) {
        btnname = "ELECTRICITY"
        placeholder1 = "Phone Number"
        placeholder2 = "Amount"
        self.performSegueWithIdentifier("to_recharge", sender: self)
    }
    
    @IBAction func gasBtnAction(sender: AnyObject) {
        btnname = "GAS"
        placeholder1 = "Phone Number"
        placeholder2 = "Amount"
        self.performSegueWithIdentifier("to_recharge", sender: self)
    }
    
    @IBAction func taxBtnAction(sender: AnyObject) {
        btnname = "TAX"
        placeholder1 = "Phone Number"
        placeholder2 = "Amount"
        self.performSegueWithIdentifier("to_recharge", sender: self)
    }
    
    @IBAction func corporateBtnAction(sender: AnyObject) {
        btnname = "CORPORATE TELECOM"
        placeholder1 = "Phone Number"
        placeholder2 = "Amount"
        self.performSegueWithIdentifier("to_recharge", sender: self)
    }
    
    @IBAction func waterBtnAction(sender: AnyObject) {
        btnname = "WATER"
        placeholder1 = "Phone Number"
        placeholder2 = "Amount"
        self.performSegueWithIdentifier("to_recharge", sender: self)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "to_recharge") {
            let nextview = segue.destinationViewController as! UtilityRechargeViewController
            nextview.titlename = self.btnname
            nextview.placeholder1 = self.placeholder1
            nextview.placeholder2 = self.placeholder2
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
