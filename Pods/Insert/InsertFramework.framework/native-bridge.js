function getFromContext(context,name) {
  return context[name];
}

function getOrientation() {
  var orientation = IIOInsertNativeBridge.getDeviceOrientation();
  return orientation;
}

function logD(msg) {
  IIOInsertNativeBridge.log(msg);
};

function StandIn(id){
  this.elementId = id;
};

StandIn.prototype.getPageNumber = function() {
  return IIOInsertNativeBridge.getPageNumber(this.elementId);
};

StandIn.prototype.getAnswers = function(){
  
  var d = IIOInsertNativeBridge.getAnswers(this.elementId);
  var c = d['additionalInfo'];
  return c;
};

var dispatchActions = function(actions, context){
  IIOInsertNativeBridge.dispatchActionsContext(actions,context);
};

var dispatchTriggerActions = function(actions, context){
  IIOInsertNativeBridge.dispatchTriggerActionsContext(actions,context);
};

var findElementById = function(id) {
  return new StandIn(id);
};

function insertRun(context){}
