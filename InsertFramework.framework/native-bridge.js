function getFromContext(context,name) {
  return context[name];
}

function getOrientation() {
  return require('IIOInsertNativeBridge').getDeviceOrientation();
}

function logD(msg){
  require('NSLog');
};

function StandIn(id){
  this.elementId = id;
};

StandIn.prototype.getPageNumber = function(){
  require('IIOInsertNativeBridge');
  return IIOInsertNativeBridge.getPageNumber(this.elementId);
};

StandIn.prototype.getAnswers = function(){
  require('IIOInsertNativeBridge');
  var d = IIOInsertNativeBridge.getAnswers(this.elementId);
  var c = d.objectForKey('additionalInfo');
  return c.toJS();
};

var dispatchActions = function(actions, context){
  return require('IIOInsertNativeBridge').dispatchActions_context(actions,context);
};

var dispatchTriggerActions = function(actions, context){
  return require('IIOInsertNativeBridge').dispatchTriggerActions_context(actions,context);
};

var findElementById = function(id) {
  return new StandIn(id);
};

function insertRun(context){}

