//
//  InsertManager.h
//  InsertFramework
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


/**
 *  The SDK will post the following notifications on initialization.
 *  The notifications are posted to the main thread
 *
 *  This notification is sent out after the SDK has successfully initialized
 */
extern NSString *const kIIODidSuccessfullyInitializeSDKNotification;

/**
 *  This notification is sent out when an error occurs during initialization of the SDK
 */
extern NSString *const kIIOErrorInitializeSDKNotification;

@class InsertManagerOptions;

/**
 *  InsertManager. Handles initialization of the Insert SDK.
 */
@interface InsertManager : NSObject

/**
 *  Use [InsertManager sharedManager] instead
 */
-(instancetype)init NS_UNAVAILABLE;

/**
 *  The shared instance of the InsertManager
 */
+(instancetype)sharedManager;



/**
 *  Call this method on the sharedManger with your application key and company name
 *
 *  @param appKey The app key for your account
 *  @param companyName The company name as defined in your account
 */
-(void)initSDK:(nonnull NSString *)appKey companyName:(nonnull NSString *)companyName;

/**
 Call this method on the sharedManger with your application key company name and options object

 <code> [InsertManager sharedManager] initSDK:@"Your app key" companyName:@"Your comany name" options:[InsertManagerOptions optionsWithStrictMode:@[[SomeClass class],[AnotherClass class]] whiteListPrefix:@[@"UI",@"NS"]]</code>
 @param appKey The app key for your account
 @param companyName The company name as defined in your account
 @param options The options to initialize with
 */
-(void)initSDK:(nonnull NSString *)appKey companyName:(nonnull NSString *)companyName options:(nullable InsertManagerOptions *)options;
/**
 *  Called from your app delegate when launched from a deep link containing an insert pairing URL
 *  @warning This method should always be called after initSDK: with your application key
 *  @param url The pairing URL
 */
- (void)initWithUrl:(nonnull NSURL *)url;


/**
 Stops the SDK. The SDK will invalidate all current inserts.
 */
-(void)stop;
/**
 *  Called from your app delegate [appDelegate application:didReceiveRemoteNotification:] (or varaints) to notify the insert SDK that a push was received
 *  This is required for push based inserts
 *
 *  @param userInfo The user info object passed from the push notification
 */
-(void)didReceiveRemoteNotification:(NSDictionary *)userInfo;

/**
 *  Calling this method will forcefully close any currently visible inserts
 */
-(void)dismissVisibleInserts;

/**
 *  Called from host app for custom event occuring.
 *
 *  @param eventName - The event name that was selected by the host app
 *  @param params - A dictionary of parameters defined by the host app for checking against pre-defined conditions
 *
 */
-(void)eventOccurred:(NSString *)eventName params:(nullable NSDictionary<NSString *, NSString*> *)params;

/**
 *  The app key used when initializing the SDK
 */

@property (nonatomic, readonly) NSString *appKey;

/**
 *  The company name used when initializing the SDK
 */
@property (nonatomic, readonly) NSString *companyName;

/**
 The options used when initalizing the SDK
 */
@property (nonatomic, readonly , nullable) InsertManagerOptions *options;

/**
 *  Provide user attributes to the Insert SDK
 */
@property (nonatomic, strong) NSDictionary * userAttributes;

/**
 *  Provide a user Id to the Insert SDK
 */
@property (nonatomic, strong) NSString *userId;

/**
 * Provide a device token to the insert SDK
 */
@property (nonatomic, strong) NSData * pushId;

/**
 *  The internal device ID used by the Insert SDK. This value will not change if setting a userId
 */
@property (nonatomic, readonly) NSString *insertDeviceUserId;

@end




@interface InsertManagerOptions : NSObject

/**
 Create an options object that can be used to configure the SDK

 @param classes A list of Class objects that the SDK is allowed to access
 
 @discussion You can explicitly allow access to classes, or (and) allow access to a family of classes by their name prefix. Assuming we want to allow the SDK to acceess the classes 'UINavigationController' and 'UIView' or any classes whose names start with "IIO" or "FB" we would create the options object by: <code>[InsertManagerOptions optionsWithStrictMode:@[[UINavigationController class],[UIView class]] whiteListPrefix:@[@"IIO",@"FB"]</code>.
 @note To restrict access to ALL classes, pass in "nil" to both parameters.
 
 @return An options object
 */
+(instancetype)optionsWithStrictMode:(nullable NSArray <Class> *)classes;
@property (nonatomic, readonly, nullable) NSArray <Class> *whitelistClasses;
@end



NS_ASSUME_NONNULL_END


